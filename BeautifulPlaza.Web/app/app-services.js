app.factory('salonsService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var salonsService = {};

    salonsService.getSalons = function() {
        return $http.get(serviceBase + 'api/salons').then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    salonsService.getSalonsByCategory = function(categoryAlias) {
        return $http.get(serviceBase + 'api/salons/category/' + categoryAlias).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    return salonsService;

}]);

app.factory('phonesService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var phonesServiceFactory = {};

    phonesServiceFactory.deletePhone = function(salonId, phone) {
        return $http.delete(serviceBase + 'api/phones/' + salonId + '/' + phone).then(function(results) {
            return results;
        });
    };

    phonesServiceFactory.addPhone = function(salonId, phone) {
        return $http.post(serviceBase + 'api/phones/' + salonId, phone).then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    phonesServiceFactory.savePhone = function(salonId, phone) {
        return $http.put(serviceBase + 'api/phones/' + salonId, phone).then(function(results) {
            return results;
        });
    }

    return phonesServiceFactory;

}]);
