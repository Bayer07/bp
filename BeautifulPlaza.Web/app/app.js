var app = angular.module('app', ['ui.slider', 'ngAnimate', 'ngTouch', 'ui.bootstrap', 'ngRoute', 'uiSwitch', 'ngStorage', 'LocalStorageModule', 'ui.mask', 'ui.bootstrap', 'angular-img-cropper', 'checklist-model', 'textAngular', 'angular-clipboard']);

app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "app/components/home/home-view.html",
            controller: "home-controller",
        })
        .when("/salon/:salonid/:category?", {
            templateUrl: "app/components/salonInfo/salonInfo-view.html",
            controller: "salonInfo-controller"
        })
        .when("/salons/:cateogry?", {
            templateUrl: "app/components/salons/salon-view.html",
            controller: "salonsController",
            title: 'Салоны'
        })
        .when("/login", {
            templateUrl: "app/components/login/login-view.html",
            controller: "loginController",
            title: 'Вход'
        })
        .when("/adminUser", {
            templateUrl: "app/components/admin/users/users-view.html",
            controller: "users-controller"
        })
        .when("/adminHotPromotions", {
            templateUrl: "app/components/admin/hotPromotions/hotPromotions-view.html",
            controller: "hotPromotions-controller",
            title: 'Горячие предложения'
        })
        .when("/adminPromotions", {
            templateUrl: "app/components/admin/promotionsAdmin/promotionsAdmin-view.html",
            controller: "promotionsAdmin-controller",
            title: 'Акции'
        })
        .when("/adminCategory", {
            templateUrl: "app/components/admin/category/category-view.html",
            controller: "category-controller",
            title: 'Управление категориями'
        })
        .when("/adminSupplierCategory", {
            templateUrl: "app/components/admin/supplier-category/supplier-category-view.html",
            controller: "supplier-category-controller",
            title: 'Категории поставщиков'
        })
        .when("/adminRole", {
            templateUrl: "app/components/admin/roles/roles-view.html",
            controller: "roles-controller",
            title: 'Роли'
        })
        .when("/adminBanners", {
            templateUrl: "app/components/admin/banners/banner-view.html",
            controller: "banner-controller",
            title: 'Управление баннерами'
        })
        .when("/salon-about/:salonid?", {
            templateUrl: "app/components/salon/salon-view.html",
            controller: "salonAbout-controller"
        })
        .when("/salon-services/:salonid?", {
            templateUrl: "app/components/salon-services/salonServices-view.html",
            controller: "salonServices-controller",
            title: 'Услуги и портфолио'
        })
        .when("/salonPaidServices/:salonid?", {
            templateUrl: "app/components/salonPaidServices/salonPaidServices-view.html",
            controller: "salonPaidServices-controller",
            title: 'Платные услуги'
        })
        .when("/messages/:messageId?", {
            templateUrl: "app/components/messages/messages-view.html",
            controller: "messages-controller"
        })
        .when("/adminSchedules", {
            templateUrl: "app/components/admin/schedules/schedule-view.html",
            controller: "adminSchedules-controller"
        })
        .when("/schedule/:salonId/:priceId/:promotionId/:hotPromotionId", {
            templateUrl: "app/components/schedule/schedule-view.html",
            controller: "schedule-controller",
            title: 'Запись'
        })
        .when("/schedules", {
            templateUrl: "app/components/schedules/schedules-view.html",
            controller: "schedules-controller",
            title: 'Записи'
        })
        .when("/salonSchedules", {
            templateUrl: "app/components/salonSchedules/salonSchedules-view.html",
            controller: "salonSchedules-controller",
            title: 'Расписание'
        })
        .when("/about", {
            templateUrl: "app/components/about/about-view.html",
            controller: "about-controller",
            title: 'О салоне'
        }).when("/register", {
            templateUrl: "app/components/register/register-view.html",
            controller: "register-controller",
            title: 'Регистрация'
        }).when("/promotions", {
            templateUrl: "app/components/promotions/promotions-view.html",
            controller: "promotions-controller",
            title: 'Акции'
        }).when("/promotion/:category", {
            templateUrl: "app/components/promotion/promotion-view.html",
            controller: "promotion-controller",
            title: 'Акции'
        }).when("/phoneVerification", {
            templateUrl: "app/components/phoneVerification/phoneVerification-view.html",
            controller: "phoneVerification-controller",
            title: 'Подтверждение телефона'
        }).when("/adminNews/:post?", {
            templateUrl: "app/components/admin/news/adminNews-view.html",
            controller: "adminNews-controller",
            title: 'Новости'
        }).when("/news/:article?", {
            templateUrl: "app/components/news/news-view.html",
            controller: "news-controller",
            title: 'Новости'
        }).when("/vacancies", {
            templateUrl: "app/components/vacancies/vacancies-view.html",
            controller: "vacancies-controller",
            title: 'Вакансии'
        }).when("/adminVacancy", {
            templateUrl: "app/components/admin/vacancy/vacancy-view.html",
            controller: "vacancy-controller",
            title: 'Управление вакансиями'
        }).when("/profile", {
            templateUrl: "app/components/profile/profile-view.html",
            controller: "profile-controller",
            title: 'Профиль'
        }).when("/myJobs", {
            templateUrl: "app/components/my-vacancies/my-vacancies-view.html",
            controller: "my-vacancies-controller",
            title: 'Мои вакансии'
        }).when("/supplier-about/:firmId", {
            templateUrl: "app/components/supplier-about/supplier-about-view.html",
            controller: "supplier-about-controller",
            title: 'О фирме'
        }).when("/supplier-services/:firmId", {
            templateUrl: "app/components/supplier-services/supplier-services-view.html",
            controller: "supplier-services-controller",
            title: 'Товары и услуги'
        }).when("/for-salon", {
            templateUrl: "app/components/for-salon/for-salon-view.html",
            controller: "for-salon-controller",
            title: 'Для салона'
        });


    $httpProvider.interceptors.push('authInterceptorService');
}]);

var serviceBase = 'http://localhost:5000/';
var serviceCDN = 'http://cdn.bp.style/';
//var serviceBase = 'http://api.bp.style/';

app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    ServiceCDNUri: serviceCDN,
    clientId: 'ngAuthApp'
});

app.config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(['authService', function(authService) {
    authService.fillAuthData();
}]);
