app.controller('supplier-services-controller', ['$scope', '$routeParams', 'supplierProductFactory', 'supplierCategoryFactory', function($scope, $routeParams, supplierProductFactory, supplierCategoryFactory) {

    var firmId = $routeParams.firmId;

    supplierProductFactory.get(firmId).then(function(response) {
        angular.forEach(response, function(cat) {
            if (cat.isChecked) {
                cat.isVisible = true;
            }
        });
        $scope.supplierCategories = response;
    }, function(err) {

    });

    $scope.categoryClick = function(category) {
        if (category.isChecked) {
            supplierCategoryFactory.addToFirm(category.category.id, firmId).then(function(response) {
                category.isVisible = true;
            }, function(err) {

            });
        } else {
            supplierCategoryFactory.deleteFromFirm(category.category.id, firmId).then(function(response) {
                category.isVisible = false;
            }, function(err) {

            });
        }
    }

    $scope.submit = function(category) {
        var data = {};
        if (category.new.id == null) {
            data = {
                name: category.new
            };
        } else {
            data = category.new;
        }
        supplierProductFactory.addToFirm(category.category.id, firmId, data).then(function(response) {
            category.supplierProductions.push(response);
        }, function(err) {
            console.log(err);
        });
    }

    $scope.remove = function(prod, category) {
        supplierProductFactory.removeFromFirm(category.category.id, firmId, prod).then(function(response) {
            var index = category.supplierProductions.indexOf(prod);
            category.supplierProductions.splice(index, 1);
        }, function(err) {
            console.log(err);
        });
    }

}]);
