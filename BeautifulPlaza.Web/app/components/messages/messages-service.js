app.factory('messageService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var messageFactory = {};
    messageFactory.getTitles = function() {
        return $http.get(serviceBase + 'api/messages').then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    messageFactory.getMessages = function(id) {
        return $http.get(serviceBase + 'api/messages/' + id).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    messageFactory.postTitle = function(title) {
        return $http.post(serviceBase + 'api/messages', title).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    messageFactory.postMessage = function(titleId, message) {
        return $http.post(serviceBase + 'api/messages/' + titleId, message).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    return messageFactory;
}]);
