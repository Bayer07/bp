'use strict';
app.controller('messages-controller', ['$scope', '$routeParams', 'messageService', '$location',
    function($scope, $routeParams, messageService, $location) {
        $scope.$parent.ContentHeader = 'Сообщения';
        var messageId = $routeParams.messageId;
        if (messageId == null) {
            $scope.isList = true;
            $scope.newTitle = {
                Title: '',
                Body: ''
            };
            $scope.messageVisible = false;
            $scope.submit = function(title) {
                messageService.postTitle(title).then(function(result) {
                    $scope.titles.push(result);
                    $scope.messageVisible = false;
                });
            }
            messageService.getTitles().then(function(result) {
                $scope.titles = result;
            });
            $scope.click = function(message) {
                $location.path('messages/' + message.Id)
            }
        } else {
            $scope.isList = false;
            $scope.newMessage = {
                Body: ''
            };
            messageService.getMessages(messageId).then(function(result) {
                $scope.$parent.ContentHeader = result.Title;
                $scope.messages = result;
            })
            $scope.return = function() {
                var path = $location.path();
                var array = path.split('/');
                var newarray2 = array.splice(-1, 1);
                var newarray3 = array.join('/');
                $location.path(newarray3);
            }
            $scope.submitMessage = function(message) {
                messageService.postMessage(messageId, message).then(function(result) {
                    $scope.messages.Messages.push(result);
                    $scope.newMessage.Body = '';
                });
            }
        }
    }
]);
