app.filter('salonsFilter', function() {
    return function(salons, searchParam) {
        var filtered = [];
        if (searchParam.Name.length == 0 && searchParam.Street.length == 0 && searchParam.IsSalon == true && searchParam.IsFreelance == true && searchParam.Category == 0 && searchParam.Price[0] == 0 && searchParam.Price[1] == 10000) {
            return salons;
        }
        angular.forEach(salons, function(salon) {
            var isMatch = true;
            if (searchParam.Category != 0) {
                var haveCategory = false;
                angular.forEach(salon.Categories, function(category) {
                    if (searchParam.Category == category.Category.Id) {
                        haveCategory = true;
                    }
                });
                if (haveCategory == false) {
                    isMatch = false;
                }
            }
            if (searchParam.Name.length) {
                if (salon.Name.toLowerCase().indexOf(searchParam.Name.toLowerCase()) == -1) {
                    isMatch = false;
                }
            }
            if (searchParam.Street.length) {
                if (salon.Street.Name.toLowerCase().indexOf(searchParam.Street.toLowerCase()) == -1) {
                    isMatch = false;
                }
            }
            if (searchParam.Name.Name !== undefined) {
                if (salon.Name.toLowerCase().indexOf(searchParam.Name.Name.toLowerCase()) == -1) {
                    isMatch = false;
                }
            }
            if (typeof searchParam.Street !== 'string') {
                if (salon.Street.Name.toLowerCase().indexOf(searchParam.Street.Name.toLowerCase()) == -1) {
                    isMatch = false;
                }
            }
            if (searchParam.IsSalon != true || searchParam.IsFreelance != true) {
                if (searchParam.IsSalon == salon.IsFreelance) {
                    isMatch = false;
                }
                if (searchParam.IsFreelance != salon.IsFreelance) {
                    isMatch = false;
                }
            }
            var havePrice = false;
            if (searchParam.Price[0] != 0 || searchParam.Price[1] != 10000) {
                havePrice = true;
            }
            if (searchParam.Service || havePrice) {
                var haveService = false;
                angular.forEach(salon.Categories, function(category) {
                    if (searchParam.Service.Id !== undefined) {
                        if (searchParam.Service.CategoryId == category.Category.Id) {
                            angular.forEach(category.Services, function(service) {
                                if (service.Service.Id == searchParam.Service.Id) {
                                    haveService = true;
                                }
                                if (searchParam.Price[0] <= service.Price.Min && searchParam.Price[1] >= service.Price.Max) {
                                    haveService = true;
                                }
                            });
                        }
                    } else {
                        angular.forEach(category.Services, function(service) {
                            if (service.Service.Name.toLowerCase().indexOf(searchParam.Service.toLowerCase()) == -1) {
                                haveService = true;
                            }
                            if (havePrice) {
                                if (searchParam.Price[0] <= service.Price.Min && searchParam.Price[1] >= service.Price.Max) {
                                    haveService = true;
                                }
                            }
                        });
                    }
                });
                if (haveService == false) {
                    isMatch = false;
                }
            }
            if (isMatch) {
                filtered.push(salon);
            }
        });
        return filtered;
    };
});
