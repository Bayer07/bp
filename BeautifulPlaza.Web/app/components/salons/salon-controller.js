app.controller('salonsController', function($scope, $http, $filter, $routeParams, $location, $anchorScroll, $q, salonsService, serviceFactory, freelanceFactory, salonFactory, localStorageService, ngAuthSettings, $rootScope, $anchorScroll) {

    $scope.ServiceCDNUri = ngAuthSettings.ServiceCDNUri;
    $scope.filtered = [];
    $scope.resetFilter = function() {
        $scope.searchParam = {
            Price: [0, 10000],
            Name: '',
            Street: '',
            Category: 0,
            IsSalon: true,
            IsFreelance: true,
            Service: ''
        }
    }

    var myMap = {};
    var fillMap = function() {
        var featureCollection = {
            type: "FeatureCollection",
            features: []
        };
        var features = [];
        var getAddress = function(salon) {
            var result = 'ул.' + salon.Street.Name;
            if (salon.House.length > 0) {
                result += ' д.' + salon.House;
            }
            if (salon.Floor.length > 0) {
                result += ' эт.' + salon.Floor;
            }
            if (salon.Office.length > 0) {
                result += ' оф.' + salon.Office;
            }
            return result;
        }
        var getPhones = function(phones) {
            var result = '';
            angular.forEach(phones, function(phone) {
                result += "<a href='tel:+" + phone.Number + "'>+" + phone.Number + '</a><br>';
            });
            return result;
        }
        var getBalloon = function(salon) {
            return "<a href='http://bp.style/#salon/" + salon.Id + "'>" + salon.Name + "</a><br>Адрес: " + getAddress(salon) + "<br>Телефоны: <br>" + getPhones(salon.Phones);
            // return "<a href='http://bp.style/salon/"+salon.Id+">"+salon.Name+"</a><br>"
        }
        angular.forEach($scope.filtered, function(salon) {

            var feature = {
                type: "Feature",
                id: salon.Id,
                geometry: {
                    type: "Point",
                    coordinates: [salon.Coordinates.Longitude, salon.Coordinates.Latitude]
                },
                properties: {
                    balloonContent: getBalloon(salon),
                    hintContent: salon.Name
                }
            };
            features.push(feature);
        });
        featureCollection.features = features;
        objectManager.removeAll();
        objectManager.add(featureCollection);
        var bounds = objectManager.getBounds();
        if (features.length > 1) {
            myMap.setBounds(bounds);
        } else if (features.length == 1) {
            myMap.setCenter(features[0].geometry.coordinates, 15);
        }
        myMap.geoObjects.events.add('click', function(e) {
            var geoObject = e.get('target');

            // console.log(geoObject.get('balloon'));
        });
    };
    $scope.showOnMap = function(coor) {
        myMap.setCenter([coor.Longitude, coor.Latitude], 17);
        var id = $location.hash();
        $location.hash('map');
        $anchorScroll();
        $location.hash(id);
    }
    var deferred = $q.defer();
    var urlCalls = [];
    urlCalls.push(ymaps.ready(init));

    // $scope.salons = localStorageService.get('salons');
    // if ($scope.salons == null) {
    //     urlCalls.push(salonFactory.get($scope.city.Id));
    // }

    urlCalls.push(salonFactory.get($scope.city.Id));

    $q.all(urlCalls).then(
        function(results) {
            if ($scope.salons == null) {
                $scope.salons = results[1];
                localStorageService.set('salons', results[1]);
            }
            $scope.$watch('filtered', function() {
                fillMap();
            });
        });

    function init() {
        myMap = new ymaps.Map('map', {
            center: [55.030199, 82.92043],
            zoom: 11
        });
        objectManager = new ymaps.ObjectManager({
            clusterize: false
        });
        myMap.geoObjects.add(objectManager);
    };
});
