app.controller('home-controller', ['$scope', '$http', 'bannerFactory', '$location', 'salonFactory', 'serviceFactory', 'streetFactory', 'hotPormotionsFactory', 'newsFactory',
    function($scope, $http, bannerFactory, $location, salonFactory, serviceFactory, streetFactory, hotPormotionsFactory, newsFactory) {

        $scope.ContentHeader = null;

        newsFactory.get().then(function(news) {
            $scope.news = news;
        });

        $scope.pathToImage = function(image) {
            return newsFactory.path(image);
        }
        
        hotPormotionsFactory.get().then(function(result) {
            $scope.hotPromotions = result;
        });

        $scope.path = function(banner) {
            return bannerFactory.path(banner);
        }

        $scope.pathHot = function(hot) {
            return hotPormotionsFactory.path(hot);
        }

        $scope.bannerClick = function(salon) {
            $location.path('salon/' + salon);
        }

        $scope.myInterval = 7000;
        $scope.noWrapSlides = false;
        $scope.active = 0;

        $scope.articleClick = function(id) {
            $location.path('news/' + id);
        }
    }
]);
