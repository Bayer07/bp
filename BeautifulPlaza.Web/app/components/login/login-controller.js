'use strict';
app.controller('loginController', ['$scope', '$location', '$route', 'authService', function($scope, $location, $route, authService) {

    $scope.$parent.ContentHeader = null;

    $scope.loginData = {
        userName: '',
        password: ''
    };

    $scope.message = '';

    $scope.login = function() {
        authService.login($scope.loginData).then(function(response) {
                if (response.role == 'Salon') {
                    $scope.$parent.loadSalons();
                }
                $location.path('/news');
            },
            function(err) {
                if (err.error == 'phone_not_confirmed') {
                    $location.path('phoneVerification').search({
                        phone: $scope.loginData.userName
                    });
                } else {
                    $scope.message = err.error_description;
                }
            });
    };

}]);
