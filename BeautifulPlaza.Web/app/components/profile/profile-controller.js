app.controller('profile-controller', ['$scope', 'authService',
    function($scope, authService) {
        authService.getProfile().then(function(response) {
            response.BirthDay = new Date(response.BirthDay);
            response.BirthDayOpened = false;
            $scope.profile = response;
        }, function(err, status) {
            console.log(err, status);
        });
        $scope.openBirthDay = function() {
            $scope.profile.BirthDayOpened = true;
        }
        $scope.submit = function() {
            authService.postProfile($scope.profile).then(function(response) {

            });
        }
    }
]);
