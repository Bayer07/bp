app.controller('users-controller', ['$scope', 'usersService', 'rolesService', function($scope, usersService, rolesService) {
    $scope.$parent.ContentHeader = 'Пользователи';
    usersService.getUsers().then(function(result) {
        $scope.users = result;
    });
    rolesService.getRoles().then(function(result) {
        $scope.roles = result;
    });
    $scope.addUserToRole = function(user) {
        rolesService.addUserToRole(user, user.Roles[0]).then(function(result) {
        });
    };
}]);
