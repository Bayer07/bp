app.factory('usersService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var userFactory = {};
    userFactory.getUser = function(id) {
        return $http.get(serviceBase + 'api/users/' + id).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    userFactory.getUsers = function() {
        return $http.get(serviceBase + 'api/users').then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    return userFactory;
}]);
