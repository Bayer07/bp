app.controller('category-controller', ['$scope', 'categoryFactory', 'localStorageService', '$uibModal', function($scope, categoryFactory, localStorageService, $uibModal) {

    categoryFactory.get().then(function(categories) {
        $scope.categories = categories;
    });

    $scope.category = {
        visible: false
    };

    $scope.path = function(category) {
        return categoryFactory.path(category);
    }

    $scope.upload = function(category) {
        var size = {
            width: 300,
            height: 200,
            minWidth: 300,
            minHeight: 200
        };
        var data = {
            size: size
        };
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/components/modals/pickLogo-view.html',
            controller: 'LogoModalCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: function() {
                    return data;
                }
            }
        });
        modalInstance.result.then(function(image) {
            category.CroppedImage = image;
            if (category.Id != null) {
                categoryFactory.putImage(category).then(function(result) {
                    category.Image = result.Image;
                });
            }
        });
    }

    $scope.submit = function(category) {
        categoryFactory.post(category).then(function(result) {
            $scope.categories.push(result);
        });
    }

    $scope.putImage = function(cat) {
        categoryFactory.putImage(cat);
    }

    $scope.update = function(cat) {
        categoryFactory.put(cat).then(function(result) {
            console.log(result);
        });
    }

    $scope.delete = function(cat) {
        categoryFactory.delete(cat.Id).then(function() {
            var index = $scope.categories.indexOf(cat);
            $scope.categories.splice(index, 1);
        });
    }
}]);
