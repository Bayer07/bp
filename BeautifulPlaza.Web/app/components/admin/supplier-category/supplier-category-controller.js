app.controller('supplier-category-controller', ['$scope', 'supplierCategoryFactory', '$uibModal', '$route', function($scope, supplierCategoryFactory, $uibModal, $route) {

    $scope.newcategory = {
        name: '',
        image: ''
    };
    supplierCategoryFactory.get().then(function(response) {
        $scope.categories = response;
    });

    $scope.upload = function(category) {
        var size = {
            width: 300,
            height: 200,
            minWidth: 300,
            minHeight: 200
        };
        var data = {
            size: size
        };
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/components/modals/pickLogo-view.html',
            controller: 'LogoModalCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: function() {
                    return data;
                }
            }
        });
        modalInstance.result.then(function(image) {
            category.croppedImage = image;
            if (category.id != null) {
                $scope.change(category);
            }
        });
    }

    $scope.submit = function() {
        supplierCategoryFactory.post($scope.newcategory).then(function(response) {
            $scope.categories.push(response);
            $scope.error = null;
            $scope.newcategory = {
                name: '',
                image: ''
            };
        }, function(err) {
            $scope.error = err.modelState;
        });
    }

    $scope.path = function(image) {
        return supplierCategoryFactory.path(image);
    }

    $scope.delete = function(cat) {
        supplierCategoryFactory.delete(cat).then(function(response) {
            var index = $scope.categories.indexOf(cat);
            $scope.categories.splice(index, 1);
        }, function(err) {
            $scope.error = err.modelState;
        });
    }

    $scope.change = function(cat) {
        supplierCategoryFactory.change(cat).then(function(response) {
            if (cat.id != null) {
                // $scope.$apply();
            }
        }, function(err) {
            $scope.error = err.modelState;
        });
    }

    $scope.submitProduct = function(category, newProduct) {
        var data = {
            name: newProduct
        };
        supplierCategoryFactory.addProduct(category.id, data).then(function(response) {
            category.supplierProducts.push(response);
        }, function(err) {

        });
    }

    $scope.changeProduct = function(categoryid, prod) {
        supplierCategoryFactory.changeProduct(categoryid, prod).then(function(response) {
            console.log(response);
        }, function(err) {
            console.log(err);
        });
    }
}]);
