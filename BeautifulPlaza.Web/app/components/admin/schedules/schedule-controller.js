'use strict';

app.controller('adminSchedules-controller', ['salonsService', '$scope', 'scheduleService', 'salonFactory', function(salonsService, $scope, scheduleService, salonFactory) {
    $scope.$parent.ContentHeader = 'Online записи';
    $scope.state = [{
        value: false,
        text: 'Не активен'
    }, {
        value: true,
        text: 'Активен'
    }];
    $scope.format = 'yyyy/MM/dd';
    scheduleService.getAllSchedules().then(function(schedules) {
        angular.forEach(schedules, function(schedule) {
            schedule.startOpened = false;
            schedule.endOpened = false;
            schedule.StartDate = new Date(schedule.StartDate);
            schedule.EndDate = new Date(schedule.EndDate);
            salonFactory.getName(schedule.FirmId).then(function(name) {
                schedule.salon = name;
            })
        })
        $scope.schedules = schedules;
    });
    $scope.startOpen = function(schedule) {
        schedule.startOpened = true;
    };
    $scope.endOpen = function(schedule) {
        schedule.endOpened = true;
    };
    $scope.update = function(schedule) {
        scheduleService.update(schedule);
    };
    $scope.delete = function(schedule) {
        scheduleService.delete(schedule).then(function(result) {
            var index = $scope.schedules.indexOf(schedule);
            $scope.schedules.splice(index, 1);
        })
    };
    $scope.newSchedule = {
        isVisible: false,
        salon: null,
        IsActive: true,
        StartDate: new Date(),
        EndDate: new Date(),
        startOpened: false,
        endOpened: false
    };
    salonsService.getSalons().then(function(result) {
        $scope.salons = result;
    });
    $scope.post = function(schedule) {
        scheduleService.post(schedule).then(function(result) {
            result.startOpened = false;
            result.endOpened = false;
            result.StartDate = new Date(result.StartDate);
            result.EndDate = new Date(result.EndDate);
            $scope.schedules.push(result);
        })
    };
}]);
