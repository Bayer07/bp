'use strict';

app.controller('banner-controller', ['$scope', 'bannerFactory', 'salonFactory', function($scope, bannerFactory, salonFactory) {

    $scope.$parent.ContentHeader = 'Баннеры';
    $scope.state = [{
        value: false,
        text: 'Не активен'
    }, {
        value: true,
        text: 'Активен'
    }];

    $scope.format = 'yyyy/MM/dd';
    bannerFactory.getBanners().then(function(banners) {
        angular.forEach(banners, function(banner) {
            banner.startOpened = false;
            banner.endOpened = false;
            banner.StartDate = new Date(banner.StartDate);
            banner.EndDate = new Date(banner.EndDate);
            salonFactory.getName(banner.SalonId).then(function(name) {
                banner.Salon = {
                    Name: name
                };
            })
        })
        $scope.banners = banners;
    });

    $scope.startOpen = function(banner) {
        banner.startOpened = true;
    };

    $scope.endOpen = function(banner) {
        banner.endOpened = true;
    };

    $scope.update = function(banner) {
        banner.StartDate = banner.StartDate;
        banner.EndDate = banner.EndDate;
        bannerFactory.update(banner);
    };

    $scope.path = function(banner) {
        return bannerFactory.path(banner);
    }

    $scope.delete = function(banner) {
        bannerFactory.delete(banner).then(function() {
            var index = $scope.banners.indexOf(banner);
            $scope.banners.splice(index, 1);
        });
    };
}]);
