'use strict';
app.controller('roles-controller', ['$scope', 'rolesService', 'usersService', function($scope, rolesService, usersService) {
    $scope.$parent.ContentHeader = 'Роли';
    rolesService.getRoles().then(function(roles) {
        $scope.roles = roles;
    });
    $scope.newrole = {
        Name: '',
        Description: ''
    };
    $scope.post = function(role) {
        rolesService.post(role).then(function(result) {
            $scope.roles.push(result);
            $scope.newrole = {
                Name: '',
                Description: ''
            };
        });
    };
    $scope.put = function(role) {
        rolesService.put(role).then(function(result) {
        });
    };
}]);
