app.factory('rolesService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var roleFactory = {};
    roleFactory.getRoles = function() {
        return $http.get(serviceBase + 'api/roles').then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    roleFactory.post = function(role) {
        return $http.post(serviceBase + 'api/roles', role).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    roleFactory.put = function(role) {
        return $http.put(serviceBase + 'api/roles', role).then(function(results) {
            return JSON.parse([results.data]);
        });
    };
    roleFactory.addUserToRole = function(user, role) {
        return $http.post(serviceBase + 'api/roles/' + user.Id, role).then(function(result) {
            return JSON.parse([result.data]);
        })
    };
    return roleFactory;
}]);
