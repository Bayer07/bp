app.controller('vacancy-controller', ['$scope', 'vacancyFactory', function($scope, vacancyFactory) {
    $scope.experience = {
        Text: '',
        array: []
    };
    vacancyFactory.getExpirience().then(function(arr) {
        $scope.experience.array = arr;
    });
    vacancyFactory.getByAdmin().then(function(result) {
        $scope.salons = result;
        angular.forEach(result, function(salon) {
            angular.forEach(salon.Vacancies, function(item) {
                item.openedStartDate = false;
                item.openedEndDate = false;
                item.opened = false;
                item.startDate = new Date(item.StartDate);
                item.endDate = new Date(item.EndDate);
            });
        });
    });
    vacancyFactory.getNamesByAdmin().then(function(names) {
        $scope.vacancyNames = names;
    });
    $scope.status = [{
        Name: 'Активен',
        Value: true
    }, {
        Name: 'Не активен',
        Value: false
    }];
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        startingDay: 1
    };
    $scope.open = function(item) {
        item.openedStartDate = true;
    };
    $scope.open2 = function(item) {
        item.openedEndDate = true;
    };
    $scope.open3 = function(item) {
        item.opened = !item.opened;
    };
    $scope.put = function(v) {
        v.StartDate = v.startDate;
        v.EndDate = v.endDate;
        vacancyFactory.put(v);
    }
    $scope.put2 = function(v) {
        vacancyFactory.putVacancyName(v);
    }
    $scope.submitExperience = function() {
        vacancyFactory.postExperience($scope.experience).then(function(e) {
            $scope.experience.array.push(e);
        });
    }
}]);
