app.controller('adminNews-controller', ['$scope', '$uibModal', 'textAngularManager', 'newsFactory', '$location', '$routeParams', '$filter', function($scope, $uibModal, textAngularManager, newsFactory, $location, $routeParams, $filter) {

    var postId = $routeParams.post;

    $scope.postNews = {
        Title: ''
    };

    newsFactory.getAll().then(function(result) {
        $scope.news = result;
        if (postId != null) {
            $scope.post = $filter('getById')($scope.news, postId);
        }
    });

    $scope.addNews = function() {
        newsFactory.post($scope.postNews).then(function(result) {
            $scope.newsClick(result.Id);
        });
    }

    $scope.newsClick = function(news) {
        $location.path('adminNews/' + news);
    }

    $scope.pickTitleImage = function() {
        var size = {
            width: 300,
            height: 300,
            minWidth: 300,
            minHeight: 300
        };
        var data = {
            size: size
        };
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/components/modals/pickLogo-view.html',
            controller: 'LogoModalCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: function() {
                    return data;
                }
            }
        });
        modalInstance.result.then(function(image) {
            newsFactory.postTitleImage(postId, {
                Image: image
            }).then(function(result) {
                $scope.post.TitleImage = result;
            });
        });
    }

    $scope.deleteImage = function(image) {
        newsFactory.deleteImage(postId, image.Id).then(function() {
            var index = $scope.post.Images.indexOf(image);
            $scope.post.Images.splice(index, 1);
        });
    }

    $scope.pickImage = function() {
        var size = {
            width: 1280,
            height: 720,
            minWidth: 1280,
            minHeight: 720
        };
        var data = {
            size: size
        };
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/components/modals/pickLogo-view.html',
            controller: 'LogoModalCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: function() {
                    return data;
                }
            }
        });
        modalInstance.result.then(function(image) {
            newsFactory.postImage(postId, {
                Image: image
            }).then(function(result) {
                $scope.post.Images.push(result);
            });
        });
    }

    $scope.pathToImage = function(image) {
        return newsFactory.path(image);
    }

    $scope.supported = false;

    $scope.success = function() {
        console.log('Copied!');
    };

    $scope.fail = function(err) {
        console.error('Error!', err);
    };

    $scope.editNews = function() {
        newsFactory.put($scope.post);
    }
}]);
