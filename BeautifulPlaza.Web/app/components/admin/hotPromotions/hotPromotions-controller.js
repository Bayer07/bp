app.controller('hotPromotions-controller', ['$scope', '$uibModal', 'hotPormotionsFactory', 'salonFactory', function($scope, $uibModal, hotPormotionsFactory, salonFactory) {

    hotPormotionsFactory.getAll().then(function(result) {
        $scope.hotPromotions = result;
    });

    salonFactory.getNames().then(function(result) {
        $scope.salonNames = result;
    })

    $scope.promotion = {
        visible: false
    };

    $scope.submit = function(promotion) {
        hotPormotionsFactory.post(promotion).then(function(result) {
            $scope.hotPromotions.push(result);
        });
    }

    $scope.upload = function(promotion) {
        var size = {
            width: 300,
            height: 200,
            minWidth: 300,
            minHeight: 200
        };
        var data = {
            size: size
        };
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/components/modals/pickLogo-view.html',
            controller: 'LogoModalCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: function() {
                    return data;
                }
            }
        });
        modalInstance.result.then(function(image) {
            promotion.CroppedImage = image;
        });
    }

    $scope.pathHot = function(hot) {
        return hotPormotionsFactory.path(hot);
    }
}]);
