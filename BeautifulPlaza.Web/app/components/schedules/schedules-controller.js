'use strict';
app.controller('schedules-controller', ['$scope', '$routeParams', 'scheduleService',
    function($scope, $routeParams, scheduleService) {
        $scope.$parent.ContentHeader = 'Мои записи';
        scheduleService.getUserSchedules().then(function(result) {
            angular.forEach(result, function(i) {
                i.Date = new Date(i.Date);
            })
            $scope.schedules = result;
        })
        $scope.status = ['В ожидании', 'Отменен', 'Выполнен'];
    }
]);
