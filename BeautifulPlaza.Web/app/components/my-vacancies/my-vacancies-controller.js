app.controller('my-vacancies-controller', ['$scope', 'vacancyFactory', function($scope, vacancyFactory) {
    vacancyFactory.getByUser().then(function(response) {
        angular.forEach(response, function(firm) {
            angular.forEach(firm.Vacancies, function(v) {
                v.StartDate = new Date(v.StartDate);
                v.EndDate = new Date(v.EndDate);
            });
        });

        $scope.firms = response;
    });
    $scope.status = [{
        Name: 'Активен',
        Value: true
    }, {
        Name: 'Не активен',
        Value: false
    }];
    vacancyFactory.getExpirience().then(function(arr) {
        $scope.experience = arr;
    });
}]);
