app.controller('promotions-controller', ['$scope', '$http', '$routeParams', '$location', 'categoryFactory', 'promotionFactory',
    function($scope, $http, $routeParams, $location, categoryFactory, promotionFactory) {

        categoryFactory.get().then(function(categories) {
            $scope.categories = categories;
        });

        $scope.registerClick = function() {
            $("#myModal").removeClass("in");
            $(".modal-backdrop").remove();
            $("#myModal").hide();
            $location.path('register');
        }

        $scope.path = function(category) {
            return categoryFactory.path(category);
        }
    }
]);
