app.controller('phoneVerification-controller', ['$scope', '$location', 'authService', '$routeParams',
    function($scope, $location, authService, $routeParams) {
        $scope.$parent.ContentHeader = "Подтверждение номера телефона";
        $scope.submit = function() {
            authService.verifyPhone({
                Code: $scope.code,
                Phone: $routeParams.phone
            }).then(function(result) {
                $location.path('login');
            }, function(error) {
                $scope.error = error;
            });
        }
    }
]);
