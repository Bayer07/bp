app.factory('salonPaidServicesService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var salonPaidServicesFactory = {};    

    salonPaidServicesFactory.getPromotions = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/promotion/').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    salonPaidServicesFactory.getBannerCost = function() {
        return $http.get(serviceBase + 'api/bannerCost').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    salonPaidServicesFactory.getPromotionCost = function() {
        return $http.get(serviceBase + 'api/promotionCost').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    salonPaidServicesFactory.getVacancyCost = function() {
        return $http.get(serviceBase + 'api/vacancyCost').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    salonPaidServicesFactory.getBannerDesignerCost = function() {
        return $http.get(serviceBase + 'api/bannerDesignerCost').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    salonPaidServicesFactory.getPromotionDesignerCost = function() {
        return $http.get(serviceBase + 'api/protomionDesignerCost').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    salonPaidServicesFactory.submitVacancy = function(salonid, data) {
        return $http.post(serviceBase + 'api/salon/' + salonid + '/vacancy', data).then(function(results) {
                return JSON.parse([results.data]);
            },
            function errorCallback(response) {
                return response.data;
            });
    };

    salonPaidServicesFactory.submitBanner = function(data) {
        return $http.post(serviceBase + 'api/banner', data).then(function(results) {
            return JSON.parse([results.data]);
        })
    }

    return salonPaidServicesFactory;

}]);
