'use strict';
app.controller('salonPaidServices-controller', ['$scope', 'salonPaidServicesService', '$routeParams', 'categoryFactory', 'scheduleService', 'bannerFactory', '$uibModal', 'cityFactory', 'streetFactory', '$rootScope', 'promotionFactory', 'vacancyFactory',
    function($scope, salonPaidServicesService, $routeParams, categoryFactory, scheduleService, bannerFactory, $uibModal, cityFactory, streetFactory, $rootScope, promotionFactory, vacancyFactory) {

        $scope.ContentHeader = $rootScope.title;
        var firmId = $routeParams.salonid;

        $scope.banner = {
            NeedDesigner: true,
            Image: '',
            Message: '',
            SalonId: firmId
        };

        $scope.promotion = {
            Image: null,
            NeedDesigner: true,
            Message: '',
            Title: '',
            Description: '',
            SalonId: firmId,
            Categories: []
        };

        $scope.vacancy = {};
        vacancyFactory.getByFirm(firmId).then(function(vacancies) {
            $scope.vacancies = vacancies;
        });
        vacancyFactory.getNames().then(function(names) {
            $scope.vacancyNames = names;
        });
        vacancyFactory.getExpirience().then(function(e) {
            $scope.expirience = e;
            $scope.vacancy.Experience = e[0];
        });
        $scope.selectVacancy = function(v) {
            $scope.vacancy.Vacancy = v;
        };

        cityFactory.get().then(function(result) {
            $scope.cities = result;
        });
        $scope.selectCity = function(city) {
            $scope.vacancy.City = city;
            streetFactory.getByCity(city.Id).then(function(result) {
                $scope.streets = result;
            });
        };

        $scope.categorySelection = [];

        categoryFactory.get().then(function(categories) {
            $scope.categories = categories;
        });

        bannerFactory.getBySalon(firmId).then(function(banners) {
            $scope.banners = banners;
        });

        $scope.bannerPath = function(banner) {
            return bannerFactory.path(banner);
        }

        $scope.promotionPath = function(promotion) {
            return promotionFactory.path(promotion);
        }

        scheduleService.getScheduleRequests(firmId).then(function(scheduleRequest) {
            $scope.scheduleRequests = scheduleRequest;
        });

        scheduleService.getScheduleCost().then(function(scheduleCost) {
            $scope.scheduleCost = scheduleCost;
        });

        $scope.submitSchedule = function() {
            scheduleService.submit(firmId).then(function(result) {
                $scope.scheduleRequests.push(result);
            });
        }

        salonPaidServicesService.getPromotions(firmId).then(function(promotoionsRequest) {
            $scope.promotoionsRequest = promotoionsRequest;
        });

        salonPaidServicesService.getBannerCost().then(function(bannerCost) {
            $scope.bannerCost = bannerCost;
        });

        salonPaidServicesService.getPromotionCost().then(function(promotionCost) {
            $scope.promotionCost = promotionCost;
        });

        salonPaidServicesService.getVacancyCost().then(function(cost) {
            $scope.vacancy.Cost = cost;
        });

        salonPaidServicesService.getBannerDesignerCost().then(function(designerCost) {
            $scope.designerCost = designerCost;
        });

        salonPaidServicesService.getPromotionDesignerCost().then(function(promotionDesignerCost) {
            $scope.promotionDesignerCost = promotionDesignerCost;
        });

        $scope.submitBanner = function() {
            salonPaidServicesService.submitBanner($scope.banner).then(function(result) {
                $scope.banners.push(result);
            })
        }
        $scope.submitVacancy = function() {
            var city = $scope.vacancy.City;
            var street = $scope.vacancy.Street;
            var vacancyName = $scope.vacancy.VacancyName;
            if (city.Name == null) {
                $scope.vacancy.City = {
                    Name: city
                };
            }
            if (street.Name == null) {
                $scope.vacancy.Street = {
                    Name: street
                };
            };
            if (vacancyName.Name == null) {
                $scope.vacancy.VacancyName = {
                    Name: vacancyName
                };
            };
            salonPaidServicesService.submitVacancy(firmId, $scope.vacancy).then(function(result) {
                $scope.vacanciesRequest.push(result);
            })
        }
        $scope.submitPromotion = function() {
            promotionFactory.submit(firmId, $scope.promotion).then(function(result) {
                $scope.promotoionsRequest.push(result);
            }, function(error) {
                $scope.vacancy.errorCallback = error;
            })
        }

        $scope.openModal = function() {

            var size = {
                width: 960,
                height: 250,
                minWidth: 860,
                minHeight: 230
            };
            var data = {
                size: size
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/modals/pickLogo-view.html',
                controller: 'LogoModalCtrl',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    data: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function(image) {
                $scope.banner.image = image;
            });
        }
        $scope.openModalPromotion = function() {
            var data = {
                size: {
                    width: 300,
                    height: 200
                }
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/modals/pickLogo-view.html',
                controller: 'LogoModalCtrl',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    data: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function(image) {
                $scope.promotion.Image = image;
            });
        }
    }
]);
