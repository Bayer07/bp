app.controller('ChoiseCityController', function($uibModalInstance, cities) {
    var $ctrl = this;
    $ctrl.cities = cities;
    $ctrl.pick = function(city) {
        $uibModalInstance.close(city);
    };
    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
