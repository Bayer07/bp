app.controller('ModalInstanceCtrl', function($uibModalInstance, date) {
    var $ctrl = this;
    $ctrl.date = date;

    $ctrl.ok = function() {
        $uibModalInstance.close($ctrl.date);
    };
    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
