app.controller('PortfolioModalCtrl', function($uibModalInstance, data) {
    var $ctrl = this;
    $ctrl.image = null;
    $ctrl.croppedImage = '';
    $ctrl.width = 100;
    $ctrl.ok = function() {
        $uibModalInstance.close($ctrl.croppedImage);
    };
    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
