app.controller('LogoModalCtrl', function($uibModalInstance, data) {
    var $ctrl = this;
    $ctrl.image = null;
    $ctrl.croppedImage = '';
    $ctrl.size = data.size;
    $ctrl.ok = function() {
        $uibModalInstance.close($ctrl.croppedImage);
    };
    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
