app.controller('for-salon-controller', ['$scope', 'supplierCategoryFactory', function($scope, supplierCategoryFactory) {
    supplierCategoryFactory.getFirms().then(function(response) {
        $scope.categories = response;
    });
    $scope.path = function(path) {
        return supplierCategoryFactory.path(path);
    }
}]);
