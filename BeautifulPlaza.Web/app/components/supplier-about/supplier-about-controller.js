app.controller('supplier-about-controller', ['$scope', '$routeParams', 'supplierFactory', 'streetFactory', 'worktimeFactory', function($scope, $routeParams, supplierFactory, streetFactory, worktimeFactory) {
    var firmId = $routeParams.firmId;
    var streets = function(id) {
        streetFactory.getByCityNew(id).then(function(response) {
            $scope.supplierstreets = response;
            if ($scope.supplier.street.cityId != id) {
                $scope.supplier.street.id = response[0].id;
                $scope.supplier.street.name = response[0].name;
                $scope.supplier.street.cityId = response[0].cityId;
            }
        });
    }

    supplierFactory.getById(firmId).then(function(response) {
        angular.forEach(response.workTimes, function(wt) {
            var open = wt.open.split(':');
            var close = wt.close.split(':');
            wt.open = new Date(2000, 1, 1, open[0], open[1], open[2]);
            wt.close = new Date(2000, 1, 1, close[0], close[1], close[2]);
        });
        $scope.supplier = response;
        streets(response.city.id);
        worktimeFactory.getDaysOfWeek().then(function(days) {
            angular.forEach($scope.supplier.workTimes, function(wt) {
                var index = days.map(function(day) {
                    return day.id;
                }).indexOf(wt.dayOfWeek.id);
                if (index !== -1) {
                    days.splice(index, 1);
                }
            });
            $scope.daysofweek = days;
            $scope.worktime.dayofweek = days[0];
        });
    });

    $scope.selectCity = function(city) {
        $scope.supplier.city.id = city.id;
        $scope.supplier.city.name = city.name;
        streets(city.id);
    }

    $scope.leaveCity = function() {
        angular.forEach($scope.cities, function(city) {
            if (city.id == $scope.supplier.city.id) {
                if (city.name != $scope.supplier.city.name) {
                    $scope.supplier.city.name = city.name;
                    return;
                }
            }
        });
    }

    $scope.selectStreet = function(street) {
        $scope.supplier.street.id = street.id;
        $scope.supplier.street.name = street.name;
    }

    $scope.leaveStreet = function() {
        angular.forEach($scope.supplierstreets, function(street) {
            if (street.id == $scope.supplier.street.id) {
                if (street.name != $scope.supplier.street.name) {
                    $scope.supplier.street.name = street.name;
                    return;
                }
            }
        });
    }

    $scope.addNewPhone = function() {
        if ($scope.newPhone.number != undefined) {
            var data = {
                Post: true,
                Number: $scope.newPhone.number
            };
            supplierFactory.postPhone(firmId, data).then(function(response) {
                $scope.supplier.phones.push(response);
                $scope.newPhone.number = '';
            });
        }
    }

    $scope.changePhone = function(phone) {
        var data = {
            Put: true,
            Number: phone.number,
            Id: phone.id
        };
        supplierFactory.postPhone(firmId, data).then(function(response) {});
    }

    $scope.removePhone = function(phone) {
        var data = {
            Delete: true,
            Number: phone.number,
            Id: phone.id
        };
        supplierFactory.postPhone(firmId, data).then(function(response) {
            var index = $scope.supplier.phones.indexOf(phone);
            $scope.supplier.phones.splice(index, 1);
        });
    }

    $scope.postworktime = function() {
        $scope.worktime.Post = true;
        worktimeFactory.post(firmId, $scope.worktime).then(function(response) {
            var open = response.open.split(':');
            var close = response.close.split(':');
            response.open = new Date(2000, 1, 1, open[0], open[1], open[2]);
            response.close = new Date(2000, 1, 1, close[0], close[1], close[2]);
            $scope.supplier.workTimes.push(response);
            var index = $scope.daysofweek.map(function(day) {
                return day.id;
            }).indexOf(response.dayOfWeek.id);
            if (index !== -1) {
                $scope.daysofweek.splice(index, 1);
            }
            $scope.worktime.dayofweek = $scope.daysofweek[0];
        });
    }

    $scope.worktime = {
        open: new Date(2000, 1, 1, 9, 00),
        close: new Date(2000, 1, 1, 21, 00),
    };

    $scope.changeWorkTime = function(time) {
        time.Put = true;
        worktimeFactory.post(firmId, time).then(function(response) {});
    }

    $scope.removeWorktime = function(time) {
        time.Delete = true;
        worktimeFactory.post(firmId, time).then(function(response) {
            var index = $scope.supplier.workTimes.indexOf(time);
            $scope.supplier.workTimes.splice(index, 1);
            $scope.daysofweek.push(time.dayOfWeek);
        });
    }

    $scope.submit = function() {
        var data = {
            name: $scope.supplier.name,
            city: {
                id: $scope.supplier.city.id
            },
            street: {
                id: $scope.supplier.street.id
            },
            house: $scope.supplier.house,
            description: $scope.supplier.description,
            email: $scope.supplier.email.address,
        };
        data.email = $scope.supplier.email.address;
        supplierFactory.post(firmId, data).then(function(response) {
            $scope.errors = null;
        }, function(error) {
            $scope.errors = error.modelState;
        });
    }
}]);
