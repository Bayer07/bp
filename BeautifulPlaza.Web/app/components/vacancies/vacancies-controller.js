app.controller('vacancies-controller', ['$scope', 'vacancyFactory', function($scope, vacancyFactory) {
    vacancyFactory.get().then(function(firms) {
        $scope.firms = firms;
    });
}]);
