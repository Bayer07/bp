app.controller('promotion-controller', ['$scope', '$routeParams', 'authService', 'promotionFactory',
    function($scope, $routeParams, authService, promotionFactory) {
        $scope.isHaveSchedule = authService.authentication.isAuth && authService.authentication.role == 'Client';
        $scope.role = authService.authentication.role;
        $scope.category = $routeParams.category;
        promotionFactory.getByCategory($scope.category).then(function(result) {
            $scope.promotions = result;
        });

        $scope.path = function(path) {
            return promotionFactory.path(path);
        }
    }
]);
