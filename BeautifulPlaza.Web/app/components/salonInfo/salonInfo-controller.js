app.controller('salonInfo-controller', ['$scope', '$routeParams', '$http', '$location', 'authService', 'salonFactory', 'priceFactory', 'reviewFactory', 'authService', '$rootScope', 'serviceFactory', 'ngAuthSettings',
    function($scope, $routeParams, $http, $location, authService, salonFactory, priceFactory, reviewFactory, authService, $rootScope, serviceFactory, ngAuthSettings) {

        function chunk(arr, size) {
            var newArr = [];
            for (var i = 0; i < arr.length; i += size) {
                newArr.push(arr.slice(i, i + size));
            };
            return newArr;
        };

        var uri = ngAuthSettings.ServiceCDNUri;
        $scope.portfolioPath = function(portfolio) {
            return uri + 'images/portfolio/' + portfolio.Image + '.jpg';
        }

        $scope.scheduleClick = function(service) {
            $location.path('schedule/' + salonId + '/' + service.Id);
        }

        $scope.pathabs = $location.absUrl();
        $scope.url = $location.url();
        $scope.path = RemoveLastDirectoryPartOf($location.path());
        $scope.imagesPath = 'http://api.bp.style/images/portfolio';
        $scope.category = $routeParams.category;
        var salonId = $routeParams.salonid;
        $scope.isAuth = authService.authentication.isAuth;
        salonFactory.getById(salonId).then(function(salon) {
            $scope.salon = salon;
            $rootScope.title = salon.Name;
            $scope.salonCategories = chunk(salon.Categories, 2);

            if ($scope.category != undefined) {
                serviceFactory.getServicesByCategory($scope.category, salonId).then(function(services) {
                    $scope.services = services;
                });
            };

            angular.forEach(salon.Categories, function(value) {
                value.chunked = chunk(value.Portfolios, 2);
            });
        });
        reviewFactory.get(salonId).then(function(reviews) {
            $scope.reviews = reviews;
        });
        $scope.review = {
            Text: '',
            Rating: 5
        };
        $scope.reviewEditMode = false;
        $scope.reviewSubmit = function() {
            reviewFactory.post(salonId, $scope.review).then(function() {
                $scope.reviews.push($scope.review);
                $scope.review = {
                    Text: '',
                    Rating: 5
                };
                $scope.reviewEditMode = false;
            }, function() {
                $scope.reviewError = true;
            })
        }
        $scope.reviewPanelVisible = function() {
            return authService.authentication.role == 'Client';
        }

        function RemoveLastDirectoryPartOf(the_url) {
            var the_arr = the_url.split('/');
            the_arr.pop();
            return (the_arr.join('/'));
        }

        $scope.currentDate = new Date();
        var dayOfWeek = $scope.currentDate.getDay();
        $scope.setDayOfWeek = function(wt) {
            if (wt.DayOfWeek.Id == dayOfWeek) {
                return 'bg-success';
            }
        }
        $scope.getStyle = function(rating) {
            if (rating < 3) {
                return {
                    color: '#FF0000'
                };
            } else if (rating > 2 && rating < 5) {
                return {
                    color: '#FFD700'
                };
            } else {
                return {
                    color: '#008000'
                };
            }
        };

        $scope.setWorkTime =
            function(wt) {
                if (wt.DayOfWeek.Id == dayOfWeek) {
                    var hour = $scope.currentDate.getHours();
                    var minutes = $scope.currentDate.getMinutes();
                    var open = new Date();
                    var close = new Date();
                    openArray = wt.Open.split(':');
                    closeArray = wt.Close.split(':');
                    open.setHours(openArray[0], openArray[1], 0, 0);
                    close.setHours(closeArray[0], closeArray[1], 0, 0);
                    var openHours = open.getHours();
                    var closeHours = close.getHours();
                    if (hour < openHours) {
                        var difHour = openHours - hour;
                        var hourName = '';
                        if (difHour > 4) {
                            hourName = 'часов';
                        }
                        if (difHour <= 4 && difHour > 1) {
                            hourName = 'часа';
                        }
                        if (difHour > 1) {
                            return 'Откроется через ' + difHour + ' ' + hourName;
                        }
                        if (difHour == 0) {
                            var openMinutes = open.getMinutes();
                            var divMinutes = openMinutes - minutes;
                            return 'Откроется через ' + divMinutes + ' минут';
                        }
                    }
                    if (hour >= closeHours - 3) {
                        var difHour = closeHours - hour - 1;
                        if (difHour == 0) {
                            var closeMinutes = close.getMinutes();
                            if (closeMinutes == 0) {
                                closeMinutes = 60;
                            }
                            var divMinutes = closeMinutes - minutes;
                            return 'Закроется через ' + divMinutes + ' минут';
                        }
                        if (difHour == 1) {
                            return 'Закроется через ' + difHour + ' час';
                        }
                        if (difHour < 3 && difHour > 0) {
                            return 'Закроется через ' + difHour + ' часа';
                        }
                        if (difHour < 0) {
                            return 'Закрыто';
                        }
                    }
                    return "Открыто";
                }
            }
    }
]);
