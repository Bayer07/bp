'use strict';
app.controller('salonAbout-controller', ['$scope', '$route', 'salonFactory', '$routeParams', 'phonesService', '$rootScope', '$uibModal', 'streetFactory', 'ngAuthSettings',
    function($scope, $route, salonFactory, $routeParams, phonesService, $rootScope, $uibModal, streetFactory, ngAuthSettings) {
        $scope.$parent.ContentHeader = 'О салоне';
        $scope.newphone = {
            Number: '',
            Id: 0
        };
        $scope.addPhone = function() {
            if ($scope.newphone.Number.length > 0) {
                phonesService.addPhone($routeParams.salonid, $scope.newphone).then(function(phone) {
                    $scope.salon.Phones.push(phone);
                    $scope.newphone = {
                        Number: '',
                        Id: 0
                    };
                })
            }
        }
        $scope.getLogoPath = function() {
            return ngAuthSettings.ServiceCDNUri + 'images/salons/' + $scope.salon.Image + '.jpg';
        }
        $scope.savePhone = function(phone) {
            phonesService.savePhone($routeParams.salonid, phone).then(function(results) {

            })
        }
        $scope.deletePhone = function(phone) {
            phonesService.deletePhone($routeParams.salonid, phone.Id).then(function(result) {
                var index = $scope.salon.Phones.indexOf(phone);
                $scope.salon.Phones.splice(index, 1);
            })
        }
        $scope.changeTime = function(time) {
            var data = {
                Id: time.Id,
                Open: {
                    Hours: time.open.getHours(),
                    Minutes: time.open.getMinutes()
                },
                Close: {
                    Hours: time.close.getHours(),
                    Minutes: time.close.getMinutes()
                },
                DayOfWeekId: time.DayOfWeekId
            };
            salonFactory.changeTime($routeParams.salonid, data).then(function(result) {

            })
        }
        $scope.times = [];
        var fillTimes = function(array) {
            angular.forEach(array, function(time) {
                var open = time.Open.split(':');
                var close = time.Close.split(':');
                var openTime = new Date();
                var closeTime = new Date();
                openTime.setHours(open[0]);
                openTime.setMinutes(open[1]);
                closeTime.setHours(close[0]);
                closeTime.setMinutes(close[1]);
                var workTime = {
                    open: openTime,
                    close: closeTime,
                    name: time.DayOfWeek.Name,
                    DayOfWeekId: time.DayOfWeek.Id,
                    Id: time.Id
                };
                $scope.times.push(workTime);
            });
        }
        $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 10;

        salonFactory.getById($routeParams.salonid).then(function(result) {
            $scope.salon = result;
            fillTimes(result.WorkTimes);
            $scope.$parent.ContentHeader = result.Name;
        }, function(error) {
            alert(error.data.message);
        });


        streetFactory.get().then(function(result) {
            $scope.streets = result;
        })
        $scope.ismeridian = false;
        $scope.openModal = function() {
            var data = {
                salonId: $scope.salon.Id,
                size: {
                    width: 400,
                    height: 300
                }
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/modals/pickLogo-view.html',
                controller: 'LogoModalCtrl',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    data: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function(image) {
                $scope.salon.Image = '';
                var data = {
                    Image: image
                };
                salonFactory.postLogo($scope.salon.Id, data).then(function(guid) {
                    $scope.salon.Image = guid;
                });
            });
        }
        $scope.streetClick = function() {
            streetFactory.post($scope.salon.Id, $scope.salon.Street).then(function() {
                streetFactory.get().then(function(result) {
                    $scope.streets = result;
                })
            })
        }
        $scope.submit = function() {
            if ($scope.salon.Street.Id == null) {
                $scope.salon.Street = {
                    Name: $scope.salon.Street
                };
            }
            salonFactory.post($scope.salon).then(function(result) {
                streetFactory.get().then(function(result) {
                    $scope.streets = result;
                })
            });
        }
    }
]);
