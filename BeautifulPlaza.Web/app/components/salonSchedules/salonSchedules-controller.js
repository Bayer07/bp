'use strict';
app.controller('salonSchedules-controller', ['$scope', 'scheduleService', '$uibModal',
    function($scope, scheduleService, $uibModal) {
        $scope.$parent.ContentHeader = 'Записи клиентов';
        scheduleService.getAllSalonScedules().then(function(result) {
            angular.forEach(result, function(salon) {
                angular.forEach(salon.Schedules, function(schedule) {
                    schedule.Date = new Date(schedule.Date);
                })
            })
            $scope.salons = result;
        });
        $scope.state = [{
            value: 0,
            text: 'В ожидании'
        }, {
            value: 1,
            text: 'Отменен'
        }, {
            value: 2,
            text: 'Выполнен'
        }];
        $scope.change = function(schedule, salon) {
            scheduleService.postSchedule(schedule, salon);
        };
        $scope.open = function(schedule, salon) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/modals/changeDateOfSchedule-view.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    schedule: function() {
                        return schedule;
                    }
                }
            });
            modalInstance.result.then(function(updatedSchedule) {
                scheduleService.postSchedule(updatedSchedule, salon);
            });
        };
        $scope.changeService=function (price) {

        };
    }
]);
