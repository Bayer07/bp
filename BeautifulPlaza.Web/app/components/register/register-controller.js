'use strict';
app.controller('register-controller', ['$scope', 'authService', 'cityFactory', 'streetFactory', '$location',
    function($scope, authService, cityFactory, streetFactory, $location) {

        $scope.$parent.ContentHeader = 'Регистрация';

        $scope.user = {
            FirstName: '',
            SecondName: '',
            PatronomicName: '',
            Email: '',
            DateBirthday: new Date('2000-01-01'),
            Password: '',
            PasswordConfirmation: '',
            Phone: '',
            Role: 'Client',
            FirmName: '',
        };

        $scope.submit = function() {
            authService.saveRegistration($scope.user).then(function(result) {
                $location.path('phoneVerification').search({
                    phone: $scope.user.Phone
                });
            }, function(error) {
                $scope.error = error.data.modelState;
            });
        };

        $scope.options = {
            maxDate: new Date(),
        };

        cityFactory.get().then(function(cities) {
            $scope.cities = cities;
        });

        $scope.cityClick = function() {
            streetFactory.getByCity($scope.user.City.Id).then(function(streets) {
                $scope.streets = streets;
            });
        };
    }
]);
