'use strict';
app.controller('news-controller', ['$scope', 'newsFactory', '$routeParams', function($scope, newsFactory, $routeParams) {
    newsFactory.get().then(function(news) {
        $scope.news = news;
    });
    $scope.pathToImage = function(image) {
        return newsFactory.path(image);
    }
    var postId = $routeParams.article;
    if (postId != undefined) {
        newsFactory.getById(postId).then(function(article) {
            $scope.article = article;
        });
    }
}]);
