'use strict';
app.controller('schedule-controller', ['$scope', '$routeParams', 'salonService', 'serviceService', 'scheduleService', '$location',
    function($scope, $routeParams, salonService, serviceService, scheduleService, $location) {
        $scope.$parent.ContentHeader = 'Желаемое время на Online запись';
        $scope.salonId = $routeParams.salonId;
        var priceId = $routeParams.priceId;
        $scope.date = new Date();
        $scope.date.setHours(8);
        $scope.date.setMinutes(0);
        $scope.options = {
            minDate: new Date()
        };
        salonService.getAddress($scope.salonId).then(function(result) {
            $scope.address = result;
        });
        serviceService.getServiceByPrice(priceId).then(function(result) {
            $scope.service = result.Name;
        });
        $scope.submit = function() {
            var data = {
                SalonId: $scope.salonId,
                PriceId: priceId,
                Date: $scope.date
            };
            scheduleService.submitSchedule(data).then(function(result) {
                $location.path('/schedules')
            })
        }
        scheduleService.isActive($scope.salonId).then(function(result) {
            $scope.haveSchedule = result;
        });
    }
]);
