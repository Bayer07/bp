'use strict';
app.controller('salonServices-controller', ['$scope', '$routeParams', 'salonsService', 'categoryFactory', 'portfolioFactory', 'serviceFactory', 'priceFactory', '$uibModal', 'ngAuthSettings',
    function($scope, $routeParams, salonsService, categoryFactory, portfolioFactory, serviceFactory, priceFactory, $uibModal, ngAuthSettings) {

        var uri = ngAuthSettings.ServiceCDNUri;        
        var salonId = $routeParams.salonid;

        serviceFactory.getServicesBySalon(salonId).then(function(services) {
            $scope.services = services;
        });

        $scope.categoryClick = function(category) {
            if (category.Checked) {
                categoryFactory.postBySalon(salonId, category.Category.Id).then(function(result) {
                    category.CustomServices = [];
                    category.Portfolios = [];
                    category.Id = result.Id;
                });
            } else {
                categoryFactory.deleteBySalon(salonId, category.Category.Id).then(function(result) {
                    category.Services = result;
                    category.CustomServices = [];
                });
            }
        }

        $scope.serviceClick = function(service) {
            if (service.Checked) {
                serviceFactory.post(salonId, service.Service.CategoryId, service.Service.Id).then(function(result) {
                    service.Price = result.Price;
                    service.Id = result.Id;
                });
            } else {
                serviceFactory.delete(salonId, service.Service.CategoryId, service.Service.Id).then(function(result) {
                    service.Price = null;
                });
            }
        }

        $scope.postUserService = function(category) {
            serviceFactory.postUserService(salonId, category.Category.Id, category.service).then(function(result) {
                category.CustomServices.push(result);
                category.service = {
                    Name: '',
                    Price: {
                        Min: 0,
                        Max: 0
                    }
                };
            });
        }

        $scope.updateService = function(service) {
            serviceFactory.put(salonId, service.Service.CategoryId, service).then(function(result) {

            });
        }

        $scope.updateCustomService = function(categoryId, service) {
            serviceFactory.putCustomService(salonId, categoryId, service).then(function(result) {

            });
        }

        $scope.deleteCustomService = function(category, customService) {
            serviceFactory.deleteCustomService(salonId, category.Id, customService.Id).then(function(result) {
                var index = category.CustomServices.indexOf(customService);
                category.CustomServices.splice(index, 1);
            });
        }

        $scope.portfolioPath = function(portfolio) {
            return uri + 'images/portfolio/' + portfolio.Image + '.jpg';
        }

        $scope.pickPortfolio = function(category) {

            var data = {
                size: {
                    width: 300,
                    height: 500
                }
            };

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/modals/pickLogo-view.html',
                controller: 'LogoModalCtrl',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    data: function() {
                        return data;
                    }
                }
            });

            modalInstance.result.then(function(image) {
                portfolioFactory.post(salonId, category.Id, image).then(function(portfolio) {
                    category.Portfolios.push(portfolio);
                });
            });
        }

        $scope.removePortfolio = function(category, portfolio) {
            portfolioFactory.remove(salonId, category.Id, portfolio.Id).then(function() {
              var index = category.Portfolios.indexOf(portfolio);
              category.Portfolios.splice(index, 1);
            });
        }
    }
]);
