app.controller('main-controller', ['$scope', '$http', '$location', 'authService', '$rootScope', 'cityFactory', 'salonFactory', 'categoryFactory', 'localStorageService', '$uibModal', '$filter', 'serviceFactory', 'streetFactory', '$route', 'ngAuthSettings', 'authService', 'bannerFactory', 'supplierFactory',
    function($scope, $http, $location, authService, $rootScope, cityFactory, salonFactory, categoryFactory, localStorageService, $uibModal, $filter, serviceFactory, streetFactory, $route, ngAuthSettings, authService, bannerFactory, supplierFactory) {

        $scope.uri = ngAuthSettings.apiServiceBaseUri;
        $scope.authService = authService;

        bannerFactory.get().then(function(result) {
            $scope.banners = result;
        });

        if (authService.authentication.isAuth && authService.authentication.role == 'Supplier') {
            supplierFactory.get().then(function(response) {
                $scope.supplierFirms = response;
            });
        }

        $scope.salonChoise = function(salon) {
            $location.path('salon/' + salon.Id);
        }
        $scope.city = localStorageService.get('City');
        cityFactory.get().then(function(result) {
            $scope.cities = result;            
        });
        $scope.getCategoryStyle = function(cat) {
            return $scope.searchParam.Category == cat ? 'active' : '';
        }
        $scope.categoryClick = function(category, isSalon, isFreelance) {
            $scope.searchParam.Category = category;
            $scope.searchParam.IsSalon = isSalon;
            $scope.searchParam.IsFreelance = isFreelance;
            if (category != 0) {
                angular.forEach($scope.categories, function(c) {
                    if (c.Id == category) {
                        $rootScope.title = $route.current.title + " - " + c.Name;
                    }
                });
            } else {
                $rootScope.title = $route.current.title;
            }
            if ($location.path() !== '/salons') {
                $location.path('/salons');
            }
        }
        $scope.filterClick = function() {
            $location.path('/salons');
        }
        if ($scope.city == null) {
            ymaps.ready(init);

            function init() {
                var geolocation = ymaps.geolocation;
                geolocation.get({
                    provider: 'yandex',
                    mapStateAutoApply: true
                }).then(function(result) {
                    var cityFound = result.geoObjects.get(0).properties.get('name').toLowerCase();
                    cityFactory.get().then(function(cities) {
                        var found = false;
                        angular.forEach(cities, function(city) {
                            if (city.Name.toLowerCase() == cityFound) {
                                $scope.city = city;
                                localStorageService.set('City', city);
                                found = true;
                            }
                        });
                        if (found == false) {
                            var city = cities.find(function(city) {
                                return city.Id == 1;
                            });
                            $scope.city = city;
                            localStorageService.set('City', city);
                        }
                        streetFactory.getByCity($scope.city.Id).then(function(streets) {
                            $scope.streets = streets;
                        });
                    });
                });
            };
        };

        $scope.choiseCity = function() {
            if ($scope.cities != null) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/components/modals/choiseCity-view.html',
                    controller: 'ChoiseCityController',
                    controllerAs: '$ctrl',
                    size: 'sm',
                    resolve: {
                        cities: function() {
                            return $scope.cities;
                        }
                    }
                });
                modalInstance.result.then(function(city) {
                    $scope.city = city;
                    localStorageService.set('City', city);
                    localStorageService.remove('salons');
                });
            }
        };

        $scope.Title = 'Beauty Plaza ';
        $scope.logOut = function() {
            categoryFactory.get().then(function(categories) {
                $scope.categories = categories;
            });
            authService.logOut();
            $location.path('');
        };

        $scope.authentication = authService.authentication;

        if (authService.authentication.isAuth == false || authService.authentication.role == 'Client') {
            categoryFactory.get().then(function(categories) {
                $scope.categories = categories;
            });
        }

        $scope.loadSalons = function() {
            salonFactory.getByUser().then(function(salons) {
                $scope.userSalons = salons;
            });
        };

        if ($scope.authentication.role == 'Salon') {
            $scope.loadSalons();
        };

        $scope.filterPanelHiden = true;

        salonFactory.getNames().then(function(salonNames) {
            $scope.salonNames = salonNames;
        });

        serviceFactory.getServices().then(function(services) {
            $scope.services = services;
        });

        $rootScope.bodyFilterPanelHidden = false;

        $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute) {
            if ($location.path().split('/')[1] == 'salons') {
                $rootScope.bodyFilterPanelHidden = true;
                if ($scope.searchParam.Category != 0) {
                    angular.forEach($scope.categories, function(c) {
                        if (c.Id == $scope.searchParam.Category) {
                            $rootScope.title = $route.current.title + " - " + c.Name;
                        }
                    });
                } else {
                    $rootScope.title = $route.current.title;
                }
            } else {
                $rootScope.bodyFilterPanelHidden = false;
                $rootScope.title = $route.current.title;
            }
        });

        $scope.searchParam = {
            Price: [0, 10000],
            Name: '',
            Street: '',
            Category: 0,
            IsSalon: true,
            IsFreelance: true,
            Service: ''
        }
    }
]);
