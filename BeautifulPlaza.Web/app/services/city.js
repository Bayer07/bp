app.factory('cityFactory', ['$http', 'ngAuthSettings', '$q', function($http, ngAuthSettings, $q) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var cityFactory = {};

    cityFactory.get = function() {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/city').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    return cityFactory;
}]);
