app.factory('supplierProductFactory', ['$http', 'ngAuthSettings', '$q', function($http, ngAuthSettings, $q) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var supplierProductFactory = {};

    supplierProductFactory.get = function(firmId) {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/supplierproduct/firm/' + firmId).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierProductFactory.addToFirm = function(categoryid, firmid, data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/supplierproduct/' + categoryid + '/' + firmid, data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    supplierProductFactory.removeFromFirm = function(categoryid, firmid, data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/supplierproduct/' + categoryid + '/' + firmid + '/remove', data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    return supplierProductFactory;
}]);
