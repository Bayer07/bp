app.factory('bannerFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var serviceCDNUri = ngAuthSettings.ServiceCDNUri;

    var bannerFactory = {};

    bannerFactory.getBanners = function() {
        return $http.get(serviceBase + 'api/admin/banners').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    bannerFactory.getBySalon = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/banner').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    bannerFactory.update = function(banner) {
        return $http.put(serviceBase + 'api/admin/banners', banner);
    };

    bannerFactory.delete = function(banner) {
        return $http.delete(serviceBase + 'api/admin/banners/' + banner.Id);
    };

    bannerFactory.get = function() {
        return $http.get(serviceBase + 'api/banners').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    bannerFactory.path = function(banner) {
        return serviceCDNUri + 'images/banners/' + banner + '.jpg';
    };

    return bannerFactory;
}]);
