app.factory('portfolioFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var portfolioFactory = {};

    portfolioFactory.remove = function(salonId, categoryId, portfolioId) {
        return $http.delete(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/portfolio/' + portfolioId);
    };

    portfolioFactory.post = function(salonId, categoryId, image) {
        var portfolio = {
            Image: image
        };
        return $http.post(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/portfolio', portfolio).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    return portfolioFactory;
}]);
