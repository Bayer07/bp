app.factory('worktimeFactory', ['$http', '$q', 'ngAuthSettings', function($http, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var worktimeFactory = {};

    worktimeFactory.getDaysOfWeek = function() {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/worktime/daysofweek').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    worktimeFactory.post = function(firmId, worktime) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/worktime/firm/' + firmId, worktime).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    return worktimeFactory;
}]);
