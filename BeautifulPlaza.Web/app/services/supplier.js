app.factory('supplierFactory', ['$http', 'ngAuthSettings', '$q', function($http, ngAuthSettings, $q) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var supplierFactory = {};

    supplierFactory.get = function() {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/supplier').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierFactory.getById = function(firmId) {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/supplier/firm/' + firmId).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierFactory.post = function(firmId, sup) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/supplier/firm/' + firmId, sup).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierFactory.postPhone = function(firmId, phone) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/supplier/firm/' + firmId + '/phone', phone).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    return supplierFactory;
}]);
