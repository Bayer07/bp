app.factory('priceFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var priceFactory = {};
    priceFactory.getSalonPricesByCategory = function(salonId, categoryId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/prices').then(function(result) {
            return JSON.parse([result.data]);
        });
    };
    priceFactory.getSalonPricesByCategoryAlias = function(salonId, category) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/categoryAlias/' + category + '/services/prices').then(function(result) {
            return JSON.parse([result.data]);
        });
    };
    priceFactory.post = function(salonId, categoryId, serviceId) {
        var price = {
            Min: 0,
            Max: 0
        };
        return $http.post(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/service/' + serviceId + '/price', price).then(function(result) {
            return JSON.parse([result.data]);
        });
    };
    priceFactory.put = function(salonId, categoryId, service) {
        var price = {
            Min: service.Min,
            Max: service.Max
        };
        return $http.put(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/service/' + service.Id + '/price', price).then(function() {});
    };
    priceFactory.delete = function(salonId, categoryId, service) {
        var price = {
            Min: service.min,
            Max: service.max
        };
        return $http.delete(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/service/' + service.Id + '/price', price).then(function() {});
    };
    priceFactory.get = function(priceId) {
        return $http.get(serviceBase + 'api/price/' + priceId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };
    return priceFactory;
}]);
