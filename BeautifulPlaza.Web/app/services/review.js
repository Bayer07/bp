app.factory('reviewFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var reviewFactory = {};
    reviewFactory.get = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/review').then(function(result) {
            return result.data.length > 0 ? result.data : '';
        });
    }
    reviewFactory.post = function(salonId, data) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/review', data);
    }
    return reviewFactory;
}]);
