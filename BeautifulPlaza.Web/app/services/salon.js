app.factory('salonFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var salonFactory = {};

    salonFactory.get = function(cityId) {
        return $http.get(serviceBase + 'api/city/' + cityId + '/salons').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    salonFactory.getAddress = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/address/').then(function(result) {
            return result.data;
        });
    };

    salonFactory.getName = function(salonId) {
        return $http.get(serviceBase + 'api/salonName/' + salonId).then(function(result) {
            return result.data;
        });
    };

    salonFactory.getById = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    salonFactory.getByUser = function() {
        return $http.get(serviceBase + 'api/user/salon').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    salonFactory.postLogo = function(salonId, image) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/logo', image).then(function(result) {
            return result.data;
        });
    };
    salonFactory.changeTime = function(salonId, time) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/time', time).then(function(result) {
            return result.data;
        });
    }
    salonFactory.post = function(salon) {
        return $http.post(serviceBase + 'api/salon', salon).then(function(result) {
            return result;
        });
    };

    salonFactory.getNames = function() {
        return $http.get(serviceBase + 'api/salonNames').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    return salonFactory;
}]);
