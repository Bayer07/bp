app.factory('freelanceFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var freelanceFactory = {};

    freelanceFactory.getNames = function() {
        return $http.get(serviceBase + 'api/freelancers').then(function(result) {
            return JSON.parse([result.data]);
        });
    };
    return freelanceFactory;
}]);
