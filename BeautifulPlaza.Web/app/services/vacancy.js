app.factory('vacancyFactory', ['$http', '$q', 'ngAuthSettings', function($http, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var vacancyFactory = {};

    vacancyFactory.get = function() {
        return $http.get(serviceBase + 'api/vacancy').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    vacancyFactory.getNames = function() {
        return $http.get(serviceBase + 'api/vacancynames').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    vacancyFactory.getByAdmin = function() {
        return $http.get(serviceBase + 'api/admin/vacancy').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    vacancyFactory.getExpirience = function() {
        return $http.get(serviceBase + 'api/vacancy/experience').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    vacancyFactory.postExperience = function(e) {
        return $http.post(serviceBase + 'api/vacancy/experience', e).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    vacancyFactory.getNamesByAdmin = function() {
        return $http.get(serviceBase + 'api/admin/vacancynames').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    vacancyFactory.put = function(v) {
        return $http.post(serviceBase + 'api/admin/vacancy', v);
    };

    vacancyFactory.putVacancyName = function(v) {
        return $http.post(serviceBase + 'api/admin/vacancyname', v);
    };

    vacancyFactory.getByUser = function() {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/user/vacancy').success(function(response) {
            deferred.resolve(JSON.parse([response]));
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    vacancyFactory.getByFirm = function(firmId) {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/salon/' + firmId + '/vacancy').success(function(response) {
            deferred.resolve(JSON.parse([response]));
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    return vacancyFactory;
}]);
