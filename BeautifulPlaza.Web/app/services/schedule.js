app.factory('scheduleService', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var scheduleFactory = {};

    scheduleFactory.getScheduleRequests = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/scheduleRequests').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    scheduleFactory.getAllSchedules = function() {
        return $http.get(serviceBase + 'api/schedules').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    scheduleFactory.getScheduleCost = function() {
        return $http.get(serviceBase + 'api/scheduleCost').then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    scheduleFactory.submit = function(salonId) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/scheduleRequest').then(function(result) {
            return JSON.parse([result.data]);
        })
    };

    scheduleFactory.update = function(schedule) {
        return $http.put(serviceBase + 'api/schedules', schedule).then(function(results) {
            return JSON.parse([results.data]);
        })
    };

    scheduleFactory.delete = function(schedule) {
        return $http.delete(serviceBase + 'api/schedules/' + schedule.Id);
    };

    scheduleFactory.post = function(schedule) {
        var data = {
            PaidServiceSchedule: {
                StartDate: schedule.StartDate,
                EndDate: schedule.EndDate,
                IsActive: schedule.IsActive
            },
            SalonId: schedule.salon.Id
        };
        return $http.post(serviceBase + 'api/schedules', data).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    scheduleFactory.isActive = function(salonid) {
        return $http.get(serviceBase + 'api/schedules/salon/' + salonid).then(function(results) {
            return JSON.parse([results.data]);
        });
    };

    scheduleFactory.submitSchedule = function(schedule) {
        return $http.post(serviceBase + 'api/schedule', schedule).then(function(result) {
            return JSON.parse([result.data]);
        });
    }
    scheduleFactory.getUserSchedules = function() {
        return $http.get(serviceBase + 'api/schedule').then(function(result) {
            return JSON.parse([result.data]);
        });
    }

    scheduleFactory.getSalonScedules = function(salonId) {
        return $http.get(serviceBase + 'api/schedule/salon/' + salonId).then(function(result) {
            return JSON.parse([result.data]);
        });
    }

    scheduleFactory.getAllSalonScedules = function() {
        return $http.get(serviceBase + 'api/schedule/salon').then(function(result) {
            return JSON.parse([result.data]);
        });
    }

    scheduleFactory.postSchedule = function(schedule, salon) {
        return $http.put(serviceBase + 'api/schedule/salon/' + salon.Id, schedule).then(function(result) {});
    }

    return scheduleFactory;
}]);
