app.factory('hotPormotionsFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var hotPormotionsFactory = {};

    hotPormotionsFactory.get = function() {
        return $http.get(serviceBase + 'api/hotpromotions').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    hotPormotionsFactory.getAll = function() {
        return $http.get(serviceBase + 'api/hotpromotions/all').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    hotPormotionsFactory.path = function(hot) {
        return ngAuthSettings.ServiceCDNUri + 'images/hotpromotions/' + hot + '.jpg';
    };

    hotPormotionsFactory.post = function(promotion) {
        return $http.post(serviceBase + 'api/hotpromotions', promotion).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    hotPormotionsFactory.upload = function(image) {
        return $http.post(serviceBase + 'api/hotpromotions', image).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    return hotPormotionsFactory;
}]);
