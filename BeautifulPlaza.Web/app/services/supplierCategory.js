app.factory('supplierCategoryFactory', ['$http', 'ngAuthSettings', '$q', function($http, ngAuthSettings, $q) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var supplierCategoryFactory = {};

    supplierCategoryFactory.get = function() {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/suppliercategory').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierCategoryFactory.getFirms = function() {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/suppliercategory/firms').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };


    supplierCategoryFactory.post = function(data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory', data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierCategoryFactory.delete = function(data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory/delete', data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierCategoryFactory.change = function(data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory/put', data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierCategoryFactory.path = function(category) {
        return ngAuthSettings.ServiceCDNUri + 'images/suppliercategories/' + category + '.jpg';
    };

    supplierCategoryFactory.addProduct = function(categoryid, data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory/' + categoryid + '/addproduct', data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierCategoryFactory.changeProduct = function(categoryid, data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory/' + categoryid + '/changeproduct', data).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    supplierCategoryFactory.addToFirm = function(categoryid, firmid) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory/' + categoryid + '/' + firmid).success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    supplierCategoryFactory.deleteFromFirm = function(categoryid, firmid) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/suppliercategory/' + categoryid + '/' + firmid + '/delete').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    return supplierCategoryFactory;
}]);
