app.factory('streetFactory', ['$http', 'ngAuthSettings', '$q', function($http, ngAuthSettings, $q) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var streetFactory = {};

    streetFactory.get = function() {
        return $http.get(serviceBase + 'api/street').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    streetFactory.getById = function(streetId) {
        return $http.get(serviceBase + 'api/street/' + streetId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    streetFactory.getByCity = function(cityId) {
        return $http.get(serviceBase + 'api/city/' + cityId + '/street').then(function(result) {
            return JSON.parse([result.data]);
        });
    };    

    streetFactory.getByCityNew = function(cityId) {
        var deferred = $q.defer();
        $http.get(serviceBase + 'api/city/' + cityId + '/streetnew').success(function(response) {
            deferred.resolve(response);
        }).error(function(err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    streetFactory.post = function(salonId, street) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/street', street).then(function(result) {
            return result;
        });
    };

    return streetFactory;
}]);
