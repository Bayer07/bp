app.factory('categoryFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var categoryFactory = {};

    categoryFactory.get = function() {
        return $http.get(serviceBase + 'api/categories').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    categoryFactory.path = function(category) {
        return ngAuthSettings.ServiceCDNUri + 'images/categories/' + category + '.jpg';
    };

    categoryFactory.post = function(category) {
        return $http.post(serviceBase + 'api/categories', category).then(function(result) {
            return JSON.parse([result.data.message]);
        });
    }

    categoryFactory.putImage = function(category) {
        return $http.put(serviceBase + 'api/categories/image', category).then(function(result) {
            return JSON.parse([result.data.message]);
        });
    }

    categoryFactory.put = function(category) {
        return $http.put(serviceBase + 'api/categories', category);
    }

    categoryFactory.delete = function(category) {
        return $http.delete(serviceBase + 'api/categories/' + category);
    }

    categoryFactory.getBySalon = function(id) {
        return $http.get(serviceBase + 'api/salon/' + id + '/categories').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    categoryFactory.postBySalon = function(salonId, categoryId) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    categoryFactory.deleteBySalon = function(salonId, categoryId) {
        return $http.delete(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    return categoryFactory;
}]);
