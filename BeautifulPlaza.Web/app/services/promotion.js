app.factory('promotionFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var serviceCDNUri = ngAuthSettings.ServiceCDNUri;
    var promotionFactory = {};
    promotionFactory.getByCategory = function(category) {
        return $http.get(serviceBase + 'api/category/' + category + '/promotion').then(function(result) {
            return JSON.parse([result.data]);
        });
    }

    promotionFactory.submit = function(salonId, data) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/promotion', data).then(function(results) {
            return JSON.parse([results.data]);
        })
    }

    promotionFactory.path = function(promotion) {
        return serviceCDNUri + 'images/promotions/' + promotion + '.jpg';
    };

    return promotionFactory;
}]);
