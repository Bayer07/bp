app.factory('serviceFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var serviceFactory = {};

    serviceFactory.getServiceByPrice = function(priceId) {
        return $http.get(serviceBase + 'api/service/price/' + priceId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.getServicesBySalon = function(salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/services').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.getServicesByCategory = function(category) {
        return $http.get(serviceBase + 'api/categories/' + category + '/services').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.getServices = function() {
        return $http.get(serviceBase + 'api/services').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.getServicesByCategory = function(categoryId, salonId) {
        return $http.get(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/services').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.delete = function(salonId, categoryId, serviceId) {
        return $http.delete(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/service/' + serviceId);
    };

    serviceFactory.post = function(salonId, categoryId, serviceId) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/service/' + serviceId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.postUserService = function(salonId, categoryId, service) {
        return $http.post(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/userService', service).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.put = function(salonId, categoryId, service) {
        return $http.put(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/service', service);
    };

    serviceFactory.putCustomService = function(salonId, categoryId, service) {
        return $http.put(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/customService', service).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    serviceFactory.deleteCustomService = function(salonId, categoryId, serviceId) {
        return $http.delete(serviceBase + 'api/salon/' + salonId + '/category/' + categoryId + '/customService/' + serviceId).then(function(result) {
            return result;
        });
    };

    return serviceFactory;
}]);
