app.factory('newsFactory', ['$http', 'ngAuthSettings', function($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var newsFactory = {};

    newsFactory.get = function() {
        return $http.get(serviceBase + 'api/news').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    newsFactory.getById = function(articleId) {
        return $http.get(serviceBase + 'api/news/' + articleId).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    newsFactory.getAll = function() {
        return $http.get(serviceBase + 'api/news/all').then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    newsFactory.post = function(title) {
        return $http.post(serviceBase + 'api/news', title).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    newsFactory.path = function(image) {
        return ngAuthSettings.ServiceCDNUri + 'images/news/' + image + '.jpg';
    }

    newsFactory.postImage = function(newsId, image) {
        return $http.post(serviceBase + 'api/news/' + newsId + '/image', image).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    newsFactory.put = function(title) {
        return $http.put(serviceBase + 'api/news', title);
    };

    newsFactory.postTitleImage = function(newsId, image) {
        return $http.post(serviceBase + 'api/news/' + newsId + '/titleImage', image).then(function(result) {
            return JSON.parse([result.data]);
        });
    };

    newsFactory.deleteImage = function(newsId, imageId) {
        return $http.delete(serviceBase + 'api/news/' + newsId + '/image/' + imageId);
    };

    return newsFactory;
}]);
