﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;

namespace BP
{
    public class Image
    {
        public static string Upload(string path, string image)
        {
            string[] base64 = image.Split(',');
            var appSettings = ConfigurationManager.AppSettings;
            string ftpUsername = appSettings["cdnUser"];
            string ftpPassword = appSettings["cdnPassword"];
            string serverUri = appSettings["CDNUri"];

            var wr = WebRequest.Create(serverUri + '/' + path);
            FtpWebRequest request = (FtpWebRequest)wr;

            request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

            request.Method = WebRequestMethods.Ftp.UploadFile;
            byte[] fileContents = Convert.FromBase64String(base64[1]);
            request.ContentLength = fileContents.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            return response.StatusDescription;
        }
        
        public static string Delete(string path)
        {
            var appSettings = ConfigurationManager.AppSettings;
            string ftpUsername = appSettings["cdnUser"];
            string ftpPassword = appSettings["cdnPassword"];
            string serverUri = appSettings["CDNUri"];
            var baseUri = new Uri(serverUri);
            var uri = new Uri(baseUri, path);
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uri);

            request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

            request.Method = WebRequestMethods.Ftp.DeleteFile;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();            
            response.Close();
            return response.StatusDescription;
        }
        public static string GenerateGuid(List<string> guids)
        {
            while (true)
            {
                string guid = Guid.NewGuid().ToString();
                var result = guids.Where(x => x == guid);
                if (result.Count() == 0)
                {
                    return guid;
                }
            }
        }

        public static string ConvertToString(FileStream file)
        {
            var bytes = imageToByteArray(System.Drawing.Image.FromStream(file));
            return "data:image/jpeg;base64," + Convert.ToBase64String(bytes);
        }

        private static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            Encoder myEncoder = Encoder.Quality;
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            imageIn.Save(ms, jpgEncoder, myEncoderParameters);
            return ms.ToArray();
        }
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }
}