﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BP.Controllers
{
    public class PhonesController : ApiController
    {
        private BPModel _model = BPModel.Create();        

        [HttpPost]
        [Route("api/phones/{salonId}")]
        [Authorize(Roles = "Salon, Freelance")]
        public async Task<HttpResponseMessage> Post(int salonId, PhoneViewModel phone)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = await _model.Users.Include("Firms.Phones").FirstAsync(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var newPhone = new Phone() { Number = phone.Number };
                firm.Phones.Add(newPhone);
                await _model.SaveChangesAsync();
                string phoneJson = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(newPhone));
                return Request.CreateResponse(HttpStatusCode.OK, phoneJson, "application/json");

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPut]
        [Route("api/phones/{salonId}")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage Put(int salonId, PhoneViewModel phone)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Phones").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                int id = (int)phone.Id;
                var existPhone = firm.Phones.First(x => x.Id == id);
                existPhone.Number = phone.Number;
                _model.Entry(existPhone).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpDelete]
        [Route("api/phones/{salonId}/{phoneId}")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage Delete(int salonId, int phoneId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.Phones").First(x => x.Id == userId);
            var firm = user.Firms.First(x => x.Id == salonId);           
            var phone = firm.Phones.First(x => x.Id == phoneId);
            firm.Phones.Remove(phone);
            _model.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
    public class PhoneViewModel
    {
        public int? Id { get; set; }
        public string Number { get; set; }
    }
}