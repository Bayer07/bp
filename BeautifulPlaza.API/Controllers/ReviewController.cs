﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace BP.Controllers
{
    public class ReviewController : ApiController
    {
        private BPModel _model = BPModel.Create();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        [Route("api/salon/{salonId}/review")]
        public HttpResponseMessage Get(int salonId)
        {
            var salon = _model.Firms.Include("Reviews").First(x => x.Id == salonId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(salon.Reviews.Select(x =>
                    new ReviewViewModel() { Rating = x.Rating, Text = x.Text, User = UserManager.FindById(x.UserId).Name.Split(' ').First() })))
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpPost]
        [Route("api/salon/{salonId}/review")]
        [Authorize(Roles = "Client, Administrator")]
        public HttpResponseMessage Post(int salonId, PostReviewViewModel review)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var salon = _model.Firms.Include("Reviews").First(x => x.Id == salonId);
                var newReview = new Review(review.Text, review.Rating, userId);
                salon.Reviews.Add(newReview);
                _model.SaveChanges();
                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(
                        JsonConvert.SerializeObject(
                        new ReviewViewModel() { Text = newReview.Text, Rating = newReview.Rating, User = UserManager.FindById(newReview.UserId).Name.Split(' ').First() }))
                };
                resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return resp;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

        }
    }
    public class ReviewViewModel
    {
        public string Text { get; set; }
        public decimal Rating { get; set; }
        public string User { get; set; }
    }
    public class PostReviewViewModel
    {
        [MaxLength(300, ErrorMessage = "Длинна не должна превышать 300 символов.")]
        public string Text { get; set; }
        public decimal Rating { get; set; }
    }
}