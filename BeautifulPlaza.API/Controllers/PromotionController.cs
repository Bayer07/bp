﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class PromotionController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/category/{category}/promotion")]
        public HttpResponseMessage Get(string category)
        {
            try
            {
                var promotions = _model.Promotions.Include("Categories.Category").Where(x => x.Categories.Any(y => y.Category.Alias == category));
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(promotions.ToList()));
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("api/salon/{salonId}/promotion")]
        public string GetSalonPaidServicePromotion(int salonId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.Promotions").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            return JsonConvert.SerializeObject(salon.Promotions);
        }

        [HttpPost]
        [Route("api/salon/{salonId}/promotion")]
        public HttpResponseMessage Promotion(int salonId,PromotionViewModel promotion)
        {
            string guid = Image.GenerateGuid(_model.Promotions.Select(x => x.Image).ToList());
            var appSettings = ConfigurationManager.AppSettings;
            string promotionsPath = appSettings["PromotionsPath"];
            try
            {
                Image.Upload(promotionsPath + guid + ".jpg", promotion.Image);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            try
            {
                var userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Promotions").First(x => x.Id == userId);
                var firm = _model.Firms.First(x => x.Id == salonId);
                if (promotion.Categories.Count == 0)
                    throw new Exception("Категории не выбраны");
                var now = DateTime.Now;
                var p = new Promotion()
                {
                    StartDate = now,
                    EndDate = now,
                    Cost = _model.PromotionCost.ToList().Last(),
                    Image = guid,
                    IsActive = false,
                    Description = promotion.Description,
                    Message = promotion.Message,
                    Title = promotion.Title,
                    Weight = 0,
                    IsNeedDesigner = promotion.NeedDesigner
                };
                if (promotion.NeedDesigner)
                {
                    p.DesignerCost = _model.PromotionDesignerCost.ToList().Last();
                }
                firm.Promotions.Add(p);
                foreach (Category category in promotion.Categories)
                {
                    var cate = _model.Categories.Find(category.Id);
                    var c = new PromotionInCategories() { Category = cate };
                    p.Categories.Add(c);
                }
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(p));
            }
            catch (Exception e)
            {
                Image.Delete(promotionsPath + guid + ".jpg");
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            } 
        }
        public class PromotionViewModel
        {
            public List<Category> Categories { get; set; }
            public bool NeedDesigner { get; set; }
            public string Image { get; set; }
            public string Message { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
        }
    }
}