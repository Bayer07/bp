﻿using BP.Models;
using Newtonsoft.Json;
using System.Linq;
using System.Data.Entity;
using System.Web.Http;
using System;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace BP.Controllers
{
    public class PricesController : ApiController
    {
        BPModel _model = BPModel.Create();

        //public string Post([FromBody]PricesViewModel value)
        //{
        //    var salon = _model.Salons.Include("Categories.Category").Include("Categories.Prices").Include("Categories.Prices.Service").First(x => x.Id == value.SalonId);
        //    var category = salon.Categories.First(x => x.Category.Alias == value.Category);
        //    return JsonConvert.SerializeObject(category.Prices);
        //}

        [HttpGet]
        [Route("api/price/{priceId}")]
        public string Get(int priceId)
        {
            var price = _model.Prices.Include("Service").First(x => x.Id == priceId);
            return JsonConvert.SerializeObject(price);
        }

        [HttpGet]
        [Route("api/salon/{salonId}/category/{categoryId}/services/prices")]
        public async System.Threading.Tasks.Task<string> GetSalonPricesByCategory(int salonId, int categoryId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Salons.Categories.Services.Service").Include("Salons.Categories.Services.Price").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            var category = salon.Categories.First(x => x.CategoryId == categoryId);
            return JsonConvert.SerializeObject(category.Services);
        }

        [HttpGet]
        [Route("api/salon/{salonId}/categoryAlias/{categoryAlias}/services/prices")]
        public async Task<string> GetSalonPricesByCategoryAlias(int salonId, string categoryAlias)
        {
            try
            {                
                var salon = _model.Firms.Include("Categories.Services.Service").Include("Categories.Services.Price").Include("Categories.Category").First(x => x.Id == salonId);
                var category = salon.Categories.First(x => x.Category.Alias == categoryAlias);
                return JsonConvert.SerializeObject(category.Services);
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        [Authorize(Roles = "Salon")]
        [Route("api/salon/{salonId}/category/{categoryId}/service/{serviceId}/services/price")]
        public string PostServiceToSalon(int salonId, int categoryId, int serviceId, Price price)
        {
            //string userId = User.Identity.GetUserId();
            //var user = _model.Users.Include("Salons.Categories.Prices.Service").Include("Salons.Categories.Category").First(x => x.Id == userId);
            //var salon = user.Firms.First(x => x.Id == salonId);
            //var category = salon.Categories.First(x => x.Category.Id == categoryId);
            //try
            //{
            //    var p = category.Services.First(x => x.Service.Id == serviceId);
            //    return "";
            //}
            //catch
            //{
            //    var s = _model.Services.Find(serviceId);
            //    var p = new Price(price.Min, price.Max, s);
            //    category.Prices.Add(p);
            //    _model.Entry(category).State = EntityState.Modified;
            //    _model.SaveChanges();
            //    return JsonConvert.SerializeObject(p);
            //}            
            return "";
        }

        [HttpPut]
        [Authorize(Roles = "Salon")]
        [Route("api/salon/{salonId}/category/{categoryId}/service/{serviceId}/price")]
        public string PutServiceToSalon(int salonId, int categoryId, int serviceId, Price price)
        {
            //string userId = User.Identity.GetUserId();
            //var user = _model.Users.Include("Salons.Categories.Prices").First(x => x.Id == userId);
            //var salon = user.Firms.First(x => x.Id == salonId);
            //var category = salon.Categories.First(x => x.CategoryId == categoryId);
            //var service = _model.Services.Find(serviceId);
            //var p = category.Prices.First(x => x.Service == service);
            //p.Max = price.Max;
            //p.Min = price.Min;
            //_model.Entry(p).State = EntityState.Modified;
            //_model.SaveChanges();
            //return JsonConvert.SerializeObject(p);
            return "";
        }

        [HttpDelete]
        [Authorize(Roles = "Salon")]
        [Route("api/salon/{salonId}/category/{categoryId}/service/{serviceId}/price")]
        public string DeleteServiceFromSalon(int salonId, int categoryId, int serviceId, Price price)
        {
            //string userId = User.Identity.GetUserId();
            //var user = _model.Users.Include("Salons.Categories.Prices").First(x => x.Id == userId);
            //var salon = user.Firms.First(x => x.Id == salonId);
            //var category = salon.Categories.First(x => x.CategoryId == categoryId);
            //var service = _model.Services.Find(serviceId);
            //var p = category.Prices.First(x => x.Service == service);
            //_model.Prices.Remove(p);
            //_model.SaveChanges();
            return "";
        }
    }
}