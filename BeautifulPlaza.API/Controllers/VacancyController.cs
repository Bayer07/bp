﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class VacancyController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/vacancy")]
        public HttpResponseMessage Get()
        {
            try
            {
                var now = DateTime.Now;
                var firms = _model.Firms.Include("Vacancies.Experience").Include("Vacancies.City").Include("Vacancies.Cost").Include("Vacancies.Email").Include("Vacancies.Phone").Include("Vacancies.Street").Include("Vacancies.VacancyName");
                var f = GetFirms(firms);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(f));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        private IEnumerable<Firm> GetFirms(IEnumerable<Firm> list)
        {
            foreach (var item in list)
            {
                item.Vacancies = GetVacancies(item.Vacancies).ToList();
                if (item.Vacancies.Count > 0)
                    yield return item;
            }
        }

        private IEnumerable<Vacancy> GetVacancies(IEnumerable<Vacancy> list)
        {
            var now = DateTime.Now;
            foreach (var item in list)
            {
                if (item.IsActive && item.StartDate < now && item.EndDate > now)
                    yield return item;
            }
        }

        [HttpGet]
        [Route("api/user/vacancy")]
        public HttpResponseMessage GetByUser()
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Vacancies.Experience").Include("Firms.Vacancies.Cost").Include("Firms.Vacancies.City").Include("Firms.Vacancies.Email").Include("Firms.Vacancies.Phone").Include("Firms.Vacancies.Street").Include("Firms.Vacancies.VacancyName").First(x => x.Id == userId);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(user.Firms.Select(x => new { Firm = x.Name, Vacancies = x.Vacancies })));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            
        }
        [HttpGet]
        [Route("api/salon/{salonId}/vacancy")]
        public string GetSalonPaidServiceVacancy(int salonId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.Vacancies.Cost").Include("Firms.Vacancies.City").Include("Firms.Vacancies.Email").Include("Firms.Vacancies.Phone").Include("Firms.Vacancies.Street").Include("Firms.Vacancies.VacancyName").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            var vacancies = salon.Vacancies;
            return JsonConvert.SerializeObject(vacancies);
        }

        [HttpGet]
        [Route("api/vacancy/experience")]
        public HttpResponseMessage GetExperience()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(_model.VacancyWorkExperience));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("api/vacancy/experience")]
        public HttpResponseMessage PostExperience(VacancyWorkExperience e)
        {
            try
            {
                var exp = new VacancyWorkExperience() { Text = e.Text };
                _model.VacancyWorkExperience.Add(exp);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(exp));
            }
            catch (Exception exeption)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exeption);
            }
        }

        [HttpGet]
        [Route("api/vacancynames")]
        public HttpResponseMessage GetVacancyNames()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(_model.VacancyNames.Where(x => x.IsActive == true)));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("api/admin/vacancynames")]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage GetVacancyNamesByAdmin()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(_model.VacancyNames));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        [Route("api/admin/vacancy")]
        public HttpResponseMessage GetByAdmin()
        {
            try
            {
                var firms = _model.Firms.Include("Vacancies.Experience").Include("Vacancies.City").Include("Vacancies.Cost").Include("Vacancies.Email").Include("Vacancies.Phone").Include("Vacancies.Street").Include("Vacancies.VacancyName").ToList();
                var f = firms.Where(x => x.Vacancies.Count > 0);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(f));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [Route("api/admin/vacancy")]
        public HttpResponseMessage Post(Vacancy v)
        {
            try
            {
                var vac = _model.Vacancies.Include("Phone").Include("City").Include("Cost").Include("Email").Include("Street").Include("VacancyName").First(x => x.Id == v.Id);
                vac.StartDate = v.StartDate;
                vac.EndDate = v.EndDate;
                vac.IsActive = v.IsActive;
                vac.Experience = _model.VacancyWorkExperience.Find(v.Experience.Id);
                _model.Entry(vac).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [Route("api/admin/vacancyname")]
        public HttpResponseMessage PostVacancyName(VacancyName v)
        {
            try
            {
                var vac = _model.VacancyNames.Find(v.Id);
                vac.IsActive = v.IsActive;
                vac.Name = v.Name;
                _model.Entry(vac).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("api/salon/{salonId}/vacancy")]
        public IHttpActionResult Vacancy(int salonId, Vacancy vacancy)
        {
            string userId = User.Identity.GetUserId();

            var user = _model.Users.Include("Firms.Vacancies").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);

            VacancyName vn = null;
            if (vacancy.VacancyName.Id == 0)
            {
                try
                {
                    vn = new VacancyName(vacancy.VacancyName.Name, userId);
                    vn.CreatorId = userId;
                    _model.VacancyNames.Add(vn);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.GetBaseException().Message);
                }
            }
            else
            {
                vn = _model.VacancyNames.Find(vacancy.VacancyName.Id);
            }
            City city = vacancy.City.Id == 0 ? new City(vacancy.VacancyName.Name, userId) : _model.Cities.Find(vacancy.City.Id);
            VacancyCost cost = _model.VacancyCost.Find(vacancy.Cost.Id);
            Street street = vacancy.Street.Id == 0 ? new Street(vacancy.Street.Name, city.Id, userId) : _model.Streets.Find(vacancy.Street.Id);
            var newVacancy = new Vacancy() { VacancyName = vn, City = city, ContactName = vacancy.ContactName, Cost = cost, Email = vacancy.Email, House = vacancy.House, MaxPayment = vacancy.MaxPayment, MinPayment = vacancy.MinPayment, Phone = vacancy.Phone, Street = street, Text = vacancy.Text };
            if (vacancy.Percent != null)
                newVacancy.Percent = vacancy.Percent;

            salon.Vacancies.Add(vacancy);
            try
            {
                _model.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
            return Ok(JsonConvert.SerializeObject(vacancy));
        }

        [HttpGet]
        [Route("api/vacancyCost")]
        public string GetPaidServiceVacancyCost()
        {
            return JsonConvert.SerializeObject(_model.VacancyCost.ToList().Last());
        }
    }

    public class VacancyPostViewModel
    {

    }
}