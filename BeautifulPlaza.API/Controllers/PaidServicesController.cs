﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BP.Controllers
{
    public class PaidServicesController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/salon/{salonId}/banner")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage GetSalonPaidServiceBanner(int salonId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Banners.Cost").Include("Firms.Banners.DesignerCost").First(x => x.Id == userId);
                var salon = user.Firms.First(x => x.Id == salonId);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(salon.Banners));
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }           

        [Authorize(Roles = "Administrator, Moderator")]
        [HttpGet]
        [Route("api/schedules")]
        public string GetSchedules()
        {
            return JsonConvert.SerializeObject(_model.ScheduleRequests.Include("Cost"));
        }

        [Authorize(Roles = "Administrator, Moderator")]
        [HttpPost]
        [Route("api/schedule")]
        public string PostSchedule(ScheduleRequest schedule)
        {
            _model.ScheduleRequests.Add(schedule);
            _model.SaveChanges();
            return JsonConvert.SerializeObject(schedule);
        }

        [Authorize(Roles = "Administrator, Moderator")]
        [HttpPut]
        [Route("api/schedules")]
        public async Task<string> PutSchedule(ScheduleRequest schedule)
        {
            _model.Entry(schedule).State = EntityState.Modified;
            await _model.SaveChangesAsync();
            return JsonConvert.SerializeObject(schedule);
        }

        [Authorize(Roles = "Administrator, Moderator")]
        [HttpDelete]
        [Route("api/schedules/{id}")]
        public string DeleteSchedule(int id)
        {
            return "";
            //try
            //{
            //    var schedules = _model.SalonInPaidServices.First(x => x.PaidServiceSchedule != null && x.PaidServiceSchedule.Id == id);
            //    _model.SalonInPaidServices.Remove(schedules);
            //    _model.SaveChanges();
            //    return JsonConvert.SerializeObject(schedules);
            //}
            //catch (Exception)
            //{
            //    return "";
            //}
        }

        [HttpGet]
        [Route("api/scheduleCost")]
        public string GetPaidServiceScheduleCost()
        {
            var cost = _model.ScheduleCost.ToList();
            return JsonConvert.SerializeObject(cost.Last());
        }

        [HttpGet]
        [Route("api/promotionCost")]
        public string GetPaidServicePromotionCost()
        {
            var cost = _model.PromotionCost.ToList();
            return JsonConvert.SerializeObject(cost.Last());
        }

        [HttpGet]
        [Route("api/bannerCost")]
        public string GetPaidServiceBannerCost()
        {
            var cost = _model.BannerCost.ToList();
            return JsonConvert.SerializeObject(cost.Last());
        }

        [HttpGet]
        [Route("api/bannerDesignerCost")]
        public string GetPaidServiceBannerDesignerCost()
        {
            var cost = _model.BannerDesignerCost.ToList();
            return JsonConvert.SerializeObject(cost.Last());
        }

        [HttpGet]
        [Route("api/protomionDesignerCost")]
        public string GetPaidServiceProtomionDesignerCost()
        {
            var cost = _model.PromotionDesignerCost.ToList();
            return JsonConvert.SerializeObject(cost.Last());
        }      
    }
}