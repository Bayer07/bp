﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BP.Controllers
{
    public class SalonInfoController : ApiController
    {
        private BPModel _model = BPModel.Create();

        private User GetUser()
        {
            var user = _model.Users.Find(User.Identity.GetUserId());
            return user;
        }                

        public class PostImageLogoViewModel
        {
            public int salonId { get; set; }
        }

        [Route("api/saloninfo/postformdata/{id}")]
        [Authorize(Roles = "Salon")]
        public async Task<HttpResponseMessage> PostImageLogo(int id)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg.");
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            var user = _model.Users.Find(User.Identity.GetUserId());
                            var salon = await _model.Firms.FindAsync(id);
                            var filePath = HttpContext.Current.Server.MapPath("~/assets/img/salons/");
                            if (salon.Image != null)
                            {
                                var oldFile = new FileInfo(filePath + salon.Image + ".jpg");
                                try
                                {
                                    oldFile.Delete();
                                }
                                catch (Exception)
                                {
                                    var message = string.Format("Не удалось удалить файл.");
                                    return Request.CreateErrorResponse(HttpStatusCode.Created, message); ;
                                }
                            }
                            salon.Image = Guid.NewGuid().ToString();
                            try
                            {
                                postedFile.SaveAs(filePath + salon.Image + ".jpg");
                                _model.Entry(salon).State = EntityState.Modified;
                                _model.SaveChanges();
                            }
                            catch (HttpException)
                            {
                                var message = string.Format("Не удалось сохранить файл.");
                                return Request.CreateErrorResponse(HttpStatusCode.Created, message); ;
                            }

                        }
                    }
                    var message1 = string.Format("Image Updated Successfully.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }        

        [HttpGet]
        [Route("api/salonName/{salonId}")]
        public string GetName(int salonId)
        {
            return _model.Firms.Find(salonId).Name;
        }

        [HttpGet]
        [Route("api/schedules/salon/{salonId}")]
        public bool IsActive(int salonId)
        {
            try
            {
                var now = DateTime.Now;
                var salon = _model.Firms.Include("Promotions").First(x => x.Id == salonId);
                return salon.Promotions.Any(x => x.StartDate >= now && x.EndDate <= now && x.IsActive);
            }
            catch
            {
                return false;
            }
        }
        [HttpGet]
        [Route("api/salon/{salonId}/address")]
        public string GetAddress(int salonId)
        {
            var salon = _model.Firms.Find(salonId);
            StringBuilder sb = new StringBuilder();
            //if (!string.IsNullOrEmpty(salon.Street))
            //    sb.Append("ул." + salon.Street);
            if (!string.IsNullOrEmpty(salon.House))
                sb.Append(" д." + salon.House);
            if (!string.IsNullOrEmpty(salon.Floor))
                sb.Append(" эт." + salon.Floor);
            if (!string.IsNullOrEmpty(salon.Office))
                sb.Append(" оф." + salon.Office);
            return sb.ToString();
        }
    }
}