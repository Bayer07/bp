﻿using BP.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    [Authorize(Roles = "Supplier")]
    [RoutePrefix("api/supplierproduct")]
    public class SupplierProductController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [Route("{categoryid:int}/{firmid:int}")]
        [HttpPost]
        public HttpResponseMessage AddToFirm(int categoryid, int firmid, SupplierProductViewModel prod)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.Categories.Category").Include("Suppliers.Categories.SupplierProductions").First(x => x.Id == userId);
                var firm = user.Suppliers.First(x => x.Id == firmid);
                var category = firm.Categories.First(x => x.Category.Id == categoryid);
                SupplierProduct product = null;
                if (prod.id == null)
                {
                    product = new SupplierProduct(prod.name, userId, category.Id);
                    category.SupplierProductions.Add(product);
                }
                else
                {
                    product = _model.SupplierCategories.Include("SupplierProducts").First(x => x.Id == categoryid).SupplierProducts.First(x => x.Id == prod.id);
                    category.SupplierProductions.Add(product);
                }
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, product);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("{categoryid:int}/{firmid:int}/remove")]
        [HttpPost]
        public HttpResponseMessage RemoveFromFirm(int categoryid, int firmid, SupplierProductViewModel prod)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.Categories.Category").Include("Suppliers.Categories.SupplierProductions").First(x => x.Id == userId);
                var category = user.Suppliers.First(x => x.Id == firmid).Categories.First(x => x.Category.Id == categoryid);
                var product = category.SupplierProductions.First(x => x.Id == prod.id);
                category.SupplierProductions.Remove(product);
                var products = _model.SupplierProducts.Where(x => x.Id == prod.id);
                if (products.Count() == 1)
                {
                    _model.SupplierProducts.Remove(product);
                }
                _model.SaveChanges();                
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("category/{categoryId:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(int categoryId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _model.SupplierCategories.Include("SupplierProducts").First(x => x.Id == categoryId).SupplierProducts);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("firm/{firmId:int}")]
        [HttpGet]
        public HttpResponseMessage GetByFirm(int firmId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.Categories.Category").Include("Suppliers.Categories.SupplierProductions").First(x => x.Id == userId);
                var firm = user.Suppliers.First(x => x.Id == firmId);
                var cateogries = _model.SupplierCategories.ToList();
                var leftOuterJoinQuery = from category in cateogries
                                         join firmCategory in firm.Categories on category.Id equals firmCategory.Category.Id into cateogryGroup
                                         from cis in cateogryGroup.DefaultIfEmpty(new CategoryInSupplier() { Category = category })
                                         select new { isChecked = cis.Id != 0, Id = cis.Id, category = category, supplierProductions = cis.SupplierProductions, supplierProductionsList = Products(category.Id) };
                return Request.CreateResponse(HttpStatusCode.OK, leftOuterJoinQuery);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        private IEnumerable<SupplierProduct> Products(int categoryid)
        {
            var category = _model.SupplierCategories.Include("SupplierProducts").First(x => x.Id == categoryid);
            foreach (SupplierProduct prod in category.SupplierProducts)
            {
                if (prod.SupplierCategoryId == categoryid && prod.IsActive == true)
                {
                    yield return prod;
                }
            }
        }
    }
}