﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NickBuhro.Translit;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class ServicesController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/salon/{salonId}/services")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage GetServicesBySalon(int salonId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.Category").Include("Firms.Categories.Services.Service").Include("Firms.Categories.Services.Price").Include("Firms.Categories.CustomServices.Price").Include("Firms.Categories.Portfolios").First(x => x.Id == userId);
                var salon = user.Firms.First(x => x.Id == salonId);
                var cateogries = _model.Categories.ToList();
                var leftOuterJoinQuery = from category in cateogries
                                         join salonCategory in salon.Categories on category.Id equals salonCategory.CategoryId into cateogryGroup
                                         from cis in cateogryGroup.DefaultIfEmpty(new CategoryInSalon() { Category = category })
                                         select new { Checked = cis.Id != 0, Id = cis.Id, Category = category, Services = Services(cis), CustomServices = cis.CustomServices, Portfolios = cis.Portfolios };
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(leftOuterJoinQuery));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        private IEnumerable<ServiceViewModel> Services(CategoryInSalon cis)
        {
            var services = _model.Services.Where(x => x.CategoryId == cis.Category.Id);
            foreach (Service service in services)
            {
                var svm = new ServiceViewModel() { Service = service };
                try
                {
                    var s = cis.Services.First(x => x.Service.Id == service.Id);
                    svm.Price = s.Price;
                    svm.Id = s.Id;
                    svm.Checked = true;
                }
                catch
                {
                    svm.Checked = false;
                }

                yield return svm;
            }
        }
        public class ServiceViewModel
        {
            public bool Checked { get; set; }
            public Service Service { get; set; }
            public Price Price { get; set; }
            public int Id { get; set; }
        }
        [HttpGet]
        [Route("api/categories/{categoryAlias}/services")]
        public string GetServicesByCategory(string categoryAlias)
        {
            Category category = null;
            try
            {
                int id = Convert.ToInt32(categoryAlias);
                category = _model.Categories.First(x => x.Id == id);
            }
            catch
            {
                category = _model.Categories.First(x => x.Alias == categoryAlias);
            }
            var services = _model.Services.Where(x => x.CategoryId == category.Id && x.Priority != 0);
            return JsonConvert.SerializeObject(services);
        }

        [HttpGet]
        [Route("api/services")]
        public string GetServices()
        {
            return JsonConvert.SerializeObject(_model.Services);
        }

        [HttpGet]
        [Route("api/salon/{salonId}/category/{category}/services")]
        public string Services(int salonId, string category)
        {
            var salon = _model.Firms.Include("Categories.Category").Include("Categories.Services.Service").Include("Categories.Services.Price").Include("Categories.CustomServices.Price").First(x => x.Id == salonId);
            var c = salon.Categories.First(x => x.Category.Alias == category);
            return JsonConvert.SerializeObject(new { Services = c.Services, CustomServices = c.CustomServices });
        }

        [HttpGet]
        [Route("api/salon/{salonId}/category/{categoryId}/service/{serviceId}")]
        public string Get(int salonId, int categoryId, int serviceId)
        {
            try
            {
                var salon = _model.Firms.Find(salonId);
                var category = _model.CategoryInSalon.Include("Prices.Service").First(x => x.CategoryId == categoryId);
                //var service = category.Prices.First(x => x.Service.Id == serviceId);
                //return JsonConvert.SerializeObject(service, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [HttpGet]
        [Route("api/service/price/{priceId}")]
        public string GetService(int priceId)
        {
            //var service = _model.Prices.Include("Service").First(x => x.Id == priceId).Service;
            //return JsonConvert.SerializeObject(service);re
            return "";
        }

        [HttpPost]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}/service/{serviceId}")]
        public HttpResponseMessage PostService(int salonId, int categoryId, int serviceId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.Services.Price").Include("Firms.Categories.Services.Service").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.CategoryId == categoryId);                
                if (category.Services.Where(x => x.Service.Id == serviceId).Count() > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }                
                var sic = new ServiceInCategory(_model.Services.Find(serviceId), new Price(0, 0));
                category.Services.Add(sic);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(sic));                
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }            
        }

        [HttpPost]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}/userService")]
        public HttpResponseMessage PostUserService(int salonId, int categoryId, CustomService service)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.CustomServices.Price").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.CategoryId == categoryId);
                var cs = new CustomService(service.Name, 0, new Price(service.Price.Min, service.Price.Max));
                category.CustomServices.Add(cs);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(cs));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}/service")]
        public HttpResponseMessage PutService(int salonId, int categoryId, ServiceInCategory service)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.Services.Price").Include("Firms.Categories.Services.Service").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.CategoryId == categoryId);
                var s = category.Services.First(x => x.Id == service.Id);
                s.Price.Min = service.Price.Min;
                s.Price.Max = service.Price.Max;
                _model.Entry(s.Price).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}/customService")]
        public HttpResponseMessage PutCustomService(int salonId, int categoryId, CustomService service)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.CustomServices.Price").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.CategoryId == categoryId);
                var s = category.CustomServices.First(x => x.Id == service.Id);
                s.Price.Min = service.Price.Min;
                s.Price.Max = service.Price.Max;
                if (s.Name != service.Name)
                {
                    s.Alias = Transliteration.CyrillicToLatin(service.Name, Language.Russian).Replace(" ", "_").Replace("`", "");
                    s.Name = service.Name;
                }
                _model.Entry(s.Price).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(s));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}/customService/{serviceId}")]
        public HttpResponseMessage DeleteCustomService(int salonId, int categoryId, int serviceId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.CustomServices.Price").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.Id == categoryId);
                var service = category.CustomServices.First(x => x.Id == serviceId);
                _model.Prices.Remove(service.Price);
                category.CustomServices.Remove(service);
                _model.CustomServices.Remove(service);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}/service/{serviceId}")]
        public HttpResponseMessage DeleteService(int salonId, int categoryId, int serviceId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.Services.Price").Include("Firms.Categories.Services.Service").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.CategoryId == categoryId);
                var service = category.Services.First(x => x.Service.Id == serviceId);
                _model.Prices.Remove(service.Price);
                category.Services.Remove(service);
                _model.ServicesInCategory.Remove(service);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }            
        }
    }
}