﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class ScheduleController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpPost]
        [Route("api/salon/{salonId}/schedule")]
        [Authorize(Roles = "Client")]
        public string Post(int salonId, Schedule schedule)
        {
            string userId = User.Identity.GetUserId();
            Service service = _model.Services.Find(schedule.Service.Id);
            Price price = _model.Prices.Find(schedule.Price.Id);
            Schedule s = new Schedule(userId, service, price, schedule.Date);
            var salon = _model.Firms.Include("Schedules").First(x => x.Id == salonId);
            salon.Schedules.Add(schedule);
            _model.Entry(salon).State = EntityState.Modified;
            _model.SaveChanges();
            return JsonConvert.SerializeObject(schedule);
        }

        [HttpGet]
        [Route("api/salon/{salonId}/scheduleRequests")]
        [Authorize(Roles = "Salon, Freelance")]
        public string GetSceduleRequests(int salonId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.ScheduleRequests.Cost").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            return JsonConvert.SerializeObject(salon.ScheduleRequests);
        }

        [HttpPost]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/scheduleRequest")]
        public HttpResponseMessage ScheduleRequest(int salonId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.ScheduleRequests").First(x => x.Id == userId);
                var salon = user.Firms.First(x => x.Id == salonId);
                var cost = _model.ScheduleCost.ToList().Last();
                var request = new ScheduleRequest(cost);
                salon.ScheduleRequests.Add(request);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(request));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("api/schedule")]
        [Authorize(Roles = "Client")]
        public string Get()
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("ScheduleRequests.Price.Service").Include("Schedules.Salon").First(x => x.Id == userId);
            return JsonConvert.SerializeObject(user.Schedules.Select(x => new
            {
                //Salon = x.sc.Name,
                //Service = x.Price.Service.Name,
                Price = string.Format("{0}-{1}", x.Price.Min, x.Price.Max),
                Date = x.Date,
                Status = x.ScheduleStatus
            }));
        }

        [HttpGet]
        [Route("api/schedule/salon")]
        [Authorize(Roles = "Salon")]
        public string GetSchedules()
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.Schedules").First(x => x.Id == userId);
            return JsonConvert.SerializeObject(user.Firms);
        }

        [HttpPut]
        [Route("api/schedule/salon/{salonId}")]
        [Authorize(Roles = "Salon")]
        public void PutSchedule(int salonId, Schedule schedule)
        {
            //string userId = User.Identity.GetUserId();
            //var user = _model.Users.Include("Salons.Schedules").First(x => x.Id == userId);
            //var salon = user.Salons.First(x => x.Id == salonId);
            //var s = salon.Schedules.First(x => x.Id == schedule.Id);
            //s.ScheduleStatus = schedule.ScheduleStatus;
            //s.Date = schedule.Date;        
            //_model.Entry(s).State = EntityState.Modified;
            //_model.SaveChanges();
        }
        class SceduleViewModel
        {
            public ScheduleStatus Status { get; set; }
        }
    }
}