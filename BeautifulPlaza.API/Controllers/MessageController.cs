﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    [Authorize]
    public class MessageController : ApiController
    {
        ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/messages")]
        public string GetMessages()
        {
            if (User.IsInRole("Administrator"))
            {
                return JsonConvert.SerializeObject(_model.Messages);
            }
            string userId = User.Identity.GetUserId();
            var messages = _model.Messages.Where(x => x.ApplicationUserId == userId);
            return JsonConvert.SerializeObject(messages);
        }

        [HttpGet]
        [Route("api/messages/{id}")]
        public string GetMessages(int id)
        {
            string userId = User.Identity.GetUserId();
            var title = _model.Messages.Include("Messages").First(x => x.Id == id);
            if (title.ApplicationUserId != userId && !User.IsInRole("Administrator"))
                return "";

            var messages = new List<object>();
            foreach (var message in title.Messages)
            {
                var isAdmin = UserManager.IsInRole(message.ApplicationUserId, "Administrator");
                var isModer = UserManager.IsInRole(message.ApplicationUserId, "Moderator");
                var isUser = message.ApplicationUserId == userId;
                var from = isUser ? "Вы" : isAdmin ? "Администратор" : isModer ? "Модератор" : "Клиент";                
                messages.Add(new { From = from, Body = message.Body });
            }
            return JsonConvert.SerializeObject(new { Title = title.Title, Messages = messages });            
        }

        [HttpGet]
        [Route("api/unreadedMessagesCount")]
        public string GetUnderadedMessages()
        {
            string userId = User.Identity.GetUserId();
            var titles = _model.Messages.Count(x => x.Messages.Any(y => y.IsReaded == false));
            return JsonConvert.SerializeObject(titles);
        }

        [HttpPost]
        [Route("api/messages/{id}")]
        public string PostMessage(int id, MessageViewModel message)
        {
            string userId = User.Identity.GetUserId();
            var title = _model.Messages.Include("Messages").First(x => x.Id == id);
            var newMessage = new MessageBody() { Body = message.Body, MessageTitleId = id, ApplicationUserId = userId, IsReaded = false };
            title.Messages.Add(newMessage);
            _model.SaveChanges();

            return JsonConvert.SerializeObject(newMessage);
        }

        [HttpPost]
        [Route("api/messages")]
        public string PostMessage(MessageViewModel message)
        {
            var body = new MessageBody() { Body = message.Body, ApplicationUserId = User.Identity.GetUserId() };
            var messages = new List<MessageBody>();
            messages.Add(body);
            var title = new MessageTitle() { ApplicationUserId = User.Identity.GetUserId(), Title = message.Title, Messages = messages };
            _model.Messages.Add(title);
            _model.SaveChanges();

            return JsonConvert.SerializeObject(title);
        }
    }
    public class MessageViewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}