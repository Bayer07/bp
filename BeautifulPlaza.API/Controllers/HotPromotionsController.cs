﻿using BP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class HotPromotionsController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [AllowAnonymous]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(JsonConvert.SerializeObject(_model.HotPromotions.Take(8).OrderByDescending(x => x.Weight)));
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Authorize(Roles = "Administrator")]
        [Route("api/hotpromotions/all")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                return Request.CreateResponse(JsonConvert.SerializeObject(_model.HotPromotions.OrderByDescending(x => x.Weight)));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Authorize(Roles ="Administrator")]
        public HttpResponseMessage Post(HotPromotionsViewModel hp)
        {
            var firm = _model.Firms.Include("HotPromotions").First(x=>x.Id==hp.Firm.Id);
            string guid = Image.GenerateGuid(_model.HotPromotions.Select(x => x.Image).ToList());
            string path = ConfigurationManager.AppSettings["HotPromotionsPath"];
            try
            {
                Image.Upload(path + guid + ".jpg", hp.CroppedImage);
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            try
            {
                var promotion = new HotPromotion(hp.Title, hp.Body, guid, hp.Weight);
                firm.HotPromotions.Add(promotion);
                _model.SaveChanges();
                return Request.CreateResponse(JsonConvert.SerializeObject(promotion));
            }
            catch (Exception e)
            {
                Image.Delete(path + guid + ".jpg");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public class HotPromotionsViewModel
        {
            public string Title { get; set; }
            public string Body { get; set; }
            public string CroppedImage { get; set; }
            public FirmViewModel Firm { get; set; }
            public int Weight { get; set; }
        }
        public class FirmViewModel
        {
            public int Id { get; set; }
        }

    }
}