﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NickBuhro.Translit;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class CategoriesController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/categories")]
        public string Get()
        {
            return JsonConvert.SerializeObject(_model.Categories.OrderByDescending(x=>x.Priority));
        }

        [HttpGet]
        [Route("api/categories/{id}")]
        public string Get(int id)
        {
            return JsonConvert.SerializeObject(_model.Categories.FindAsync(id));
        }

        [HttpGet]
        [Route("api/salon/{id}/categories")]
        public string salon(int id)
        {
            var salon = _model.Firms.Include("Categories.Category").First(x => x.Id == id);
            return JsonConvert.SerializeObject(salon.Categories);
        }

        [HttpPost]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}")]
        public string AddCategoryToSalon(int salonId, int categoryId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.Categories").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            try
            {
                var c = salon.Categories.First(x => x.CategoryId == categoryId);
                return "";
            }
            catch
            {
                var category = new CategoryInSalon(_model.Categories.Find(categoryId));
                salon.Categories.Add(category);
                _model.SaveChanges();
                return JsonConvert.SerializeObject(category);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Salon, Freelance")]
        [Route("api/salon/{salonId}/category/{categoryId}")]
        public HttpResponseMessage DeleteCategoryFromSalon(int salonId, int categoryId)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.Categories.Services.Price").Include("Firms.Categories.Portfolios").Include("Firms.Categories.CustomServices.Price").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            try
            {
                var c = salon.Categories.First(x => x.CategoryId == categoryId);
                foreach (ServiceInCategory service in c.Services.ToList())
                {
                    _model.Prices.Remove(service.Price);
                    c.Services.Remove(service);
                    _model.ServicesInCategory.Remove(service);
                }
                foreach (CustomService service in c.CustomServices.ToList())
                {
                    _model.Prices.Remove(service.Price);
                    _model.CustomServices.Remove(service);
                    c.CustomServices.Remove(service);
                }
                var appSettings = ConfigurationManager.AppSettings;
                string portfolioPath = appSettings["PortfolioPath"];
                foreach (Portfolio portfolio in c.Portfolios.ToList())
                {
                    Image.Delete(portfolioPath + "/" + portfolio.Image + ".jpg");
                    _model.Portfolios.Remove(portfolio);
                    c.Portfolios.Remove(portfolio);
                }
                salon.Categories.Remove(c);
                _model.CategoryInSalon.Remove(c);
                _model.Entry(salon).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(_model.Services.Where(x => x.CategoryId == categoryId).Select(y => new ServicesController.ServiceViewModel() { Checked = false, Service = y })));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [Route("api/categories")]
        public HttpResponseMessage PostCategory(CategoryViewModel cat)
        {
            string guid = Image.GenerateGuid(_model.Categories.Select(x => x.Image).ToList());
            string path = ConfigurationManager.AppSettings["CategoriesPath"];
            try
            {
                Image.Upload(path + guid + ".jpg", cat.CroppedImage);
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            try
            {
                var category = new Category(guid, cat.Name, 0);
                _model.Categories.Add(category);
                _model.SaveChanges();
                return Request.CreateErrorResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(category));
            }
            catch (Exception e)
            {
                Image.Delete(path + guid + ".jpg");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        public class CategoryViewModel
        {
            public string Name { get; set; }
            public string CroppedImage { get; set; }
            public int Id { get; set; }
            public int Priority { get; set; }
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [Route("api/categories/image")]
        public HttpResponseMessage PutImage(CategoryViewModel cat)
        {
            try
            {
                var category = _model.Categories.Find(cat.Id);
                string guid;
                if (category.Image.Length > 20 || category.Image.Length == 0)
                {
                    guid = Image.GenerateGuid(_model.Categories.Select(x => x.Image).ToList());
                    category.Image = guid;
                    _model.Entry(category).State = EntityState.Modified;
                    _model.SaveChanges();
                }
                else
                {
                    guid = category.Image;
                }
                string path = ConfigurationManager.AppSettings["CategoriesPath"];
                Image.Upload(path + guid + ".jpg", cat.CroppedImage);
                return Request.CreateErrorResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(category));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [Route("api/categories")]
        public HttpResponseMessage PutCategory(CategoryViewModel cat)
        {
            try
            {
                var category = _model.Categories.Find(cat.Id);
                category.Name = cat.Name;
                category.Priority = cat.Priority;
                category.Alias = Transliteration.CyrillicToLatin(cat.Name, Language.Russian).Replace(" ", "_").Replace("`", "");
                _model.Entry(category).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        [Route("api/categories/{categoryId}")]
        public HttpResponseMessage DeleteCategory(int categoryId)
        {
            try
            {
                var category = _model.Categories.Find(categoryId);
                _model.Categories.Remove(category);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}