﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BP.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : ApiController
    {
        private BPModel _model = BPModel.Create();
        
        [Route("api/users/{userId}")]
        public string GetUser(string userId)
        {
            var user = _model.Users.Find(User.Identity.GetUserId());
            return JsonConvert.SerializeObject(user);
        }
        
        [Route("api/users")]
        public string GetUsers()
        {
            return JsonConvert.SerializeObject(_model.Users.Include("Roles").ToList());
        }
    }
}