﻿using BP.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace BP.Controllers
{
    public class SalonListController : ApiController
    {
        private const int pageSize = 21;

        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/city/{cityId}/salons")]
        public string Get(int cityId)
        {
            var salons = _model.Firms.Include(x => x.Coordinates).Include("Street").Include("Phones").Include("City").Include("Categories.Category").Include("Categories.Services.Service").Include("Categories.Services.Price").Where(x => x.City.Id == cityId);
            return JsonConvert.SerializeObject(salons);                     
        }

        [HttpGet]
        [Route("api/salons")]
        public string Get()
        {
            var salons = _model.Firms.Select(x => new { Id = x.Id, Name = x.Name, IsSalon = true });
            return JsonConvert.SerializeObject(salons);
        }

        [HttpGet]
        [Route("api/salons/category/{categoryAlias}")]
        public string GetByCategory(string categoryAlias)
        {
            var category = _model.Categories.First(x => x.Alias == categoryAlias);
            var salons = _model.Firms.Where(x => x.Categories.Any(y => y.CategoryId == category.Id));
            return JsonConvert.SerializeObject(salons);
        }
    }
}