﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class BannerController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        [Route("api/admin/banners")]
        public string GetBannersByAdmin()
        {
            var banners = _model.Banners.Include("Cost").Include("DesignerCost");
            return JsonConvert.SerializeObject(banners);
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [Route("api/admin/banners")]
        public void GetBannersByAdmin(Banner banner)
        {
            var banners = _model.Banners;
            var bannerExist = banners.First(x => x.Id == banner.Id);
            bannerExist.EndDate = banner.EndDate;
            bannerExist.StartDate = banner.StartDate;
            bannerExist.IsActive = banner.IsActive;
            bannerExist.Weight = banner.Weight;
            _model.Entry(bannerExist).State = System.Data.Entity.EntityState.Modified;
            _model.SaveChanges();
        }

        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        [Route("api/admin/banners/{id}")]
        public HttpResponseMessage DeleteBannerByAdmin(int id)
        {
            var appSettings = ConfigurationManager.AppSettings;
            string bannerPath = appSettings["BannerPath"];
            var banner = _model.Banners.Find(id);
            try
            {
                Image.Delete(bannerPath + banner.Image + ".jpg");
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            _model.Banners.Remove(banner);
            _model.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/banners")]
        public string GetBanners()
        {
            var now = DateTime.Now;
            var banners = _model.Banners.Where(x => x.EndDate > now && x.StartDate < now && x.IsActive == true).OrderByDescending(x => x.Weight);
            return JsonConvert.SerializeObject(banners.Select(x => new { Image = x.Image, FirmId = x.SalonId }));
        }

        [HttpPost]
        [Route("api/banner")]
        public HttpResponseMessage PostBanner(BannerViewModel bannerViewModel)
        {
            string guid = Image.GenerateGuid(_model.Banners.Select(x => x.Image).ToList());
            var appSettings = ConfigurationManager.AppSettings;
            string bannerPath = appSettings["BannerPath"];
            try
            {
                Image.Upload(bannerPath + guid + ".jpg", bannerViewModel.Image);
            }
            catch (Exception e)
            {
                Image.Delete(bannerPath + guid + ".jpg");
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }

            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Banners").First(x => x.Id == userId);
                var salon = user.Firms.First(x => x.Id == bannerViewModel.SalonId);
                var cost = _model.BannerCost.ToList().Last();
                var now = DateTime.Now;

                var banner = new Banner() { Cost = cost, EndDate = now, Image = guid, IsActive = false, StartDate = now, Message = bannerViewModel.Message, Weight = 0, SalonId = bannerViewModel.SalonId };
                if (bannerViewModel.NeedDesigner)
                {
                    banner.DesignerCost = _model.BannerDesignerCost.ToList().Last();
                }
                _model.Banners.Add(banner);
                salon.Banners.Add(banner);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(banner));
            }
            catch (Exception e)
            {
                Image.Delete(bannerPath + guid + ".jpg");
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        public class BannerViewModel
        {
            public bool NeedDesigner { get; set; }
            public string Image { get; set; }
            public string Message { get; set; }
            public int SalonId { get; set; }
        }
    }
}