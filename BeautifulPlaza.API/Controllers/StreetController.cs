﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    public class StreetController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/street/{streetId}")]
        public string GetById(int streetId)
        {
            return JsonConvert.SerializeObject(_model.Streets.Find(streetId));
        }

        [HttpGet]
        [Route("api/city/{cityId}/street")]
        public string GetByCityId(int cityId)
        {
            var streets = _model.Streets.Where(x => x.CityId == cityId);
            return JsonConvert.SerializeObject(streets);
        }

        [HttpGet]
        [Route("api/city/{cityId}/streetnew")]
        public HttpResponseMessage GetByCityIdNew(int cityId)
        {
            try
            {
                var streets = _model.Streets.Where(x => x.CityId == cityId && x.IsActive == true);
                return Request.CreateResponse(HttpStatusCode.OK, streets.Select(x => new { id = x.Id, name = x.Name }));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("api/street")]
        public string Get()
        {
            return JsonConvert.SerializeObject(_model.Streets);
        }

        [HttpPost]
        [Route("api/salon/{salonId}/street")]
        [Authorize(Roles = "Salon")]
        public string Post(int salonId, Street street)
        {
            //string userId = User.Identity.GetUserId();
            //var user = _model.Users.Include("Salons.Street").First(x => x.Id == userId);
            //var salon = user.Firms.First(x => x.Id == salonId);
            //int oldStreet = salon.Street.Id;
            //salon.Street.Id = (int)street.Id;
            //_model.Entry(salon).State = EntityState.Modified;
            //_model.SaveChanges();
            //var salons = _model.Salons.Where(x => x.StreetId == oldStreet);
            //if (salons.Count() == 0)
            //{
            //    _model.Streets.Remove(_model.Streets.Find(oldStreet));
            //    _model.SaveChanges();
            //}
            return "";
        }
    }
}