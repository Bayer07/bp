﻿using BP.Models;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BP.Controllers
{
    [RoutePrefix("api/supplier")]
    public class SupplierController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Authorize(Roles = "Supplier")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _model.Suppliers);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("firm/{firmId:int}/phone")]
        [Authorize(Roles = "Supplier")]
        public async Task<HttpResponseMessage> PostPhone(int firmId, SupplierPhoneViewModel phone)
        {
            try
            {
                if (phone.Number == "")
                    throw new Exception();
                string userId = User.Identity.GetUserId();
                var user = await _model.Users.Include("Suppliers.Phones").FirstAsync(x => x.Id == userId);
                var firm = user.Suppliers.First(x => x.Id == firmId);
                Phone p = null;
                if (phone.Post)
                {
                    p = new Phone() { Number = phone.Number };
                    firm.Phones.Add(p);
                }
                if (phone.Delete)
                {
                    p = _model.Phones.Find(phone.Id);
                    firm.Phones.Remove(p);
                    _model.Phones.Remove(p);
                }
                if (phone.Put)
                {
                    p = _model.Phones.Find(phone.Id);
                    p.Number = phone.Number;
                }
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, p);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("firm/{firmId:int}")]
        [Authorize(Roles = "Supplier")]
        public async Task<HttpResponseMessage> GetById(int firmId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = await _model.Users.Include("Suppliers.City").Include("Suppliers.WorkTimes.DayOfWeek").Include("Suppliers.Street").Include("Suppliers.Phones").Include("Suppliers.Email").FirstAsync(x => x.Id == userId);

                return Request.CreateResponse(HttpStatusCode.OK, user.Suppliers.First(x => x.Id == firmId));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("firm/{firmId:int}")]
        [Authorize(Roles = "Supplier")]
        public HttpResponseMessage Post(int firmId, SupplierViewModel sup)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.City").Include("Suppliers.Street").Include("Suppliers.Email").First(x => x.Id == userId);
                var firm = user.Suppliers.First(x => x.Id == firmId);
                if (!ModelState.IsValid)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);
                }
                firm.Name = sup.name;
                firm.City = _model.Cities.Find(sup.city.id);
                firm.Street = _model.Streets.Find(sup.street.id);
                firm.House = sup.house;
                firm.Description = sup.description;
                if (firm.Email == null)
                {
                    firm.Email = new Email() { Address = sup.email };
                }
                else
                {
                    firm.Email.Address = sup.email;
                    _model.Entry(firm.Email).State = EntityState.Modified;
                }
                _model.Entry(firm).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
    public class SupplierViewModel
    {
        public int id { get; set; }
        [MaxLength(20,ErrorMessage ="Название фирмы не может быть длиннее 20 символов")]
        public string name { get; set; }
        public CityViewModel city { get; set; }
        public StreetViewModel street { get; set; }
        [MaxLength(10, ErrorMessage = "Номер дома не может быть длиннее 10 символов")]
        public string house { get; set; }
        [MaxLength(500, ErrorMessage = "Описание не может быть длиннее {1} символов")]
        public string description { get; set; }
        [EmailAddress(ErrorMessage = "Не корректный Email адрес")]
        [MaxLength(100, ErrorMessage = "Длинна Email адреса не может быть длиннее 100 символов")]
        public string email { get; set; }
    }
    public class StreetViewModel
    {
        public int id { get; set; }
    }
    public class CityViewModel
    {
        public int id { get; set; }
    }    
    public class SupplierPhoneViewModel
    {
        public int? Id { get; set; }
        public bool Delete { get; set; }
        public bool Post { get; set; }
        public bool Put { get; set; }
        [StringLength(10)]
        public string Number { get; set; }
    }
}