﻿using BP.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BP.Controllers
{
    [RoutePrefix("api/worktime")]
    public class WorktimeController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _model.WorkTimes);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("firm/{firmId:int}")]
        [Authorize(Roles = "Supplier, Salon, Freelance")]
        public HttpResponseMessage Post(int firmId, WorkTimeViewModel worktime)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.WorkTimes.DayOfWeek").First(x => x.Id == userId);
                var firm = user.Suppliers.First(x => x.Id == firmId);
                WorkTime wt = null;
                if (worktime.Post)
                {
                    var times = firm.WorkTimes.Where(x => x.DayOfWeek.Id == worktime.dayofweek.id);
                    if (times.Count() > 0)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");
                    }
                    var day = _model.DaysOfWeek.Find(worktime.dayofweek.id);
                    var open = worktime.open.ToLocalTime();
                    var close = worktime.close.ToLocalTime();
                    wt = new WorkTime(day, open.TimeOfDay, close.TimeOfDay);
                    firm.WorkTimes.Add(wt);
                }
                if (worktime.Put)
                {
                    wt = _model.WorkTimes.Find(worktime.id);
                    wt.Open = worktime.open.ToLocalTime().TimeOfDay;
                    wt.Close = worktime.close.ToLocalTime().TimeOfDay;
                    _model.Entry(wt).State = EntityState.Modified;
                }
                if (worktime.Delete)
                {
                    wt = _model.WorkTimes.Find(worktime.id);
                    firm.WorkTimes.Remove(wt);
                    _model.WorkTimes.Remove(wt);
                }
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, wt);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("daysofweek")]
        public HttpResponseMessage GetDays()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _model.DaysOfWeek);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
    public class WorkTimeViewModel
    {
        public int? id { get; set; }
        public DayOfWeekViewModel dayofweek { get; set; }
        public DateTime open { get; set; }
        public DateTime close { get; set; }
        public bool Post { get; set; }
        public bool Delete { get; set; }
        public bool Put { get; set; }
    }

    public class DayOfWeekViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}