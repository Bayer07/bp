﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BP.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RolesController : ApiController
    {
        private static  ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private static ApplicationRoleManager _roleManager;

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        private static BPModel _model = BPModel.Create();
        private static RoleStore<IdentityRole> _roleStore;
        public static RoleStore<IdentityRole> RoleStore
        {
            get
            {
                if (_roleStore == null)
                {
                    _roleStore = new RoleStore<IdentityRole>(_model);
                }
                return _roleStore;
            }
        }

        private User GetUser()
        {
            User user = _model.Users.Find(User.Identity.GetUserId());
            return user;
        }
        
        [HttpGet]
        [Route("api/roles")]
        public string Get()
        {
            var roles = RoleManager.Roles.ToList().Select(x => new { RoleId = x.Id, Name = x.Name, Description = x.Description });
            return JsonConvert.SerializeObject(roles);
        }

        [HttpPost]
        [Route("api/roles")]
        public string Post(ApplicationRole role)
        {
            RoleManager.Create(role);
            return JsonConvert.SerializeObject(role);
        }

        [HttpPut]
        [Route("api/roles")]
        public string Put(ApplicationRole role)
        {
            var appRole = RoleManager.FindById(role.Id);
            appRole.Description = role.Description;
            RoleManager.Update(appRole);
            return JsonConvert.SerializeObject(role);
        }

        [HttpPost]
        [Route("api/roles/{userId}")]
        public string addRoleToUser(string userId, ApplicationRole role)
        {
            var appUser = UserManager.FindById(userId);
            var roles = appUser.Roles.Select(x => RoleManager.FindById(x.RoleId).Name).ToArray();
            UserManager.RemoveFromRoles(userId, roles);
            UserManager.AddToRole(userId, role.Name);
            return JsonConvert.SerializeObject(new { RoleId = role.Id });
        }
    }
}