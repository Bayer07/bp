﻿using BP.Models;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net;
using Microsoft.AspNet.Identity;
using System.Configuration;

namespace BP.Controllers
{
    public class PortfolioController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/salon/{salonId}/category/{categoryId}/portfolios")]
        public string Get(int salonId, int categoryId)
        {
            var salon = _model.Firms.Include("Categories.Portfolios").First(x => x.Id == salonId);
            var category = salon.Categories.First(x => x.CategoryId == categoryId);
            return JsonConvert.SerializeObject(category.Portfolios);
        }

        [HttpDelete]
        [Route("api/salon/{salonId}/category/{categoryId}/portfolio/{portfolioId}")]
        public HttpResponseMessage DeletePortfolio(int salonId, int categoryId, int portfolioId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.Portfolios").First(x => x.Id == userId);
                var firm = user.Firms.First(x => x.Id == salonId);
                var category = firm.Categories.First(x => x.Id == categoryId);
                var portfolio = category.Portfolios.First(x => x.Id == portfolioId);
                _model.Portfolios.Remove(portfolio);
                category.Portfolios.Remove(portfolio);
                Image.Delete(ConfigurationManager.AppSettings["PortfolioPath"] + portfolio.Image + ".jpg");
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("api/salon/{salonId}/category/{categoryId}/portfolio")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage Post(int salonId, int categoryId, Portfolio portfolio)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Firms.Categories.Portfolios").First(x => x.Id == userId);
                var salon = user.Firms.First(x => x.Id == salonId);
                var category = salon.Categories.First(x => x.Id == categoryId);
                if (category.Portfolios.Count >= 5)
                    throw new Exception("Не более 5");
                string guid = Image.GenerateGuid(_model.Portfolios.Select(x => x.Image).ToList());
                var p = new Portfolio(guid);
                category.Portfolios.Add(p);                
                Image.Upload(ConfigurationManager.AppSettings["PortfolioPath"] + guid + ".jpg", portfolio.Image);
                _model.Entry(category).State = EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(p));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}