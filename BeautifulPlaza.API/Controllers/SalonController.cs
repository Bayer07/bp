﻿using BP.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BP.Controllers
{
    public class SalonController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [HttpGet]
        [Route("api/salonNames")]
        public string GetSalonNames()
        {
            return JsonConvert.SerializeObject(_model.Firms.Select(x => new { Id = x.Id, Name = x.Name }));
        }

        [HttpGet]
        [Route("api/user/salon")]
        [Authorize(Roles = "Salon, Freelance")]
        public async Task<string> GetByUser()
        {
            string userId = User.Identity.GetUserId();
            var user = await _model.Users.Include("Firms").FirstAsync(x => x.Id == userId);
            return JsonConvert.SerializeObject(user.Firms);
        }

        [HttpGet]
        [Route("api/salon/{salonId}")]
        public async Task<string> GetById(int salonId)
        {
            Firm firm = await _model.Firms.Include("City").Include("Street").Include("Phones").Include("Emails").Include("Links.LinkType").Include("WorkTimes.DayOfWeek").Include("Categories.Category").Include("Categories.Portfolios").FirstAsync(x => x.Id == salonId);
            return JsonConvert.SerializeObject(firm);
        }

        [HttpPost]
        [Route("api/salon/{salonId}/time")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage PostTime(int salonId, WorkTimeViewModel time)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.WorkTimes.DayOfWeek").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            var workTime = salon.WorkTimes.First(x => x.Id == time.Id && x.DayOfWeek.Id == time.DayOfWeekId);
            workTime.Open = new TimeSpan(time.Open.Hours, time.Open.Minutes, 0);
            workTime.Close = new TimeSpan(time.Close.Hours, time.Close.Minutes, 0);
            _model.Entry(workTime).State = EntityState.Modified;
            _model.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        public class WorkTimeViewModel
        {
            public int Id { get; set; }
            public TimeViewModel Open { get; set; }
            public TimeViewModel Close { get; set; }
            public int DayOfWeekId { get; set; }
        }
        public class TimeViewModel
        {
            public int Hours { get; set; }
            public int Minutes { get; set; }
        }

        [HttpPost]
        [Route("api/salon/{salonId}/logo")]
        [Authorize(Roles = "Salon, Freelance")]
        public HttpResponseMessage PostLogo(int salonId, ImageViewModel image)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonId);
            string guid = "";
            if (string.IsNullOrEmpty(salon.Image))
            {
                guid = Image.GenerateGuid(_model.Firms.Select(x => x.Image).ToList());
                try
                {
                    salon.Image = guid;
                    _model.Entry(salon).State = EntityState.Modified;
                }
                catch (Exception e)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                }
            }
            else
            {
                guid = salon.Image;
            }
            try
            {
                Image.Upload("images/salons/" + guid + ".jpg", image.Image);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, guid);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        public class ImageViewModel
        {
            public string Image { get; set; }
        }

        [HttpPost]
        [Route("api/salon")]
        [Authorize(Roles = "Salon, Freelance")]
        public string Post(SalonViewModel salonViewModel)
        {
            string userId = User.Identity.GetUserId();
            var user = _model.Users.Include("Firms.City").Include("Firms.Street").First(x => x.Id == userId);
            var salon = user.Firms.First(x => x.Id == salonViewModel.Id);
            salon.Description = salonViewModel.Description;
            salon.DiscountForBirthday = salonViewModel.DiscountForBirthday;
            salon.Floor = salonViewModel.Floor;
            salon.House = salonViewModel.House;
            salon.IsCashlessPayment = salonViewModel.IsCashlessPayment;
            salon.IsWifi = salonViewModel.IsWifi;
            salon.IsWorkAtHome = salonViewModel.IsWorkAtHome;
            salon.Name = salonViewModel.Name;
            salon.Office = salonViewModel.Office;
            salon.WorkWithProduction = salonViewModel.WorkWithProduction;
            City city = null;
            if (salonViewModel.City.Id == null)
            {
                city = new City(salonViewModel.City.Name, userId);
                _model.Cities.Add(city);
                _model.SaveChanges();
            }
            if (city == null)
            {
                city = salon.City;
            }
            if (salonViewModel.Street.Id == null)
            {
                var street = new Street(salonViewModel.Street.Name, city.Id, userId);
                salon.Street = street;
            }
            _model.Entry(salon).State = EntityState.Modified;
            _model.SaveChanges();
            return "";
        }

        public class SalonViewModel
        {
            [MaxLength(50)]
            public string Name { get; set; }
            public int Id { get; set; }
            public StreetViewModel Street { get; set; }
            public CityViewModel City { get; set; }
            [MaxLength(250)]
            public string Description { get; set; }
            [MaxLength(250)]
            public string DiscountForBirthday { get; set; }
            [MaxLength(3)]
            public string Floor { get; set; }
            [MaxLength(10)]
            public string House { get; set; }
            [MaxLength(3)]
            public string Office { get; set; }
            [MaxLength(250)]
            public string WorkWithProduction { get; set; }
            public bool IsCashlessPayment { get; set; }
            public bool IsWifi { get; set; }
            public bool IsWorkAtHome { get; set; }
        }
        public class CityViewModel
        {
            public int? Id { get; set; }
            [MaxLength(50)]
            public string Name { get; set; }
        }
        public class StreetViewModel
        {
            public int? Id { get; set; }
            [MaxLength(50)]
            public string Name { get; set; }
        }
    }
}