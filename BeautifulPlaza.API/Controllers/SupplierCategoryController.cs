﻿using BP.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    [RoutePrefix("api/suppliercategory")]
    [Authorize]
    public class SupplierCategoryController : ApiController
    {
        private BPModel _model = BPModel.Create();
        public HttpResponseMessage Get()
        {
            try
            {
                if (User.IsInRole("Administrator"))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, _model.SupplierCategories.Include("SupplierProducts"));
                }
                return Request.CreateResponse(HttpStatusCode.OK, _model.SupplierCategories.Select(x => new { id = x.Id, name = x.Name }));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage Post(SupplierCategoryViewModel cat)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);
            }
            string supplierPath = ConfigurationManager.AppSettings["SupplierCategoriesPath"];
            var guid = Image.GenerateGuid(_model.SupplierCategories.Select(x => x.Image).ToList());
            try
            {
                Image.Upload(supplierPath + guid + ".jpg", cat.croppedImage);
                var category = new SupplierCategory(guid, cat.name);
                _model.SupplierCategories.Add(category);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, category);
            }
            catch (Exception e)
            {
                Image.Delete(supplierPath + guid + ".jpg");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("delete")]
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage Delete(SupplierCategoryViewModel cat)
        {
            try
            {
                string supplierPath = ConfigurationManager.AppSettings["SupplierCategoriesPath"];
                Image.Delete(supplierPath + cat.image + ".jpg");
                _model.SupplierCategories.Remove(_model.SupplierCategories.Find(cat.id));
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("put")]
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage Put(SupplierCategoryViewModel cat)
        {
            try
            {
                var supp = _model.SupplierCategories.Find(cat.id);
                supp.Name = cat.name;
                if (!string.IsNullOrEmpty(cat.croppedImage))
                {
                    var c = _model.SupplierCategories.Find(cat.id);
                    string supplierPath = ConfigurationManager.AppSettings["SupplierCategoriesPath"];
                    Image.Delete(supplierPath + c.Image + ".jpg");
                    Image.Upload(supplierPath + c.Image + ".jpg", cat.croppedImage);
                }
                _model.Entry(supp).State = System.Data.Entity.EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("{categoryId:int}/addproduct")]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage AddProduct(int categoryId, SupplierProductViewModel prod)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);
            }
            try
            {
                var category = _model.SupplierCategories.Include("SupplierProducts").First(x => x.Id == categoryId);
                string userId = User.Identity.GetUserId();
                var product = new SupplierProduct(prod.name, userId, category.Id);
                category.SupplierProducts.Add(product);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, product);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("{categoryId:int}/changeproduct")]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage ChangeProduct(int categoryId, SupplierProductViewModel prod)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);
            }
            try
            {
                var category = _model.SupplierCategories.Include("SupplierProducts").First(x => x.Id == categoryId);
                var product = category.SupplierProducts.First(x => x.Id == prod.id);
                product.IsActive = prod.isActive;
                product.Name = prod.name;
                _model.Entry(product).State = System.Data.Entity.EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, product);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("{categoryId:int}/{firmId:int}")]
        [Authorize(Roles = "Supplier")]
        public HttpResponseMessage AddCategoryToFirm(int categoryId, int firmId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.Categories.Category").Include("Suppliers.Categories.SupplierProductions").First(x => x.Id == userId);
                var supplier = user.Suppliers.First(x => x.Id == firmId);
                var category = _model.SupplierCategories.First(x => x.Id == categoryId);
                var c = new CategoryInSupplier(category);
                supplier.Categories.Add(c);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, c);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("{categoryId:int}/{firmId:int}/delete")]
        [Authorize(Roles = "Supplier")]
        public HttpResponseMessage DeleteCategoryToFirm(int categoryId, int firmId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var user = _model.Users.Include("Suppliers.Categories.Category").Include("Suppliers.Categories.SupplierProductions").First(x => x.Id == userId);
                var supplier = user.Suppliers.First(x => x.Id == firmId);
                var category = supplier.Categories.First(x => x.Category.Id == categoryId);
                supplier.Categories.Remove(category);
                _model.CategoryInSuppliers.Remove(category);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("firms")]
        [Authorize(Roles ="Salon")]
        public HttpResponseMessage GetByFirm()
        {
            try
            {
                var cateogries = _model.SupplierCategories.ToList();
                var suppliers = _model.Suppliers.Include("Categories.Category").Include("Categories.SupplierProductions");
                var result = cateogries.Select(x => new { category = x, suppliers = Suppliers(suppliers.ToList(), x) });
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        private IEnumerable<Supplier> Suppliers(List<Supplier> suppliers, SupplierCategory category)
        {
            foreach (Supplier sup in suppliers)
            {
                if (sup.Categories.Any(x => x.Category.Id == category.Id))
                    yield return sup;
            }
        }
    }
    public class SupplierCategoryViewModel
    {
        public string image { get; set; }
        [Required]
        public string croppedImage { get; set; }
        [Required]
        public string name { get; set; }
        public int id { get; set; }
    }
    public class SupplierProductViewModel
    {
        [MaxLength(50, ErrorMessage = "Длинна названия не должна превышать 50 символов")]
        public string name { get; set; }
        public bool isActive { get; set; }
        public int? id { get; set; }
    }
}