﻿using BP.Models;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BP.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class NewsController : ApiController
    {
        private BPModel _model = BPModel.Create();

        [AllowAnonymous]
        [HttpGet]
        [Route("api/news")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(_model.News.Include("TitleImage")));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/news/{articleId}")]
        public HttpResponseMessage GetById(int articleId)
        {
            try
            {
                var article = _model.News.Include("TitleImage").Include("Images").First(x => x.Id == articleId);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(article));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("api/news/all")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(_model.News.Include("Images").Include("TitleImage")));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("api/news")]
        public HttpResponseMessage Post(News news)
        {
            try
            {
                var n = new News();
                n.Title = news.Title;
                _model.News.Add(n);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(n));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPut]
        [Route("api/news")]
        public HttpResponseMessage Put(News news)
        {
            try
            {
                var n = _model.News.Find(news.Id);
                n.Title = news.Title;
                n.Body = news.Body;
                n.Preview = news.Preview;
                _model.Entry(n).State = System.Data.Entity.EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("api/news/{newsId}/image")]
        public HttpResponseMessage Post(int newsId, NewsImage image)
        {
            var guid = Image.GenerateGuid(_model.NewsImages.Select(x => x.Image).ToList());
            string path = ConfigurationManager.AppSettings["NewsPath"] + guid + ".jpg";
            try
            {
                Image.Upload(path, image.Image);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            try
            {
                var post = _model.News.Include("Images").First(x => x.Id == newsId);
                var n = new NewsImage(guid);
                post.Images.Add(n);
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(n));
            }
            catch (Exception e)
            {
                Image.Delete(path);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("api/news/{newsId}/titleImage")]
        public HttpResponseMessage PostTitle(int newsId, NewsImage image)
        {
            var guid = Image.GenerateGuid(_model.NewsImages.Select(x => x.Image).ToList());
            string path = ConfigurationManager.AppSettings["NewsPath"] + guid + ".jpg";
            try
            {
                Image.Upload(path, image.Image);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            try
            {
                var post = _model.News.First(x => x.Id == newsId);
                var n = new NewsImage(guid);
                post.TitleImage = n;
                _model.Entry(post).State = System.Data.Entity.EntityState.Modified;
                _model.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(n));
            }
            catch (Exception e)
            {
                Image.Delete(path);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpDelete]
        [Route("api/news/{newsId}/image/{imageId}")]
        public HttpResponseMessage DeleteImage(int newsId, int imageId)
        {
            try
            {
                var news = _model.News.Include("Images").First(x => x.Id == newsId);
                var image = _model.NewsImages.First(x => x.Id == imageId);
                string path = ConfigurationManager.AppSettings["NewsPath"] + image.Image + ".jpg";
                news.Images.Remove(image);
                _model.NewsImages.Remove(image);
                _model.SaveChanges();
                Image.Delete(path);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}