﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BP.Models
{
    public class SalonListFilter
    {
        public string Category { get; set; }
        public List<SalonViewModel> Salons { get; set; }
        public Filter Filter { get; set; }
    }
    public class SalonViewModel
    {
        public int Id { get; set; }
        public bool IsCashlessPayment { get; set; }
        public bool IsWifi { get; set; }
        public bool HaveSchedule { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public ICollection<CategoryInSalon> Categories { get; set; }
        public Coordinates Coordinates { get; set; }
        public string Address { get; set; }
        public bool IsSalon { get; set; }
    }
    public class Filter
    {
        public string Salon { get; set; }
        public ServiceViewModel Service { get; set; }
        public int[] Price { get; set; }
        public string Street { get; set; }
        public bool IsSalon { get; set; }
        public bool IsFreelance { get; set; }

    }
    public class ServiceViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}