namespace BP.Models
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    using NickBuhro.Translit;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class BPModel : IdentityDbContext<User>
    {
        public BPModel()
            : base("name=BPModel")
        {
        }
        public static BPModel Create()
        {
            return new BPModel();
        }

        public virtual DbSet<CategoryInSalon> CategoryInSalon { get; set; }
        public virtual DbSet<Firm> Firms { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<Email> Emails { get; set; }
        public virtual DbSet<Link> Links { get; set; }
        public virtual DbSet<LinkType> LinkTypes { get; set; }
        public virtual DbSet<WorkTime> WorkTimes { get; set; }
        public virtual DbSet<DayOfWeek> DaysOfWeek { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Price> Prices { get; set; }
        public virtual DbSet<Portfolio> Portfolios { get; set; }
        public virtual DbSet<Banner> Banners { get; set; }
        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<BannerCost> BannerCost { get; set; }
        public virtual DbSet<BannerDesignerCost> BannerDesignerCost { get; set; }
        public virtual DbSet<ScheduleCost> ScheduleCost { get; set; }
        public virtual DbSet<VacancyCost> VacancyCost { get; set; }
        public virtual DbSet<PromotionCost> PromotionCost { get; set; }
        public virtual DbSet<PromotionDesignerCost> PromotionDesignerCost { get; set; }
        public virtual DbSet<MessageTitle> Messages { get; set; }
        public virtual DbSet<ScheduleRequest> ScheduleRequests { get; set; }
        public virtual DbSet<Vacancy> Vacancies { get; set; }
        public virtual DbSet<Coordinates> Coordinates { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Street> Streets { get; set; }
        public virtual DbSet<VacancyName> VacancyNames { get; set; }
        public virtual DbSet<VacancyWorkExperience> VacancyWorkExperience { get; set; }
        public virtual DbSet<ServiceInCategory> ServicesInCategory { get; set; }
        public virtual DbSet<CustomService> CustomServices { get; set; }
        public virtual DbSet<HotPromotion> HotPromotions { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsImage> NewsImages { get; set; }
        public virtual DbSet<SupplierCategory> SupplierCategories { get; set; }
        public virtual DbSet<CategoryInSupplier> CategoryInSuppliers { get; set; }
        public virtual DbSet<SupplierProduct> SupplierProducts { get; set; }
    }
    public class MessageTitle
    {
        public MessageTitle()
        {

        }
        public MessageTitle(string applicationUserId, string title)
        {
            ApplicationUserId = applicationUserId;
            Title = title;
            Messages = new List<MessageBody>();
        }
        public int Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string Title { get; set; }
        public ICollection<MessageBody> Messages { get; set; }
    }

    public class MessageBody
    {
        public MessageBody()
        {

        }
        public MessageBody(string body, string applicationUserId)
        {
            Body = body;
            ApplicationUserId = applicationUserId;
            IsReaded = false;
        }
        public int Id { get; set; }
        public int MessageTitleId { get; set; }
        public string Body { get; set; }
        public string ApplicationUserId { get; set; }
        public bool IsReaded { get; set; }
    }

    public class ScheduleRequest
    {
        public ScheduleRequest()
        {
            var now = DateTime.Now;
            StartDate = now;
            EndDate = now;
            IsActive = false;
        }
        public ScheduleRequest(ScheduleCost cost) : this()
        {
            Cost = cost;
        }
        public int Id { get; set; }
        public ScheduleCost Cost { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public int FirmId { get; set; }
    }

    public class Vacancy
    {
        public Vacancy()
        {
            EndDate = StartDate = DateTime.Now;
            var model = BPModel.Create();
            IsActive = false;
        }
        public int Id { get; set; }
        public int FirmId { get; set; }
        public VacancyCost Cost { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [DefaultValue(false)]
        public bool IsActive { get; set; }
        public decimal MinPayment { get; set; }
        public decimal MaxPayment { get; set; }
        [Range(0, 100,
        ErrorMessage = "�������� {0} ������ ���� �� {1} �� {2}.")]
        public int? Percent { get; set; }
        [Required]
        [MaxLength(50)]
        public string ContactName { get; set; }
        public VacancyName VacancyName { get; set; }
        public VacancyWorkExperience Experience { get; set; }
        public Email Email { get; set; }
        [Required]
        public Phone Phone { get; set; }
        public ICollection<Link> Links { get; set; }
        public Street Street { get; set; }
        public City City { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Text { get; set; }

        public string House { get; set; }
    }

    public class VacancyName
    {
        public VacancyName()
        {
            CreateDate = DateTime.Now;
            IsActive = false;
        }
        public VacancyName(string name, string creatorId) : this()
        {
            Name = name;
            CreatorId = creatorId;
        }
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatorId { get; set; }
        public bool IsActive { get; set; }
    }

    public class PromotionInCategories
    {
        public int Id { get; set; }
        public int PromotionId { get; set; }
        public Category Category { get; set; }
    }

    public class Promotion
    {
        public Promotion()
        {
            Categories = new List<PromotionInCategories>();
        }
        public int Id { get; set; }
        public PromotionCost Cost { get; set; }
        public virtual PromotionDesignerCost DesignerCost { get; set; }
        public ICollection<PromotionInCategories> Categories { get; set; }
        public string Image { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Weight { get; set; }
        public bool IsActive { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int PriceId { get; set; }
        public bool IsNeedDesigner { get; set; }
    }

    public class HotPromotion
    {
        public HotPromotion() { }
        public HotPromotion(string title, string body, string image, int weight)
        {
            Title = title;
            Body = body;
            Image = image;
            Weight = weight;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Image { get; set; }
        public int FirmId { get; set; }
        public int Weight { get; set; }
    }
    public class ScheduleHotPromotion
    {
        public ScheduleHotPromotion() { }
        public ScheduleHotPromotion(HotPromotion hot, DateTime date, User user)
        {
            HotPromotion = hot;
            Date = date;
            User = user;
        }
        public int Id { get; set; }
        public HotPromotion HotPromotion { get; set; }
        public DateTime Date { get; set; }
        public User User { get; set; }
    }
    public class Banner
    {
        public int Id { get; set; }
        public BannerCost Cost { get; set; }
        public virtual BannerDesignerCost DesignerCost { get; set; }
        public string Image { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Weight { get; set; }
        public bool IsActive { get; set; }
        public string Message { get; set; }
        public int? SalonId { get; set; }
        public int? FreelanceId { get; set; }
    }
    public class ScheduleCost
    {
        public int Id { get; set; }
        public decimal Cost { get; set; }
    }

    public class VacancyCost
    {
        public int Id { get; set; }
        public decimal Cost { get; set; }
    }

    public class PromotionCost
    {
        public int Id { get; set; }
        public decimal Cost { get; set; }
    }

    public class BannerCost
    {
        public int Id { get; set; }
        public decimal Cost { get; set; }
    }
    public class BannerDesignerCost
    {
        public int Id { get; set; }
        public decimal Cost { get; set; }
    }

    public class PromotionDesignerCost
    {
        public int Id { get; set; }
        public decimal Cost { get; set; }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
        public string Description { get; set; }
    }
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
            : base(roleStore)
        {
        }
        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(
                new RoleStore<ApplicationRole>(context.Get<BPModel>()));
        }
    }
    public class User : IdentityUser
    {
        public User()
        {
            DateRegistration = DateTime.Now;
            Status = false;
            Firms = new List<Firm>();
            Messages = new List<MessageTitle>();
            Schedules = new List<Schedule>();
            Firms = new List<Firm>();
            Suppliers = new List<Supplier>();
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }
        [MaxLength(75)]
        public string Name { get; set; }
        public bool Status { get; set; }
        public DateTime DateRegistration { get; set; }
        public DateTime DateBirthday { get; set; }
        [MaxLength(36)]
        public string Image { get; set; }
        public ICollection<Firm> Firms { get; set; }
        public ICollection<Supplier> Suppliers { get; set; }
        public string VerificationNumber { get; set; }
        public ICollection<MessageTitle> Messages { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
    }
    public class Firm
    {
        public Firm()
        {

        }
        public Firm(string name, City city, Street street)
        {
            Name = name;
            City = city;
            Street = street;
            Phones = new List<Phone>();
            Emails = new List<Email>();
            Links = new List<Link>();
            WorkTimes = new List<WorkTime>();
            ScheduleRequests = new List<ScheduleRequest>();
            Schedules = new List<Schedule>();
            Categories = new List<CategoryInSalon>();
            WorkTimes = new List<WorkTime>();
            Links = new List<Link>();
            Emails = new List<Email>();
            Phones = new List<Phone>();
            Vacancies = new List<Vacancy>();
            HotPromotions = new List<HotPromotion>();
            ScheduleHotPromotions = new List<ScheduleHotPromotion>();
            IsActive = false;
            RegisterDate = DateTime.Now;
        }
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        public City City { get; set; }
        public Street Street { get; set; }
        [MaxLength(20)]
        public string House { get; set; }
        [MaxLength(30)]
        public string Floor { get; set; }
        [MaxLength(20)]
        public string Office { get; set; }
        public bool IsCashlessPayment { get; set; }
        public bool IsWorkAtHome { get; set; }
        public bool IsWifi { get; set; }
        public bool IsActive { get; set; }
        public bool IsFreelance { get; set; }
        public ICollection<Phone> Phones { get; set; }
        public ICollection<Email> Emails { get; set; }
        public ICollection<Link> Links { get; set; }
        public ICollection<WorkTime> WorkTimes { get; set; }
        public ICollection<CategoryInSalon> Categories { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
        public ICollection<ScheduleRequest> ScheduleRequests { get; set; }
        public ICollection<HotPromotion> HotPromotions { get; set; }
        public ICollection<ScheduleHotPromotion> ScheduleHotPromotions { get; set; }
        public ICollection<Promotion> Promotions { get; set; }
        public ICollection<Banner> Banners { get; set; }
        public Coordinates Coordinates { get; set; }
        public string DiscountForBirthday { get; set; }
        public string WorkWithProduction { get; set; }
        public string Description { get; set; }
        public int Scores { get; set; }
        public string Image { get; set; }
        public ICollection<Vacancy> Vacancies { get; set; }
        public DateTime RegisterDate { get; set; }
        public decimal Rating { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
    public class Review
    {
        public Review()
        {
            IsActive = false;
            Date = DateTime.Now;
        }
        public Review(string text, decimal rating, string userId) : this()
        {
            Text = text;
            Rating = rating;
            UserId = userId;
        }
        public int Id { get; set; }
        public string Text { get; set; }
        public decimal Rating { get; set; }
        public bool IsActive { get; set; }
        public DateTime Date { get; set; }
        public string UserId { get; set; }

    }
    public class Supplier
    {
        public Supplier()
        {

        }
        public Supplier(string userId, string name, Street street, City city)
        {
            UserId = userId;
            Name = name;
            Street = street;
            City = city;
            Phones = new List<Phone>();
            Email = new Email();
            Links = new List<Link>();
            WorkTimes = new List<WorkTime>();
            IsActive = false;
            RegisterDate = DateTime.Now;
        }
        public int Id { get; set; }
        [MaxLength(100)]
        public string UserId { get; set; }

        public string Name { get; set; }
        public Street Street { get; set; }
        public City City { get; set; }
        public string House { get; set; }
        public string Office { get; set; }
        public ICollection<Phone> Phones { get; set; }
        public Email Email { get; set; }
        public ICollection<Link> Links { get; set; }
        public ICollection<WorkTime> WorkTimes { get; set; }
        public string Description { get; set; }
        public int Scores { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        public DateTime RegisterDate { get; set; }
        public ICollection<CategoryInSupplier> Categories { get; set; }
    }
    public class SupplierCategory
    {
        public SupplierCategory() { }
        public SupplierCategory(string image, string name)
        {
            Image = image;
            Name = name;
        }
        public int Id { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public ICollection<SupplierProduct> SupplierProducts { get; set; }
    }
    public class CategoryInSupplier
    {
        public CategoryInSupplier()
        {
            SupplierProductions = new List<SupplierProduct>();
        }
        public CategoryInSupplier(SupplierCategory category) : this()
        {
            Category = category;            
        }
        public int Id { get; set; }
        public ICollection<SupplierProduct> SupplierProductions { get; set; }
        public SupplierCategory Category { get; set; }
    }
    public class SupplierProduct
    {
        public SupplierProduct() { }
        public SupplierProduct(string name, string creatorId, int supplierCategoryId)
        {
            Name = name;
            CreatorId = creatorId;
            CreateDate = DateTime.Now;
            IsActive = false;
            SupplierCategoryId = supplierCategoryId;
        }
        public int Id { get; set; }
        public string CreatorId { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActive { get; set; }
        public int SupplierCategoryId { get; set; }
        public string Name { get; set; }
    }
    public class Phone
    {
        public int Id { get; set; }
        [MaxLength(12)]
        public string Number { get; set; }
    }

    public class Email
    {
        public int Id { get; set; }
        public string Address { get; set; }
    }

    public class Link
    {
        public int Id { get; set; }
        public LinkType LinkType { get; set; }
        public string Address { get; set; }
    }

    public class LinkType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class WorkTime
    {
        public int Id { get; set; }
        public WorkTime() { }
        public WorkTime(DayOfWeek dayOfWeek, TimeSpan open, TimeSpan close)
        {
            DayOfWeek = dayOfWeek;
            Open = open;
            Close = close;
        }
        public DayOfWeek DayOfWeek { get; set; }
        public TimeSpan Open { get; set; }
        public TimeSpan Close { get; set; }
    }
    public class VacancyWorkExperience
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
    public class DayOfWeek
    {
        public DayOfWeek()
        {

        }
        public DayOfWeek(string name)
        {
            Name = name;
        }
        public int Id { get; set; }
        [MaxLength(20)]
        public string Name { get; set; }
    }
    public class CategoryInSalon
    {
        public CategoryInSalon()
        {

        }
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public CategoryInSalon(Category category)
        {
            Portfolios = new List<Portfolio>();
            Services = new List<ServiceInCategory>();
            CustomServices = new List<CustomService>();
            CategoryId = category.Id;
            Category = category;
        }
        public ICollection<Portfolio> Portfolios { get; set; }
        public ICollection<ServiceInCategory> Services { get; set; }
        public ICollection<CustomService> CustomServices { get; set; }
    }
    public class Category
    {
        public Category()
        {

        }
        public Category(string image, string name, int priority)
        {
            Image = image;
            Name = name;
            Alias = Transliteration.CyrillicToLatin(name, Language.Russian).Replace(" ", "_").Replace("`", "");
            Priority = priority;
        }
        public int Id { get; set; }
        [MaxLength(36)]
        public string Image { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string Alias { get; set; }
        public int Priority { get; set; }
    }
    public class City
    {
        public City()
        {
            CreateDate = DateTime.Now;
            IsActive = false;
        }
        public City(string name, string creatorId) : this()
        {
            Name = name;
            CreatorId = creatorId;
        }
        public DateTime CreateDate { get; set; }
        public string CreatorId { get; set; }
        public bool IsActive { get; set; }
        public int Id { get; set; }
        [MaxLength(20)]
        public string Name { get; set; }
    }
    public class Street
    {
        public Street()
        {
            IsActive = false;
        }
        public Street(string name, int cityId, string creatorId) : this()
        {
            Name = name;
            CityId = cityId;
            CreatorId = creatorId;
        }
        public bool IsActive { get; set; }
        public int CityId { get; set; }
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        public string CreatorId { get; set; }
    }
    public class ServiceInCategory
    {
        public ServiceInCategory()
        {

        }
        public ServiceInCategory(Service service, Price price)
        {
            Service = service;
            Price = price;
        }
        public int Id { get; set; }
        public Service Service { get; set; }
        public Price Price { get; set; }
    }
    public class Service
    {
        public Service()
        {

        }
        public Service(string name, int priority, int categoryId)
        {
            Name = name;
            Alias = Transliteration.CyrillicToLatin(name, Language.Russian).Replace(" ", "_").Replace("`", "");
            Priority = priority;
            CategoryId = categoryId;
        }
        public int Id { get; set; }
        [MaxLength(150)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string Alias { get; set; }
        public int Priority { get; set; }
        public int CategoryId { get; set; }
    }
    public class CustomService
    {
        public CustomService()
        {

        }
        public CustomService(string name, int priority, Price price)
        {
            Name = name;
            Alias = Transliteration.CyrillicToLatin(name, Language.Russian).Replace(" ", "_").Replace("`", "");
            Priority = priority;
            Price = price;
        }
        public int Id { get; set; }
        [MaxLength(150)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string Alias { get; set; }
        public int Priority { get; set; }
        public Price Price { get; set; }
    }
    public class Price
    {
        public Price() { }
        public Price(float min, float max)
        {
            Min = min;
            Max = max;
        }
        public int Id { get; set; }
        public float Min { get; set; }
        public float Max { get; set; }
    }
    public class Portfolio
    {
        public Portfolio()
        {

        }
        public Portfolio(string image)
        {
            Image = image;
        }
        public int Id { get; set; }
        [MaxLength(36)]
        public string Image { get; set; }
    }

    public class Schedule
    {
        public Schedule()
        {

        }
        public Schedule(string userId, Service service, Price price, DateTime date)
        {
            UserId = userId;
            Service = service;
            Price = price;
            Date = date;
            ScheduleStatus = ScheduleStatus.IsWaiting;
        }
        public int Id { get; set; }
        [MaxLength(100)]
        public string UserId { get; set; }
        public Service Service { get; set; }
        public Price Price { get; set; }
        public DateTime Date { get; set; }
        public ScheduleStatus ScheduleStatus { get; set; }
    }
    public enum ScheduleStatus
    {
        IsWaiting,
        IsAccepted,
        IsDone,
        IsCanceled
    }

    public class Coordinates
    {
        public Coordinates()
        {

        }
        public Coordinates(float latitude, float longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
            UpdateDate = DateTime.Now;
        }
        public int Id { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class News
    {
        public News()
        {
            Date = DateTime.Now;
            Images = new List<NewsImage>();
        }

        public News(string title, string body, NewsImage titleImage) : this()
        {
            Title = title;
            Body = body;
            Preview = body.Substring(0, 100);
            TitleImage = titleImage;
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Preview { get; set; }
        public NewsImage TitleImage { get; set; }
        public ICollection<NewsImage> Images { get; set; }
    }
    public class NewsImage
    {
        public NewsImage() { }
        public NewsImage(string image)
        {
            Image = image;
        }
        public int Id { get; set; }
        public string Image { get; set; }
    }
}