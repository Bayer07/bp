﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BP.Models
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
        public string Alias { get; set; }
    }
}