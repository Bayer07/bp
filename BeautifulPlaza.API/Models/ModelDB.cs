﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BP.Models
{
    public class CategoriesInSalonDB
    {
        public int id_user { get; set; }
        public int id_category { get; set; }
    }

    public class PricesDB
    {
        public int? id_user { get; set; }
        public int? id_category { get; set; }
        public int? cena { get; set; }
        public int? id_podcategory { get; set; }
        public int? cena_do { get; set; }
    }

    public class PortfolioDB
    {
        public int? id_user { get; set; }
        public int? id_category { get; set; }
        public string name_file { get; set; }
    }



    public class ServicesDB
    {
        public int? id { get; set; }
        public string name { get; set; }
        public int? first { get; set; }
        public bool client_add { get; set; }
        public bool del { get; set; }
        public int? sort { get; set; }
    }

    public class SalonDB
    {
        public int? id_user { get; set; }
        public bool frilans { get; set; }
        public bool mastercard { get; set; }
        public bool wifi { get; set; }
        public bool viezd { get; set; }
        public string company_name { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string home { get; set; }
        public string etaj { get; set; }
        public string phone { get; set; }
        public string mail { get; set; }
        public string produkcia { get; set; }
        public string comment { get; set; }
        public string vr_pn { get; set; }
        public string vr_vt { get; set; }
        public string vr_sr { get; set; }
        public string vr_cht { get; set; }
        public string vr_pt { get; set; }
        public string vr_sb { get; set; }
        public string vr_vs { get; set; }
        public string vr_pn_do { get; set; }
        public string vr_vt_do { get; set; }
        public string vr_sr_do { get; set; }
        public string vr_cht_do { get; set; }
        public string vr_pt_do { get; set; }
        public string vr_sb_do { get; set; }
        public string vr_vs_do { get; set; }
        public DateTime? dtreg { get; set; }
        public int? user_create { get; set; }
        public int? id { get; set; }
        public int? status { get; set; }
        public string phone2 { get; set; }
        public string phone3 { get; set; }
        public string ofis { get; set; }
        public string mail2 { get; set; }
        public string mail3 { get; set; }
        public int? rating { get; set; }
        public DateTime? del { get; set; }
        public string skidka_rojdenie { get; set; }
        public int ball { get; set; }
    }

    public class UserDB
    {
        public int id { get; set; }
        public string phone { get; set; }
        public string pass { get; set; }
        public string mail { get; set; }
        public string imya { get; set; }
        public string familia { get; set; }
        public string otchestvo { get; set; }
        public bool status { get; set; }
        public string sms_rnd { get; set; }
        public DateTime dtreg { get; set; }
        public DateTime dtroj { get; set; }
        public string cookies { get; set; }
        public bool salon { get; set; }
        public bool adm { get; set; }
        public string ava { get; set; }
        public bool moder { get; set; }
        public string restore_pass { get; set; }
    }
}