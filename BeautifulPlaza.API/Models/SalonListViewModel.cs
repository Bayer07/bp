﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BP.Models
{
    public class SalonListViewModel
    {
        public string name { get; set; }
        public string image { get; set; }
        public string address { get; set; }
        public string description { get; set; }
        public List<CategoryViewModel> categories { get; set; }
        public int scores { get; set; }
        public bool isChashless { get; set; }
        public bool isWifi { get; set; }
        public int id { get; set; }
    }
}