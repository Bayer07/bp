namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeannotaionsemail : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Emails", "Address", c => c.String());
            AlterColumn("dbo.Links", "Address", c => c.String());
            AlterColumn("dbo.LinkTypes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LinkTypes", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Links", "Address", c => c.String(maxLength: 50));
            AlterColumn("dbo.Emails", "Address", c => c.String(maxLength: 50));
        }
    }
}
