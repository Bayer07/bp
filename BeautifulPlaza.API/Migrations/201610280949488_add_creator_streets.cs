namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_creator_streets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Streets", "CreatorId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Streets", "CreatorId");
        }
    }
}
