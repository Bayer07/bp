namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesuppliercategory : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SupplierProductions", newName: "SupplierProducts");
            AddColumn("dbo.SupplierProducts", "CreatorId", c => c.String());
            AddColumn("dbo.SupplierProducts", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.SupplierProducts", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.SupplierProducts", "SupplierCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.SupplierProducts", "SupplierCategoryId");
            AddForeignKey("dbo.SupplierProducts", "SupplierCategoryId", "dbo.SupplierCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierProducts", "SupplierCategoryId", "dbo.SupplierCategories");
            DropIndex("dbo.SupplierProducts", new[] { "SupplierCategoryId" });
            DropColumn("dbo.SupplierProducts", "SupplierCategoryId");
            DropColumn("dbo.SupplierProducts", "IsActive");
            DropColumn("dbo.SupplierProducts", "CreateDate");
            DropColumn("dbo.SupplierProducts", "CreatorId");
            RenameTable(name: "dbo.SupplierProducts", newName: "SupplierProductions");
        }
    }
}
