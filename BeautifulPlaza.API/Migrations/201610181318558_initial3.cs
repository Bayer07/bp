namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustomServices", "Name", c => c.String(maxLength: 150));
            AlterColumn("dbo.CustomServices", "Alias", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomServices", "Alias", c => c.String(maxLength: 50));
            AlterColumn("dbo.CustomServices", "Name", c => c.String(maxLength: 50));
        }
    }
}
