namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class implement_hot_promotions_Weight : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HotPromotions", "Weight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HotPromotions", "Weight");
        }
    }
}
