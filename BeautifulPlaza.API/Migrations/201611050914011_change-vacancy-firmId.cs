namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changevacancyfirmId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Vacancies", "Firm_Id", "dbo.Firms");
            DropIndex("dbo.Vacancies", new[] { "Firm_Id" });
            RenameColumn(table: "dbo.Vacancies", name: "Firm_Id", newName: "FirmId");
            AlterColumn("dbo.Vacancies", "FirmId", c => c.Int(nullable: false));
            CreateIndex("dbo.Vacancies", "FirmId");
            AddForeignKey("dbo.Vacancies", "FirmId", "dbo.Firms", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vacancies", "FirmId", "dbo.Firms");
            DropIndex("dbo.Vacancies", new[] { "FirmId" });
            AlterColumn("dbo.Vacancies", "FirmId", c => c.Int());
            RenameColumn(table: "dbo.Vacancies", name: "FirmId", newName: "Firm_Id");
            CreateIndex("dbo.Vacancies", "Firm_Id");
            AddForeignKey("dbo.Vacancies", "Firm_Id", "dbo.Firms", "Id");
        }
    }
}
