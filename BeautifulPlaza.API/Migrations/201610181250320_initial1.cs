namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.Categories", "Alias", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categories", "Alias", c => c.String(maxLength: 50));
            AlterColumn("dbo.Categories", "Name", c => c.String(maxLength: 50));
        }
    }
}
