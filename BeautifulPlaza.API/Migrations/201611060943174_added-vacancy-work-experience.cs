namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedvacancyworkexperience : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VacancyWorkExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Vacancies", "Experience_Id", c => c.Int());
            CreateIndex("dbo.Vacancies", "Experience_Id");
            AddForeignKey("dbo.Vacancies", "Experience_Id", "dbo.VacancyWorkExperiences", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vacancies", "Experience_Id", "dbo.VacancyWorkExperiences");
            DropIndex("dbo.Vacancies", new[] { "Experience_Id" });
            DropColumn("dbo.Vacancies", "Experience_Id");
            DropTable("dbo.VacancyWorkExperiences");
        }
    }
}
