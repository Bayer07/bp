namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inmplementsuppliercategories : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryInSuppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category_Id = c.Int(),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SupplierCategories", t => t.Category_Id)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.SupplierCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        Name = c.String(),
                        Alias = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SupplierProductions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CategoryInSupplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryInSuppliers", t => t.CategoryInSupplier_Id)
                .Index(t => t.CategoryInSupplier_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryInSuppliers", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierProductions", "CategoryInSupplier_Id", "dbo.CategoryInSuppliers");
            DropForeignKey("dbo.CategoryInSuppliers", "Category_Id", "dbo.SupplierCategories");
            DropIndex("dbo.SupplierProductions", new[] { "CategoryInSupplier_Id" });
            DropIndex("dbo.CategoryInSuppliers", new[] { "Supplier_Id" });
            DropIndex("dbo.CategoryInSuppliers", new[] { "Category_Id" });
            DropTable("dbo.SupplierProductions");
            DropTable("dbo.SupplierCategories");
            DropTable("dbo.CategoryInSuppliers");
        }
    }
}
