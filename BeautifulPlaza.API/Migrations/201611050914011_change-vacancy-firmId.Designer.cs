// <auto-generated />
namespace BP.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class changevacancyfirmId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(changevacancyfirmId));
        
        string IMigrationMetadata.Id
        {
            get { return "201611050914011_change-vacancy-firmId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
