namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inplementingreviews : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Rating = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.Firm_Id);
            
            AddColumn("dbo.Firms", "Rating", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "Firm_Id", "dbo.Firms");
            DropIndex("dbo.Reviews", new[] { "Firm_Id" });
            DropColumn("dbo.Firms", "Rating");
            DropTable("dbo.Reviews");
        }
    }
}
