namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changevacancyNamecreatorId : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.VacancyNames", "CreatorId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VacancyNames", "CreatorId", c => c.String(nullable: false));
        }
    }
}
