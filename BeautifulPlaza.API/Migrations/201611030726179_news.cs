namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class news : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Title = c.String(),
                        Body = c.String(),
                        Preview = c.String(),
                        TitleImage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NewsImages", t => t.TitleImage_Id)
                .Index(t => t.TitleImage_Id);
            
            CreateTable(
                "dbo.NewsImages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        News_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.News", t => t.News_Id)
                .Index(t => t.News_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.News", "TitleImage_Id", "dbo.NewsImages");
            DropForeignKey("dbo.NewsImages", "News_Id", "dbo.News");
            DropIndex("dbo.NewsImages", new[] { "News_Id" });
            DropIndex("dbo.News", new[] { "TitleImage_Id" });
            DropTable("dbo.NewsImages");
            DropTable("dbo.News");
        }
    }
}
