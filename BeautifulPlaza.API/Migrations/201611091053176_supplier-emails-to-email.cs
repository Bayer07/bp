namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class supplieremailstoemail : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Emails", "Supplier_Id", "dbo.Suppliers");
            DropIndex("dbo.Emails", new[] { "Supplier_Id" });
            AddColumn("dbo.Suppliers", "Email_Id", c => c.Int());
            CreateIndex("dbo.Suppliers", "Email_Id");
            AddForeignKey("dbo.Suppliers", "Email_Id", "dbo.Emails", "Id");
            DropColumn("dbo.Emails", "Supplier_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Emails", "Supplier_Id", c => c.Int());
            DropForeignKey("dbo.Suppliers", "Email_Id", "dbo.Emails");
            DropIndex("dbo.Suppliers", new[] { "Email_Id" });
            DropColumn("dbo.Suppliers", "Email_Id");
            CreateIndex("dbo.Emails", "Supplier_Id");
            AddForeignKey("dbo.Emails", "Supplier_Id", "dbo.Suppliers", "Id");
        }
    }
}
