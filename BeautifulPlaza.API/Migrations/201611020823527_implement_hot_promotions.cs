namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class implement_hot_promotions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HotPromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        Image = c.String(),
                        FirmId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Firms", t => t.FirmId, cascadeDelete: true)
                .Index(t => t.FirmId);
            
            CreateTable(
                "dbo.ScheduleHotPromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        HotPromotion_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HotPromotions", t => t.HotPromotion_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.HotPromotion_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Firm_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleHotPromotions", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.ScheduleHotPromotions", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ScheduleHotPromotions", "HotPromotion_Id", "dbo.HotPromotions");
            DropForeignKey("dbo.HotPromotions", "FirmId", "dbo.Firms");
            DropIndex("dbo.ScheduleHotPromotions", new[] { "Firm_Id" });
            DropIndex("dbo.ScheduleHotPromotions", new[] { "User_Id" });
            DropIndex("dbo.ScheduleHotPromotions", new[] { "HotPromotion_Id" });
            DropIndex("dbo.HotPromotions", new[] { "FirmId" });
            DropTable("dbo.ScheduleHotPromotions");
            DropTable("dbo.HotPromotions");
        }
    }
}
