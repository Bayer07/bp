namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFirmIdToScheduleRequest : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScheduleRequests", "Firm_Id", "dbo.Firms");
            DropIndex("dbo.ScheduleRequests", new[] { "Firm_Id" });
            RenameColumn(table: "dbo.ScheduleRequests", name: "Firm_Id", newName: "FirmId");
            AlterColumn("dbo.ScheduleRequests", "FirmId", c => c.Int(nullable: false));
            CreateIndex("dbo.ScheduleRequests", "FirmId");
            AddForeignKey("dbo.ScheduleRequests", "FirmId", "dbo.Firms", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleRequests", "FirmId", "dbo.Firms");
            DropIndex("dbo.ScheduleRequests", new[] { "FirmId" });
            AlterColumn("dbo.ScheduleRequests", "FirmId", c => c.Int());
            RenameColumn(table: "dbo.ScheduleRequests", name: "FirmId", newName: "Firm_Id");
            CreateIndex("dbo.ScheduleRequests", "Firm_Id");
            AddForeignKey("dbo.ScheduleRequests", "Firm_Id", "dbo.Firms", "Id");
        }
    }
}
