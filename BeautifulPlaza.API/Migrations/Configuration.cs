﻿namespace BP.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Models;
    using NickBuhro.Translit;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Data.Odbc;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Web;
    using System.Web.Hosting;

    internal sealed class Configuration : DbMigrationsConfiguration<BPModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        const string pathToPictures = @"F:\Cloud\Beautiplaza\img\";
        static string pathToSalonPictures = @"C:\Repository\BeautifulPlaza\BeautifulPlaza.API\images\salons";
        static string pathToFreePictures = @"C:\Repository\BeautifulPlaza\BeautifulPlaza.API\images\freelancers";
        static string pathToPortfolioPictures = @"C:\Repository\BeautifulPlaza\BeautifulPlaza.API\images\portfolio";

        protected override void Seed(BPModel context)
        {
            db = context;
            InitializeIdentityForEF();
        }

        private static int? ReadInt(object obj)
        {
            return obj.ToString().Length > 0 ? (int?)Convert.ToInt32(obj.ToString()) : null;
        }

        private static int ReadIntNotNull(object obj)
        {
            return (int)Convert.ToInt32(obj.ToString());
        }

        private static bool ReadBool(object obj)
        {
            return obj.ToString() == "0" ? false : true;
        }

        private static string ReadString(object obj)
        {
            return obj.ToString();
        }

        private static DateTime? ReadDate(object obj)
        {
            return obj.ToString().Length > 0 ? (DateTime?)DateTime.Parse(obj.ToString()) : null;
        }

        private static List<UserDB> ReadUsers(string queryString, string cs)
        {
            var list = new List<UserDB>();

            using (OdbcConnection connection = new OdbcConnection(cs))
            {
                OdbcCommand command = new OdbcCommand(queryString, connection);
                connection.Open();
                OdbcDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var user = new UserDB();
                    user.id = (int)reader[0];
                    user.phone = reader[1].ToString();
                    user.pass = reader[2].ToString();
                    user.mail = reader[3].ToString();
                    user.imya = reader[4].ToString();
                    user.familia = reader[5].ToString();
                    user.otchestvo = reader[6].ToString();
                    user.status = (int)reader[7] == 0 ? false : true;
                    user.sms_rnd = reader[8].ToString();
                    user.dtreg = DateTime.Parse(reader[9].ToString());
                    user.dtroj = DateTime.Parse(reader[10].ToString());
                    user.cookies = reader[11].ToString();
                    user.salon = reader[12].ToString() == "0" ? false : true;
                    user.adm = reader[13].ToString() == "0" ? false : true;
                    user.ava = reader[14].ToString();
                    user.moder = reader[15].ToString() == "0" ? false : true;
                    user.restore_pass = reader[17].ToString();
                    list.Add(user);
                }
                return list;
            }
        }

        private static List<SalonDB> ReadSalons(string queryString, string cs)
        {
            var list = new List<SalonDB>();

            using (OdbcConnection connection = new OdbcConnection(cs))
            {
                OdbcCommand command = new OdbcCommand(queryString, connection);
                connection.Open();
                OdbcDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var salon = new SalonDB()
                    {
                        id_user = ReadInt(reader[0]),
                        frilans = ReadBool(reader[1]),
                        company_name = ReadString(reader[2]),
                        mastercard = ReadBool(reader[3]),
                        wifi = ReadBool(reader[4]),
                        country = ReadString(reader[5]),
                        city = ReadString(reader[6]),
                        street = ReadString(reader[7]),
                        home = ReadString(reader[8]),
                        etaj = ReadString(reader[9]),
                        phone = ReadString(reader[10]),
                        mail = ReadString(reader[11]),
                        viezd = ReadBool(reader[12]),
                        produkcia = ReadString(reader[13]),
                        comment = ReadString(reader[14]),
                        dtreg = ReadDate(reader[15]),
                        vr_pn = ReadString(reader[16]),
                        vr_vt = ReadString(reader[17]),
                        vr_sr = ReadString(reader[18]),
                        vr_cht = ReadString(reader[19]),
                        vr_pt = ReadString(reader[20]),
                        vr_sb = ReadString(reader[21]),
                        vr_vs = ReadString(reader[22]),
                        vr_pn_do = ReadString(reader[23]),
                        vr_vt_do = ReadString(reader[24]),
                        vr_sr_do = ReadString(reader[25]),
                        vr_cht_do = ReadString(reader[26]),
                        vr_pt_do = ReadString(reader[27]),
                        vr_sb_do = ReadString(reader[28]),
                        vr_vs_do = ReadString(reader[29]),
                        user_create = ReadInt(reader[30]),
                        id = ReadInt(reader[31]),
                        status = ReadInt(reader[32]),
                        phone2 = ReadString(reader[33]),
                        phone3 = ReadString(reader[34]),
                        ofis = ReadString(reader[35]),
                        mail2 = ReadString(reader[36]),
                        mail3 = ReadString(reader[37]),
                        rating = ReadInt(reader[38]),
                        del = ReadDate(reader[39]),
                        skidka_rojdenie = ReadString(reader[40]),
                        ball = ReadIntNotNull(reader[41])
                    };
                    list.Add(salon);
                }
                return list;
            }
        }

        private static List<CategoriesInSalonDB> ReadCategoriesInSalon(string queryString, string cs)
        {
            var list = new List<CategoriesInSalonDB>();
            using (OdbcConnection connection = new OdbcConnection(cs))
            {
                OdbcCommand command = new OdbcCommand(queryString, connection);
                connection.Open();
                OdbcDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var categoriesInSalonDB = new CategoriesInSalonDB();
                    categoriesInSalonDB.id_user = (int)reader[0];
                    categoriesInSalonDB.id_category = (int)reader[1];
                    list.Add(categoriesInSalonDB);
                }
            }
            return list;
        }
        private static List<ServicesDB> ReadServices(string queryString, string cs)
        {
            var list = new List<ServicesDB>();

            using (OdbcConnection connection = new OdbcConnection(cs))
            {
                OdbcCommand command = new OdbcCommand(queryString, connection);
                connection.Open();
                OdbcDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var service = new ServicesDB();
                    service.id = ReadInt(reader[0]);
                    service.name = ReadString(reader[1]);
                    service.first = ReadInt(reader[2]);
                    service.client_add = ReadBool(reader[3]);
                    service.del = ReadBool(reader[4]);
                    service.sort = ReadInt(reader[5]);
                    list.Add(service);
                }
                return list;
            }
        }

        private static List<PricesDB> ReadPrices(string queryString, string cs)
        {
            var list = new List<PricesDB>();

            using (OdbcConnection connection = new OdbcConnection(cs))
            {
                OdbcCommand command = new OdbcCommand(queryString, connection);
                connection.Open();
                OdbcDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var price = new PricesDB();
                    price.id_user = ReadInt(reader[0]);
                    price.id_category = ReadInt(reader[1]);
                    price.cena = ReadInt(reader[2]);
                    price.id_podcategory = ReadInt(reader[3]);
                    price.cena_do = ReadInt(reader[4]);
                    list.Add(price);
                }
                return list;
            }
        }

        private static List<PortfolioDB> ReadPortfolio(string queryString, string cs)
        {
            var list = new List<PortfolioDB>();

            using (OdbcConnection connection = new OdbcConnection(cs))
            {
                OdbcCommand command = new OdbcCommand(queryString, connection);
                connection.Open();
                OdbcDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var portfolio = new PortfolioDB();
                    portfolio.id_user = ReadInt(reader[0]);
                    portfolio.id_category = ReadInt(reader[1]);
                    portfolio.name_file = ReadString(reader[2]);
                    list.Add(portfolio);
                }
                return list;
            }
        }

        private static void CreateUsers(UserManager<User> userManager, List<UserDB> usersDB, ApplicationRoleManager roleManager, List<SalonDB> salons)
        {
            if (db.Users.Count() != 0)
                return;
            var adminRole = roleManager.FindByName(roleAdministrator);
            var salonRole = roleManager.FindByName(roleSalon);
            var freelanceRole = roleManager.FindByName(roleFreelance);
            var clientRole = roleManager.FindByName(roleClient);

            foreach (UserDB user in usersDB)
            {
                var exist = userManager.FindByName(user.phone);
                if (exist == null)
                {
                    var au = new User()
                    {
                        UserName = user.phone,
                        PhoneNumber = user.phone,
                        PhoneNumberConfirmed = true,
                        Email = user.mail,
                        EmailConfirmed = true,
                        Name = string.Format("{0} {1} {2}", user.imya, user.familia, user.otchestvo),
                        Status = user.status,
                        DateRegistration = user.dtreg,
                        DateBirthday = user.dtroj,
                        Image = user.ava
                    };
                    userManager.Create(au, user.pass);

                    if (user.phone == "79137824284")
                    {
                        adminRole.Users.Add(new IdentityUserRole() { RoleId = adminRole.Id, UserId = au.Id });
                        db.SaveChanges();
                        continue;
                    }

                    if (user.salon == true)
                    {
                        try
                        {
                            var salon = salons.First(x => x.id_user == user.id);
                            if (salon.frilans)
                            {
                                freelanceRole.Users.Add(new IdentityUserRole() { RoleId = freelanceRole.Id, UserId = au.Id });
                            }
                            else
                            {
                                salonRole.Users.Add(new IdentityUserRole() { RoleId = salonRole.Id, UserId = au.Id });
                            }
                        }
                        catch
                        {
                            clientRole.Users.Add(new IdentityUserRole() { RoleId = clientRole.Id, UserId = au.Id });
                        }
                    }
                    else
                    {
                        clientRole.Users.Add(new IdentityUserRole() { RoleId = clientRole.Id, UserId = au.Id });
                    }
                }
                db.SaveChanges();
            }
        }
        static string trimPhone(string phone)
        {
            string trimmedPhone = phone;
            trimmedPhone = trimmedPhone.Replace("−", string.Empty);
            trimmedPhone = trimmedPhone.Replace(" ", string.Empty);
            trimmedPhone = trimmedPhone.Replace("-", string.Empty);
            trimmedPhone = trimmedPhone.Replace("+", string.Empty);
            trimmedPhone = trimmedPhone.Replace("(", string.Empty);
            trimmedPhone = trimmedPhone.Replace(")", string.Empty);
            return trimmedPhone;
        }

        static BPModel db;
        static void FillDayOfWeek(string day)
        {
            if (!db.DaysOfWeek.Any(x => x.Name == day))
            {
                db.DaysOfWeek.Add(new Models.DayOfWeek(day));
            }
        }
        private static void CreateSalons(UserManager<User> userManager, List<SalonDB> salons, List<UserDB> users)
        {
            if (db.LinkTypes.Count() == 0)
            {
                var url = new LinkType() { Name = "Url" };
                db.LinkTypes.AddOrUpdate(x => x.Name, url);
            }

            var monday = new Models.DayOfWeek("Понедельник");
            var tuesday = new Models.DayOfWeek("Вторник");
            var wednesday = new Models.DayOfWeek("Среда");
            var thursday = new Models.DayOfWeek("Четверг");
            var friday = new Models.DayOfWeek("Пятница");
            var saturday = new Models.DayOfWeek("Суббота");
            var sunday = new Models.DayOfWeek("Воскресенье");

            if (db.DaysOfWeek.Count() == 0)
            {
                db.DaysOfWeek.AddOrUpdate(x => x.Name, monday);
                db.DaysOfWeek.AddOrUpdate(x => x.Name, tuesday);
                db.DaysOfWeek.AddOrUpdate(x => x.Name, wednesday);
                db.DaysOfWeek.AddOrUpdate(x => x.Name, thursday);
                db.DaysOfWeek.AddOrUpdate(x => x.Name, friday);
                db.DaysOfWeek.AddOrUpdate(x => x.Name, saturday);
                db.DaysOfWeek.AddOrUpdate(x => x.Name, sunday);

                db.SaveChanges();
            }

            //DirectoryInfo di = new DirectoryInfo(@"C:\Repository\BeautifulPlaza\BeautifulPlaza.API\images\salons");
            //foreach (FileInfo file in di.GetFiles())
            //{
            //    file.Delete();
            //}

            var now = DateTime.Now;

            var cityNovosib = new City("Новосибирск", "") { IsActive = true };
            var cityBerdsk = new City("Бердск", "") { IsActive = true };
            var cityDvur = new City("Двуречье", "") { IsActive = true };
            var cityIskit = new City("Искитим", "") { IsActive = true };
            var cityKoliv = new City("Колывань", "") { IsActive = true };
            var cityKolcovo = new City("Кольцово", "") { IsActive = true };
            var cityKrasno = new City("Краснообск", "") { IsActive = true };
            var cityKrivo = new City("Криводановка", "") { IsActive = true };
            var cityOb = new City("Обь", "") { IsActive = true };

            if (db.Cities.Count() == 0)
            {
                db.Cities.AddOrUpdate(x => x.Name, cityNovosib);
                db.Cities.AddOrUpdate(x => x.Name, cityBerdsk);
                db.Cities.AddOrUpdate(x => x.Name, cityDvur);
                db.Cities.AddOrUpdate(x => x.Name, cityIskit);
                db.Cities.AddOrUpdate(x => x.Name, cityKoliv);
                db.Cities.AddOrUpdate(x => x.Name, cityKolcovo);
                db.Cities.AddOrUpdate(x => x.Name, cityKrivo);
                db.Cities.AddOrUpdate(x => x.Name, cityOb);
                db.SaveChanges();
            }


            if (db.Firms.Count() != 0)
                return;

            foreach (SalonDB salonDB in salons)
            {
                int salonId = (int)salonDB.id;
                User user = null;
                if (salonDB.id_user != null)
                {
                    var userdb = users.First(x => x.id == salonDB.id_user);
                    user = userManager.FindByName(userdb.phone);
                }
                var street = salonDB.street.ToLower();
                City city = null;
                if (street.ToLower().Contains(cityBerdsk.Name.ToLower()))
                {
                    city = cityBerdsk;
                    int index = street.IndexOf(cityBerdsk.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityBerdsk.Name.ToLower().Length, street.Length - (index + cityBerdsk.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityDvur.Name.ToLower()))
                {
                    city = cityDvur;
                    int index = street.IndexOf(cityDvur.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityDvur.Name.ToLower().Length, street.Length - (index + cityDvur.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityIskit.Name.ToLower()))
                {
                    city = cityIskit;
                    int index = street.IndexOf(cityIskit.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityIskit.Name.ToLower().Length, street.Length - (index + cityIskit.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityKoliv.Name.ToLower()))
                {
                    city = cityKoliv;
                    int index = street.IndexOf(cityKoliv.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityKoliv.Name.ToLower().Length, street.Length - (index + cityKoliv.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityKolcovo.Name.ToLower()))
                {
                    city = cityKolcovo;
                    int index = street.IndexOf(cityKolcovo.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityKolcovo.Name.ToLower().Length, street.Length - (index + cityKolcovo.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityKrasno.Name.ToLower()))
                {
                    city = cityKrasno;
                    int index = street.IndexOf(cityKrasno.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityKrasno.Name.ToLower().Length, street.Length - (index + cityKrasno.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityKrivo.Name.ToLower()))
                {
                    city = cityKrivo;
                    int index = street.IndexOf(cityKrivo.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityKrivo.Name.ToLower().Length, street.Length - (index + cityKrivo.Name.ToLower().Length));
                }
                if (street.ToLower().Contains(cityOb.Name.ToLower()))
                {
                    city = cityOb;
                    int index = street.IndexOf(cityOb.Name.ToLower());
                    street = street.Substring(0, index) + street.Substring(index + cityOb.Name.ToLower().Length, street.Length - (index + cityOb.Name.ToLower().Length));
                }
                if (city == null)
                {
                    city = db.Cities.First(x => x.Name == cityNovosib.Name.ToLower());
                }
                Street str = null;
                try
                {
                    str = db.Streets.Local.First(x => x.Name.ToLower() == street.ToLower());
                }
                catch
                {
                    str = new Street(street, city.Id,"admin");
                    db.Streets.Add(str);
                }

                var salon = new Firm(salonDB.company_name, city, str);
                salon.IsActive = true;
                salon.Description = salonDB.comment;
                salon.DiscountForBirthday = salonDB.skidka_rojdenie;
                salon.House = salonDB.home;
                salon.Floor = salonDB.etaj;
                salon.IsCashlessPayment = salonDB.mastercard;
                salon.IsWifi = salonDB.wifi;
                salon.IsWorkAtHome = salonDB.viezd;
                salon.Office = salonDB.ofis;
                salon.WorkWithProduction = salonDB.produkcia;
                salon.Scores = salonDB.ball;
                salon.IsFreelance = salonDB.frilans;
                db.Firms.Add(salon);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                }

                GetPhone(salonDB.phone, salon);
                GetPhone(salonDB.phone2, salon);
                GetPhone(salonDB.phone3, salon);
                GetMail(salonDB.mail, salon);
                GetMail(salonDB.mail2, salon);
                GetMail(salonDB.mail3, salon);
                GetTime(monday, salonDB.vr_pn, salonDB.vr_pn_do, salon);
                GetTime(tuesday, salonDB.vr_vt, salonDB.vr_vt_do, salon);
                GetTime(wednesday, salonDB.vr_sr, salonDB.vr_sr_do, salon);
                GetTime(thursday, salonDB.vr_cht, salonDB.vr_cht_do, salon);
                GetTime(friday, salonDB.vr_pt, salonDB.vr_pt_do, salon);
                GetTime(saturday, salonDB.vr_sb, salonDB.vr_sb_do, salon);
                GetTime(sunday, salonDB.vr_vs, salonDB.vr_vs_do, salon);
                if (user != null)
                {
                    user.Firms.Add(salon);
                    var userdb = users.First(x => x.id == salonDB.id_user);
                    if (!string.IsNullOrEmpty(userdb.ava))
                    {
                        var from = pathToPictures + "ava\\" + userdb.id.ToString() + "\\big-" + userdb.ava + ".jpg";
                        bool succes = false;
                        while (succes == false)
                        {
                            try
                            {
                                var image = Guid.NewGuid();
                                File.Copy(from, pathToSalonPictures + "\\" + image + ".jpg");
                                salon.Image = image.ToString();
                                succes = true;
                            }
                            catch
                            {
                                succes = false;
                            }
                        }
                    }
                }
            }
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            }
        }
        static void GetPhone(string phone, Firm salon)

        {
            if (phone != "+7 ")
            {
                var p = new Phone() { Number = trimPhone(phone) };
                salon.Phones.Add(p);
            }
        }
        static void GetTime(Models.DayOfWeek d, string from, string to, Firm salon)
        {
            TimeSpan open = new TimeSpan();
            TimeSpan close = new TimeSpan();
            if (from != "-")
            {
                var text = from.Split(':');
                open = new TimeSpan(Convert.ToInt32(text[0]), Convert.ToInt32(text[1]), 0);
            }
            if (to != "-")
            {
                var text2 = to.Split(':');
                close = new TimeSpan(Convert.ToInt32(text2[0]), Convert.ToInt32(text2[1]), 0);
            }
            var wt = new WorkTime(d, open, close);
            salon.WorkTimes.Add(wt);
        }
        static void GetMail(string mail, Firm salon)
        {
            if (!string.IsNullOrEmpty(mail))
            {
                if (IsValidEmail(mail))
                {
                    var email = new Email() { Address = mail };
                    salon.Emails.Add(email);
                }
                else if (Uri.IsWellFormedUriString(mail, UriKind.RelativeOrAbsolute))
                {
                    var newLink = new Link() { Address = mail, LinkType = db.LinkTypes.First(x => x.Name == "Url") };
                    salon.Links.Add(newLink);
                }
            }
        }
        public static void CreateServicesAndCategories(List<SalonDB> salonsDB, List<UserDB> usersDB, List<ServicesDB> services, List<PricesDB> pricesDB)
        {
            if (db.Categories.Count() == 0)
            {
                int i = 0;
                foreach (ServicesDB serviceDB in services.Where(x => x.first == null))
                {                    
                    db.Categories.Add(new Category("", serviceDB.name,i++));
                }
                db.SaveChanges();
            }

            if (db.Services.Count() == 0)
            {
                int i = 0;
                foreach (ServicesDB serviceDB in services.Where(x => x.first != null && !x.client_add))
                {
                    var alias = Transliteration.CyrillicToLatin(serviceDB.name, Language.Russian);
                    alias = alias.Replace(" ", "_");
                    alias = alias.Replace("`", "");
                    var categoryDB = services.First(x => serviceDB.first == x.id);
                    var category = db.Categories.First(x => x.Name == categoryDB.name);
                    Service service = null;
                    try
                    {
                        service = db.Services.Local.First(x => x.Name == serviceDB.name);
                    }
                    catch
                    {
                        service = new Service(serviceDB.name, i++, category.Id);
                        db.Services.Add(service);
                    }
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    throw ex;
                }
            }
        }

        public static void CreatePrices(List<SalonDB> salons, List<ServicesDB> servicesDB, List<PricesDB> pricesDB, List<UserDB> usersDB)
        {
            if (db.Prices.Count() == 0)
            {
                foreach (PricesDB priceDB in pricesDB)
                {
                    var userDB = usersDB.First(x => x.id == priceDB.id_user);
                    var categoryDB = servicesDB.First(x => x.id == priceDB.id_category && x.first == null);
                    var serviceDB = servicesDB.First(x => x.id == priceDB.id_podcategory && x.first != null);
                    var user = db.Users.Include("Firms.Categories.CustomServices").Include("Firms.Categories.Services").First(x => x.UserName == userDB.phone);
                    var category = db.Categories.First(x => x.Name == categoryDB.name);

                    if (user.Firms != null && user.Firms.Count() > 0)
                    {
                        CategoryInSalon c = null;
                        var salon = user.Firms.First();
                        try
                        {
                            c = salon.Categories.First(x => x.CategoryId == category.Id);
                        }
                        catch (InvalidOperationException)
                        {
                            c = new CategoryInSalon(category);
                            salon.Categories.Add(c);
                            try
                            {
                                db.SaveChanges();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                throw ex;
                            }
                        }

                        float min = priceDB.cena != null ? (float)priceDB.cena : 0;
                        float max = priceDB.cena_do != null ? (float)priceDB.cena_do : 0;

                        if (serviceDB.client_add)
                        {
                            c.CustomServices.Add(new CustomService(serviceDB.name, 0, new Price(min, max)));
                        }
                        else
                        {
                            try
                            {
                                Service service = db.Services.First(x => x.Name == serviceDB.name);
                                c.Services.Add(new ServiceInCategory(service, new Price(min, max)));
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                }
                db.SaveChanges();
            }

            db.SaveChanges();
        }

        public static void CreatePortfolios(List<ServicesDB> servicesDB, List<UserDB> usersDB, List<PortfolioDB> portfoliosDB)
        {
            if (db.Portfolios.Count() != 0)
            {
                return;
            }

            DirectoryInfo di = new DirectoryInfo(pathToPortfolioPictures);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            foreach (PortfolioDB portfolioDB in portfoliosDB)
            {
                var userDB = usersDB.First(x => x.id == portfolioDB.id_user);
                if (userDB == null)
                {
                    throw new Exception();
                }

                var categoryDB = servicesDB.First(x => x.id == portfolioDB.id_category);
                if (categoryDB == null)
                {
                    throw new Exception();
                }

                var category = db.Categories.First(x => x.Name == categoryDB.name);
                if (category == null)
                {
                    throw new Exception();
                }

                var user = db.Users.Include("Firms.Categories.Portfolios").First(x => x.UserName == userDB.phone);
                if (user == null)
                {
                    throw new Exception();
                }

                if (user.Firms.Count > 0)
                {
                    string image = "";
                    try
                    {
                        var from = pathToPictures + "portfolio\\" + portfolioDB.id_user.ToString() + "\\" + portfolioDB.id_category + "\\big-" + portfolioDB.name_file + ".jpg";
                        bool succes = false;
                        while (succes == false)
                        {
                            try
                            {
                                image = Guid.NewGuid().ToString();
                                File.Copy(from, pathToPortfolioPictures + "/" + image + ".jpg");
                                succes = true;
                            }
                            catch (NotSupportedException)
                            {
                                succes = false;
                            }
                        }
                    }
                    catch (FileNotFoundException)
                    {
                        var from = pathToPictures + @"portfolio\" + portfolioDB.id_user.ToString() + @"\" + portfolioDB.id_category + @"\ltl-" + portfolioDB.name_file + ".jpg";
                        bool succes = false;
                        while (succes == false)
                        {
                            try
                            {
                                image = Guid.NewGuid().ToString();
                                File.Copy(from, pathToPortfolioPictures + "/" + image + ".jpg");
                                succes = true;
                            }
                            catch (NotSupportedException)
                            {
                                succes = false;
                            }
                        }
                    }

                    CategoryInSalon cat;
                    var salon = user.Firms.First();
                    try
                    {
                        cat = salon.Categories.First(x => x.Category == category);
                    }
                    catch
                    {
                        cat = new CategoryInSalon(category);
                        salon.Categories.Add(cat);
                        db.SaveChanges();
                    }

                    var portfolio = new Portfolio(image);
                    cat.Portfolios.Add(portfolio);
                }
            }
            db.SaveChanges();
        }
        private static bool IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private static bool CreateRole(string name, string description, ApplicationRoleManager roleManager)
        {
            var roleSalon = roleManager.FindByName(name);
            if (roleSalon == null)
            {
                roleSalon = new ApplicationRole(name) { Description = description };
                try
                {
                    var roleresult = roleManager.Create(roleSalon);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        const string roleSalon = "Salon";
        const string roleFreelance = "Freelance";
        const string roleClient = "Client";
        const string roleSupplier = "Supplier";
        const string roleAdministrator = "Administrator";

        public static void InitializeIdentityForEF()
        {
            if (Debugger.IsAttached == false)
            {
                Debugger.Launch();
            }
            UserStore<User> userStore = new UserStore<User>(db);
            RoleStore<ApplicationRole> roleStore = new RoleStore<ApplicationRole>(db);
            ApplicationUserManager applicationUserManager = new ApplicationUserManager(userStore);
            ApplicationUserManager userManager = new ApplicationUserManager(userStore);
            ApplicationRoleManager roleManager = new ApplicationRoleManager(roleStore);

            userManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 1,
            };
            try
            {
                var isroleSalon = CreateRole(roleSalon, "Салон", roleManager);
                var isroleFreelance = CreateRole(roleFreelance, "Мастер", roleManager);
                var isroleClient = CreateRole(roleClient, "Клиент", roleManager);
                var isroleSupplier = CreateRole(roleSupplier, "Поставщик", roleManager);
                var isroleAdministrator = CreateRole(roleAdministrator, "Администратор", roleManager);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            string cs = "Driver={PostgreSQL UNICODE};Server=localhost;Port=5432;Database=bp;Uid=postgres;Pwd=bair;";

            var users = ReadUsers("select * from users", cs);
            var salons = ReadSalons("select * from info", cs);
            var services = ReadServices("select * from uslugi", cs);
            var prices = ReadPrices("select * from portfolio_ceni", cs);
            var portfolio = ReadPortfolio("select * from portfolio_photo", cs);

            CreateUsers(userManager, users, roleManager, salons);
            CreateSalons(userManager, salons, users);
            CreateServicesAndCategories(salons, users, services, prices);
            CreatePrices(salons, services, prices, users);
            CreatePortfolios(services, users, portfolio);
            FillPaidServices();
        }
        static void FillPaidServices()
        {
            db.BannerCost.Add(new BannerCost() { Cost = 100 });
            db.BannerDesignerCost.Add(new BannerDesignerCost() { Cost = 200 });
            db.PromotionCost.Add(new PromotionCost() { Cost = 300 });
            db.PromotionDesignerCost.Add(new PromotionDesignerCost() { Cost = 400 });
            db.ScheduleCost.Add(new ScheduleCost() { Cost = 500 });
            db.VacancyCost.Add(new VacancyCost() { Cost = 600 });
            db.SaveChanges();
        }
    }
}
