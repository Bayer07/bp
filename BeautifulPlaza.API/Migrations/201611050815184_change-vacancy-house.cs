namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changevacancyhouse : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Vacancies", "House", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vacancies", "House", c => c.Int(nullable: false));
        }
    }
}
