namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_active_streets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Streets", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Streets", "IsActive");
        }
    }
}
