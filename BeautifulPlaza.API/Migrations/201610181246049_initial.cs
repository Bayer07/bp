namespace BP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BannerCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BannerDesignerCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Banners",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Weight = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Message = c.String(),
                        SalonId = c.Int(),
                        FreelanceId = c.Int(),
                        Cost_Id = c.Int(),
                        DesignerCost_Id = c.Int(),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BannerCosts", t => t.Cost_Id)
                .ForeignKey("dbo.BannerDesignerCosts", t => t.DesignerCost_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.Cost_Id)
                .Index(t => t.DesignerCost_Id)
                .Index(t => t.Firm_Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Image = c.String(maxLength: 36),
                        Name = c.String(maxLength: 50),
                        Alias = c.String(maxLength: 50),
                        Priority = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryInSalons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.CategoryId)
                .Index(t => t.Firm_Id);
            
            CreateTable(
                "dbo.CustomServices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Alias = c.String(maxLength: 50),
                        Priority = c.Int(nullable: false),
                        Price_Id = c.Int(),
                        CategoryInSalon_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prices", t => t.Price_Id)
                .ForeignKey("dbo.CategoryInSalons", t => t.CategoryInSalon_Id)
                .Index(t => t.Price_Id)
                .Index(t => t.CategoryInSalon_Id);
            
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Min = c.Single(nullable: false),
                        Max = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Portfolios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Image = c.String(maxLength: 36),
                        CategoryInSalon_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryInSalons", t => t.CategoryInSalon_Id)
                .Index(t => t.CategoryInSalon_Id);
            
            CreateTable(
                "dbo.ServiceInCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price_Id = c.Int(),
                        Service_Id = c.Int(),
                        CategoryInSalon_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prices", t => t.Price_Id)
                .ForeignKey("dbo.Services", t => t.Service_Id)
                .ForeignKey("dbo.CategoryInSalons", t => t.CategoryInSalon_Id)
                .Index(t => t.Price_Id)
                .Index(t => t.Service_Id)
                .Index(t => t.CategoryInSalon_Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 150),
                        Alias = c.String(maxLength: 150),
                        Priority = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        CreatorId = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Coordinates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Single(nullable: false),
                        Longitude = c.Single(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DayOfWeeks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(maxLength: 50),
                        Firm_Id = c.Int(),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.Firm_Id)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.Links",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(maxLength: 50),
                        LinkType_Id = c.Int(),
                        Firm_Id = c.Int(),
                        Vacancy_Id = c.Int(),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LinkTypes", t => t.LinkType_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .ForeignKey("dbo.Vacancies", t => t.Vacancy_Id)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.LinkType_Id)
                .Index(t => t.Firm_Id)
                .Index(t => t.Vacancy_Id)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.LinkTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MessageTitles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(),
                        Title = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.MessageBodies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MessageTitleId = c.Int(nullable: false),
                        Body = c.String(),
                        ApplicationUserId = c.String(),
                        IsReaded = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MessageTitles", t => t.MessageTitleId, cascadeDelete: true)
                .Index(t => t.MessageTitleId);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(maxLength: 12),
                        Firm_Id = c.Int(),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.Firm_Id)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.PromotionCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PromotionDesignerCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Promotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Weight = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Message = c.String(),
                        Title = c.String(),
                        Description = c.String(),
                        PriceId = c.Int(nullable: false),
                        IsNeedDesigner = c.Boolean(nullable: false),
                        Cost_Id = c.Int(),
                        DesignerCost_Id = c.Int(),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PromotionCosts", t => t.Cost_Id)
                .ForeignKey("dbo.PromotionDesignerCosts", t => t.DesignerCost_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.Cost_Id)
                .Index(t => t.DesignerCost_Id)
                .Index(t => t.Firm_Id);
            
            CreateTable(
                "dbo.PromotionInCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        Category_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Description = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Firms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        House = c.String(maxLength: 20),
                        Floor = c.String(maxLength: 30),
                        Office = c.String(maxLength: 20),
                        IsCashlessPayment = c.Boolean(nullable: false),
                        IsWorkAtHome = c.Boolean(nullable: false),
                        IsWifi = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsFreelance = c.Boolean(nullable: false),
                        DiscountForBirthday = c.String(),
                        WorkWithProduction = c.String(),
                        Description = c.String(),
                        Scores = c.Int(nullable: false),
                        Image = c.String(),
                        RegisterDate = c.DateTime(nullable: false),
                        City_Id = c.Int(),
                        Coordinates_Id = c.Int(),
                        Street_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.City_Id)
                .ForeignKey("dbo.Coordinates", t => t.Coordinates_Id)
                .ForeignKey("dbo.Streets", t => t.Street_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.City_Id)
                .Index(t => t.Coordinates_Id)
                .Index(t => t.Street_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ScheduleRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Cost_Id = c.Int(),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ScheduleCosts", t => t.Cost_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.Cost_Id)
                .Index(t => t.Firm_Id);
            
            CreateTable(
                "dbo.ScheduleCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        ScheduleStatus = c.Int(nullable: false),
                        Price_Id = c.Int(),
                        Service_Id = c.Int(),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prices", t => t.Price_Id)
                .ForeignKey("dbo.Services", t => t.Service_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.Price_Id)
                .Index(t => t.Service_Id)
                .Index(t => t.Firm_Id);
            
            CreateTable(
                "dbo.Streets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CityId = c.Int(nullable: false),
                        Name = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vacancies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        MinPayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxPayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Percent = c.Int(),
                        ContactName = c.String(nullable: false, maxLength: 50),
                        Text = c.String(nullable: false, maxLength: 1000),
                        House = c.Int(nullable: false),
                        City_Id = c.Int(),
                        Cost_Id = c.Int(),
                        Email_Id = c.Int(),
                        Phone_Id = c.Int(nullable: false),
                        Street_Id = c.Int(),
                        VacancyName_Id = c.Int(),
                        Firm_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.City_Id)
                .ForeignKey("dbo.VacancyCosts", t => t.Cost_Id)
                .ForeignKey("dbo.Emails", t => t.Email_Id)
                .ForeignKey("dbo.Phones", t => t.Phone_Id, cascadeDelete: true)
                .ForeignKey("dbo.Streets", t => t.Street_Id)
                .ForeignKey("dbo.VacancyNames", t => t.VacancyName_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .Index(t => t.City_Id)
                .Index(t => t.Cost_Id)
                .Index(t => t.Email_Id)
                .Index(t => t.Phone_Id)
                .Index(t => t.Street_Id)
                .Index(t => t.VacancyName_Id)
                .Index(t => t.Firm_Id);
            
            CreateTable(
                "dbo.VacancyCosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VacancyNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        CreateDate = c.DateTime(nullable: false),
                        CreatorId = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkTimes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Open = c.Time(nullable: false, precision: 7),
                        Close = c.Time(nullable: false, precision: 7),
                        DayOfWeek_Id = c.Int(),
                        Firm_Id = c.Int(),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DayOfWeeks", t => t.DayOfWeek_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_Id)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.DayOfWeek_Id)
                .Index(t => t.Firm_Id)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Name = c.String(),
                        House = c.String(),
                        Office = c.String(),
                        Description = c.String(),
                        Scores = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Image = c.String(),
                        RegisterDate = c.DateTime(nullable: false),
                        City_Id = c.Int(),
                        Street_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.City_Id)
                .ForeignKey("dbo.Streets", t => t.Street_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.City_Id)
                .Index(t => t.Street_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(maxLength: 75),
                        Status = c.Boolean(nullable: false),
                        DateRegistration = c.DateTime(nullable: false),
                        DateBirthday = c.DateTime(nullable: false),
                        Image = c.String(maxLength: 36),
                        VerificationNumber = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Suppliers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Schedules", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.MessageTitles", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Firms", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.WorkTimes", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "Street_Id", "dbo.Streets");
            DropForeignKey("dbo.Phones", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.Links", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.Emails", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "City_Id", "dbo.Cities");
            DropForeignKey("dbo.WorkTimes", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.WorkTimes", "DayOfWeek_Id", "dbo.DayOfWeeks");
            DropForeignKey("dbo.Vacancies", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Vacancies", "VacancyName_Id", "dbo.VacancyNames");
            DropForeignKey("dbo.Vacancies", "Street_Id", "dbo.Streets");
            DropForeignKey("dbo.Vacancies", "Phone_Id", "dbo.Phones");
            DropForeignKey("dbo.Links", "Vacancy_Id", "dbo.Vacancies");
            DropForeignKey("dbo.Vacancies", "Email_Id", "dbo.Emails");
            DropForeignKey("dbo.Vacancies", "Cost_Id", "dbo.VacancyCosts");
            DropForeignKey("dbo.Vacancies", "City_Id", "dbo.Cities");
            DropForeignKey("dbo.Firms", "Street_Id", "dbo.Streets");
            DropForeignKey("dbo.Schedules", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Schedules", "Service_Id", "dbo.Services");
            DropForeignKey("dbo.Schedules", "Price_Id", "dbo.Prices");
            DropForeignKey("dbo.ScheduleRequests", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.ScheduleRequests", "Cost_Id", "dbo.ScheduleCosts");
            DropForeignKey("dbo.Promotions", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Phones", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Links", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Emails", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Firms", "Coordinates_Id", "dbo.Coordinates");
            DropForeignKey("dbo.Firms", "City_Id", "dbo.Cities");
            DropForeignKey("dbo.CategoryInSalons", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.Banners", "Firm_Id", "dbo.Firms");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Promotions", "DesignerCost_Id", "dbo.PromotionDesignerCosts");
            DropForeignKey("dbo.Promotions", "Cost_Id", "dbo.PromotionCosts");
            DropForeignKey("dbo.PromotionInCategories", "PromotionId", "dbo.Promotions");
            DropForeignKey("dbo.PromotionInCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.MessageBodies", "MessageTitleId", "dbo.MessageTitles");
            DropForeignKey("dbo.Links", "LinkType_Id", "dbo.LinkTypes");
            DropForeignKey("dbo.ServiceInCategories", "CategoryInSalon_Id", "dbo.CategoryInSalons");
            DropForeignKey("dbo.ServiceInCategories", "Service_Id", "dbo.Services");
            DropForeignKey("dbo.ServiceInCategories", "Price_Id", "dbo.Prices");
            DropForeignKey("dbo.Portfolios", "CategoryInSalon_Id", "dbo.CategoryInSalons");
            DropForeignKey("dbo.CustomServices", "CategoryInSalon_Id", "dbo.CategoryInSalons");
            DropForeignKey("dbo.CustomServices", "Price_Id", "dbo.Prices");
            DropForeignKey("dbo.CategoryInSalons", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Banners", "DesignerCost_Id", "dbo.BannerDesignerCosts");
            DropForeignKey("dbo.Banners", "Cost_Id", "dbo.BannerCosts");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Suppliers", new[] { "Street_Id" });
            DropIndex("dbo.Suppliers", new[] { "City_Id" });
            DropIndex("dbo.Suppliers", new[] { "UserId" });
            DropIndex("dbo.WorkTimes", new[] { "Supplier_Id" });
            DropIndex("dbo.WorkTimes", new[] { "Firm_Id" });
            DropIndex("dbo.WorkTimes", new[] { "DayOfWeek_Id" });
            DropIndex("dbo.Vacancies", new[] { "Firm_Id" });
            DropIndex("dbo.Vacancies", new[] { "VacancyName_Id" });
            DropIndex("dbo.Vacancies", new[] { "Street_Id" });
            DropIndex("dbo.Vacancies", new[] { "Phone_Id" });
            DropIndex("dbo.Vacancies", new[] { "Email_Id" });
            DropIndex("dbo.Vacancies", new[] { "Cost_Id" });
            DropIndex("dbo.Vacancies", new[] { "City_Id" });
            DropIndex("dbo.Schedules", new[] { "Firm_Id" });
            DropIndex("dbo.Schedules", new[] { "Service_Id" });
            DropIndex("dbo.Schedules", new[] { "Price_Id" });
            DropIndex("dbo.Schedules", new[] { "UserId" });
            DropIndex("dbo.ScheduleRequests", new[] { "Firm_Id" });
            DropIndex("dbo.ScheduleRequests", new[] { "Cost_Id" });
            DropIndex("dbo.Firms", new[] { "User_Id" });
            DropIndex("dbo.Firms", new[] { "Street_Id" });
            DropIndex("dbo.Firms", new[] { "Coordinates_Id" });
            DropIndex("dbo.Firms", new[] { "City_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.PromotionInCategories", new[] { "Category_Id" });
            DropIndex("dbo.PromotionInCategories", new[] { "PromotionId" });
            DropIndex("dbo.Promotions", new[] { "Firm_Id" });
            DropIndex("dbo.Promotions", new[] { "DesignerCost_Id" });
            DropIndex("dbo.Promotions", new[] { "Cost_Id" });
            DropIndex("dbo.Phones", new[] { "Supplier_Id" });
            DropIndex("dbo.Phones", new[] { "Firm_Id" });
            DropIndex("dbo.MessageBodies", new[] { "MessageTitleId" });
            DropIndex("dbo.MessageTitles", new[] { "User_Id" });
            DropIndex("dbo.Links", new[] { "Supplier_Id" });
            DropIndex("dbo.Links", new[] { "Vacancy_Id" });
            DropIndex("dbo.Links", new[] { "Firm_Id" });
            DropIndex("dbo.Links", new[] { "LinkType_Id" });
            DropIndex("dbo.Emails", new[] { "Supplier_Id" });
            DropIndex("dbo.Emails", new[] { "Firm_Id" });
            DropIndex("dbo.ServiceInCategories", new[] { "CategoryInSalon_Id" });
            DropIndex("dbo.ServiceInCategories", new[] { "Service_Id" });
            DropIndex("dbo.ServiceInCategories", new[] { "Price_Id" });
            DropIndex("dbo.Portfolios", new[] { "CategoryInSalon_Id" });
            DropIndex("dbo.CustomServices", new[] { "CategoryInSalon_Id" });
            DropIndex("dbo.CustomServices", new[] { "Price_Id" });
            DropIndex("dbo.CategoryInSalons", new[] { "Firm_Id" });
            DropIndex("dbo.CategoryInSalons", new[] { "CategoryId" });
            DropIndex("dbo.Banners", new[] { "Firm_Id" });
            DropIndex("dbo.Banners", new[] { "DesignerCost_Id" });
            DropIndex("dbo.Banners", new[] { "Cost_Id" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Suppliers");
            DropTable("dbo.WorkTimes");
            DropTable("dbo.VacancyNames");
            DropTable("dbo.VacancyCosts");
            DropTable("dbo.Vacancies");
            DropTable("dbo.Streets");
            DropTable("dbo.Schedules");
            DropTable("dbo.ScheduleCosts");
            DropTable("dbo.ScheduleRequests");
            DropTable("dbo.Firms");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PromotionInCategories");
            DropTable("dbo.Promotions");
            DropTable("dbo.PromotionDesignerCosts");
            DropTable("dbo.PromotionCosts");
            DropTable("dbo.Phones");
            DropTable("dbo.MessageBodies");
            DropTable("dbo.MessageTitles");
            DropTable("dbo.LinkTypes");
            DropTable("dbo.Links");
            DropTable("dbo.Emails");
            DropTable("dbo.DayOfWeeks");
            DropTable("dbo.Coordinates");
            DropTable("dbo.Cities");
            DropTable("dbo.Services");
            DropTable("dbo.ServiceInCategories");
            DropTable("dbo.Portfolios");
            DropTable("dbo.Prices");
            DropTable("dbo.CustomServices");
            DropTable("dbo.CategoryInSalons");
            DropTable("dbo.Categories");
            DropTable("dbo.Banners");
            DropTable("dbo.BannerDesignerCosts");
            DropTable("dbo.BannerCosts");
        }
    }
}
